# unittest

# 20200919/李雅兰

#### :hamburger: :fries: :pizza: :poultry_leg: :meat_on_bone: :fried_shrimp: :custard: :lollipop: :cookie: :baby_bottle:

## Write

## 目前dev分支和backup分支的版本不是同一个版本

### 写用例

### 所有用例必须放在usecase下面,参照get_user_list.py

### 注释和代码风格尽量保持一致啊

        @test #用来注册测试用例
        @injectable(['page', 'size']) #用来完成测试用例数据的命名（使用list命名）
        class TestGetUserList(unittest.TestCase):
            def setup(self):
                print('测试开始\n')
        
            @inject(data[0]) #注入测试数据（使用list注入）
            def test_get_user_list_normally(self): #测试用例名称
                """every param is normal"""
                self.assertEqual(self.page,'1')
       
            def teardown(self):
                print('测试结束\n')

### 我们还是要参照python的标准去写代码啊

###            

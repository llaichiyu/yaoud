#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

from src.base.send_email import SendEmail
from src.base.generate_report import run_case_and_generate_report
from src.utils.get_some_data import get_smtpserver, get_mail_user, get_mail_pass, get_sender, get_receiver, get_subject, \
    get_local_host, get_smtp_port
from src.base import get_lastest_report

if __name__ == "__main__":
    run_case_and_generate_report()
    report_dir = 'src/report/'
    latest_report = get_lastest_report.get_latest_report(report_dir)
    send_email = SendEmail(subject=get_subject(), sender=get_sender(), receiver=get_receiver(),
                           smtp_server=get_smtpserver(), smtp_port=get_smtp_port(), localhost=get_local_host(),
                           mail_user=get_mail_user(), mail_pass=get_mail_pass(), latest_report=latest_report)
    send_email.send_email(latest_report)

#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.operation import url_judgment

http = Http()
data = get_data('src/data/csv/operation/allocation.csv')


@test
@injectable(session=True)
class TestJudgment(YaoudTestCase):
    @inject(data)
    def test_judement(self):
        """normally judgment"""
        self.replenish_web_judgement()
        self.check_judgment_status_code(200)
        self.check_judement_code(0)

    def replenish_web_judgement(self):
        """for replenish"""
        self.result_judgment = http.post(url=url_judgment, session=self.session)

    def check_judgment_status_code(self, status_code):
        self.assertEqual(self.result_judgment.status_code, status_code)

    def check_judement_code(self, code):
        self.assertEqual(self.result_judgment.json().get('code'), code)

#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.operation import url_accept_warehouse_list

data = get_data('src/data/csv/operation/accept_warehouse_list.csv')
http = Http()


@test
@injectable(
    title=['gsahPsVoucherId', 'gsahTotalAmt', 'gsahStatus', 'orderDate', 'gsadProId', 'batchNo', 'gsahEmp', 'gsahDate',
           'pickProId', 'psVoucherId'], session=True)
class TestAcceptWareHouseList(YaoudTestCase):
    @inject(data[0])
    def test_accept_ware_house_list_by_some_params(self):
        """gsahStatus ==1 && gsahDate ==gsahDate && pickProId ==202106160001-01"""
        self.accept_ware_house_list()
        self.check_accept_ware_house_list_status_code(200)
        self.check_accept_ware_house_list_code(0)

    @inject(data[1])
    def test_pickProId_is_not_exists_in_db(self):
        """pickProdId is not exists in db"""
        self.accept_ware_house_list()
        self.check_accept_ware_house_list_status_code(200)
        self.check_accept_ware_house_list_code(0)
        self.check_pickProdId_is_not_exists(0)

    def check_pickProdId_is_not_exists(self, pickProQty):
        """pickProQty == 0 owing to pickProId is not exists in db"""
        self.data = self.res_accept_ware_house_list.json().get('data')
        self.pickProQty = self.data['totalMap']['pickProQty']
        self.assertEqual(self.pickProQty, pickProQty)

    def accept_ware_house_list(self):
        self.res_accept_ware_house_list = http.post(url=url_accept_warehouse_list, data={
            "gsahPsVoucherId": self.gsahPsVoucherId,
            "gsahTotalAmt": self.gsahTotalAmt,
            "gsahStatus": self.gsahStatus,
            "orderDate": self.orderDate,
            "gsadProId": self.gsadProId,
            "batchNo": self.batchNo,
            "gsahEmp": self.batchNo,
            "gsahDate": self.gsahDate,
            "pickProId": self.pickProId,
            "psVoucherId": self.psVoucherId}, session=self.session)

    def check_accept_ware_house_list_status_code(self, status_code):
        """for check accept ware house list status code"""
        self.assertAlmostEqual(self.res_accept_ware_house_list.status_code, status_code)

    def check_accept_ware_house_list_code(self, code):
        """for check accept ware house list status code"""
        self.assertAlmostEqual(self.res_accept_ware_house_list.json().get('code'), code)

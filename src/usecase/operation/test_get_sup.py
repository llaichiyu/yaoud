#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
@date 2021年07月27日9:48 上午
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.operation import url_get_sup

http = Http()
data = get_data('src/data/csv/operation/get_sup.csv')


@test
@injectable(title=[], session=True)
class TestGetSup(YaoudTestCase):
    # @inject(data[0])
    def test_get_sup_normally(self):
        self.get_sup()
        self.check_get_sup_status_code(200)
        self.check_get_sup_code(0)

    # @inject(data[0])
    def test_expire_token_for_get_sup_normally(self):
        self.expire_token_for_get_sup()
        self.check_expire_token_for_get_sup_status_code(200)
        self.check_expire_token_for_get_sup_code(401)

    # @inject(data[1])
    def test_none_body(self):
        self.none_body()
        self.check_none_body_status_code(200)
        self.check_none_body_code(500)

    def get_sup(self):
        self.res_get_sup = http.post(url=url_get_sup, data={}, session=self.session)

    def check_get_sup_status_code(self, status_code):
        self.assertEqual(self.res_get_sup.status_code, status_code)

    def check_get_sup_code(self, code):
        self.assertEqual(self.res_get_sup.json().get('code'), code)

    def expire_token_for_get_sup(self):
        self.res_expire_token = http.expire_token_for_post(url=url_get_sup, data={}, session=self.session)

    def check_expire_token_for_get_sup_status_code(self, status_code):
        self.assertEqual(self.res_expire_token.status_code, status_code)

    def check_expire_token_for_get_sup_code(self, code):
        self.assertEqual(self.res_expire_token.json().get('code'), code)

    def none_body(self):
        self.res_none_body = http.post(url=url_get_sup, session=self.session)

    def check_none_body_status_code(self, status_code):
        self.assertEqual(self.res_none_body.status_code, status_code)

    def check_none_body_code(self, code):
        self.assertEqual(self.res_none_body.json().get('code'), code)

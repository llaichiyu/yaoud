#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.operation import url_get_accept_order_list

data = get_data('src/data/csv/operation/get_accept_order_list.csv')
http = Http()


@test
@injectable(title=['voucherId', 'nameOrCode', 'batchNo', 'supNameOrCode', 'startDate', 'endDate', 'brIdList'],
            session=True)
class TestAcceptOrderList(YaoudTestCase):
    # @inject(data)
    def test_all_params_exits_in_db(self):
        """this datum is exists in db"""
        self.accept_normally()
        self.check_normally_accept_status_code(200)
        self.check_normally_accept_code(0)

    @inject(data[2])
    def test_all_params_blank(self):
        """brIdList == blank str"""
        self.accept_order_list()
        self.check_accept_order_list_status_code(200)
        self.check_accept_order_list_code(1001)

    @inject(data[0])
    def test_None_brIdList(self):
        """all params are delivered except brIdList"""
        self.None_brIdList()
        self.check_None_brIdList_status_code(200)
        self.check_None_brIdList_code(1001)

    @inject(data[2])
    def test_blank_brIdList(self):
        """for blank brIdList"""
        self.accept_order_list()
        self.check_accept_order_list_status_code(200)
        self.check_accept_order_list_code(1001)

    @inject(data[0])
    def test_expired_token_for_accept_order_list(self):
        """expired token for accept order list"""
        self.expire_token_for_accept_order_list()
        self.check_expire_token_for_accept_order_list_status_code(200)
        self.check_expire_token_for_accept_order_list_code(401)

    def expire_token_for_accept_order_list(self):
        """token is defined in http_class and we can mock expired token"""
        self.res_expire_token = http.expire_token_for_post(url=url_get_accept_order_list, data={
            "voucherId": self.voucherId,
            "nameOrCode": self.nameOrCode,
            "batchNo": self.batchNo,
            "supNameOrCode": self.supNameOrCode,
            "startDate": self.startDate,
            "endDate": self.endDate,
            "brIdList": self.brIdList
        }, session=self.session)

    def check_expire_token_for_accept_order_list_status_code(self, status_code):
        self.assertEqual(self.res_expire_token.status_code, status_code)

    def check_expire_token_for_accept_order_list_code(self, code):
        self.assertEqual(self.res_expire_token.json().get('code'), code)

    def None_brIdList(self):
        """brIdList is None and other params all exists"""
        self.res_None_brIdList = http.post(url=url_get_accept_order_list, data={"voucherId": self.voucherId,
                                                                                "nameOrCode": self.nameOrCode,
                                                                                "batchNo": self.batchNo,
                                                                                "supNameOrCode": self.supNameOrCode,
                                                                                "startDate": self.startDate,
                                                                                "endDate": self.endDate},
                                           session=self.session)

    def check_None_brIdList_status_code(self, status_code):
        """for check_None_brIdList status code"""
        self.assertEqual(self.res_None_brIdList.status_code, status_code)

    def check_None_brIdList_code(self, code):
        """for check None_brIdList code"""
        self.assertEqual(self.res_None_brIdList.json().get('code'), code)

    def accept_normally(self):
        """all params are defined"""
        self.res_defined = http.post(url=url_get_accept_order_list, data={
            "gsahVoucherId": "PS2021001127",
            "gsahTotalAmt": "0",
            "gsahStatus": "1",
            "orderDate": "20210329",
            "gsadProId": "01050010085",
            "batchNo": "190803",
            "gsahEmp": "1002",
            "gsahDate": "20210429",
            "brIdList": [1000]
        }, session=self.session)

    def check_normally_accept_status_code(self, status_code):
        self.assertEqual(self.res_defined.status_code, status_code)

    def check_normally_accept_code(self, code):
        self.assertEqual(self.res_defined.json().get('code'), code)

    def accept_order_list(self):
        """for accept order list"""
        self.res_accept_list = http.post(url=url_get_accept_order_list, data={
            "voucherId": self.voucherId,
            "nameOrCode": self.nameOrCode,
            "batchNo": self.batchNo,
            "supNameOrCode": self.supNameOrCode,
            "startDate": self.startDate,
            "endDate": self.endDate,
            "brIdList": self.brIdList
        }, session=self.session)

    def check_accept_order_list_status_code(self, status_code):
        """for check accept order list status code"""
        self.assertEqual(self.res_accept_list.status_code, status_code)

    def check_accept_order_list_code(self, code):
        """for check res_accept_order_list code"""
        self.assertEqual(self.res_accept_list.json().get('code'), code)

#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
@date 2021年07月27日2:05 下午
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.operation import url_show_wtps_label

http = Http()
data = get_data('src/data/csv/operation/show_wtps_label.csv')


@test
@injectable(title=[], session=True)
class TestShowWtpsLabel(YaoudTestCase):
    # @inject(data[0])
    def test_show_wtps_label_normally(self):
        self.show_wtps_label()
        self.check_show_wtps_label_status_code(200)
        self.check_show_wtps_label_code(0)
        # self.check_is_wtps_param('0')

    # @inject(data[0])
    def test_expire_token(self):
        self.expire_token_for_show()
        self.check_expire_token_for_show_status_code(200)
        self.check_expire_token_for_show_code(401)

    def check_is_wtps_param(self, is_wtps_param):
        self.assertEqual(self.res_show.json().get('data').json().get('isWtpsParam'), is_wtps_param)

    def show_wtps_label(self):
        self.res_show = http.post(url=url_show_wtps_label, data={}, session=self.session)

    def check_show_wtps_label_status_code(self, status_code):
        self.assertEqual(self.res_show.status_code, status_code)

    def check_show_wtps_label_code(self, code):
        self.assertEqual(self.res_show.json().get('code'), code)

    def expire_token_for_show(self):
        self.res_expire_token = http.post(url=url_show_wtps_label, data={}, session=self.session)

    def check_expire_token_for_show_status_code(self, status_code):
        self.assertEqual(self.res_expire_token.status_code, status_code)

    def check_expire_token_for_show_code(self, code):
        self.assertEqual(self.res_expire_token.json().get('code'), code)

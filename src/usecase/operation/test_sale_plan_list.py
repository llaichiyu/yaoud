#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
@date 2021年08月05日4:14 下午
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.operation import url_sale_plan_list

http = Http()
data = get_data('src/data/csv/operation/sale_plan_list.csv')


@test
@injectable(title=[], session=True)
class TestSalePlanList(YaoudTestCase):
    # @inject(data[0])
    def test_get_sale_plan_list_normally(self):
        """for get sale plan list normally"""
        self.sale_plan_list()
        self.check_sale_plan_list_status_code(200)
        self.check_sale_plan_listc_ode(0)

    # @inject(data[1])
    def test_sale_plan_list(self):
        self.blank_body()
        self.check_blank_body_status_code(200)
        self.check_blank_body_code(0)

    # @inject(data[0])
    def test_expire_token_for_sale_plan_list(self):
        self.expire_token_for_sale_plan_list()
        self.check_expire_token_status_code(200)
        self.check_expire_token_code(401)

    def expire_token_for_sale_plan_list(self):
        self.res_expire_token = http.expire_token_for_post(url=url_sale_plan_list,
                                                           data={},
                                                           session=self.session)

    def check_expire_token_status_code(self, status_code):
        self.assertEqual(self.res_expire_token.status_code, status_code)

    def check_expire_token_code(self, code):
        self.assertEqual(self.res_expire_token.json().get('code'), code)

    def blank_body(self):
        self.res_blank_body = http.post(url=url_sale_plan_list, data={}, session=self.session)

    def check_blank_body_status_code(self, status_code):
        self.assertEqual(self.res_blank_body.status_code, status_code)

    def check_blank_body_code(self, code):
        self.assertEqual(self.res_blank_body.json().get('code'), code)

    def sale_plan_list(self):
        self.res_sale_plan_list = http.post(url=url_sale_plan_list,
                                            data={},
                                            session=self.session)

    def check_sale_plan_list_status_code(self, status_code):
        self.assertEqual(self.res_sale_plan_list.status_code, status_code)

    def check_sale_plan_listc_ode(self, code):
        self.assertEqual(self.res_sale_plan_list.json().get('code'), code)

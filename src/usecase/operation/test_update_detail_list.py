#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
@date 2021年08月12日4:16 下午
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.operation import url_update_detail_list

http = Http()
data = get_data('src/data/csv/operation/update_detail_list.csv')


@test
@injectable(title=['id', 'status'], session=True)
class TestUpdateDetailList(YaoudTestCase):
    def update_detail_list(self):
        self.res_update = http.post(url=url_update_detail_list, data={'id': self.id, 'status': self.status},
                                    session=self.session)

    def check_update_detail_list_status_code(self, status_code):
        self.assertEqual(self.res_update.status_code, status_code)

    def check_update_detail_list_code(self, code):
        self.assertEqual(self.res_update.json().get('code'), code)

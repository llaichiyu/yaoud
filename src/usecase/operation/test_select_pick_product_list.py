#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
@date 2021年07月26日3:49 下午
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.operation import url_select_pick_productList

http = Http()
data = get_data('src/data/csv/operation/select_pick_product_list.csv')


@test
@injectable(title=['pickProId'], session=True)
class TestSelectPickProductList(YaoudTestCase):
    @inject(data[0])
    def test_select_product_list_by_pickproid(self):
        """pickproid"""
        self.select_pick_product_list()
        self.check_select_pick_product_list_status_code(200)
        self.check_select_pick_product_list_code(0)

    @inject(data[0])
    def test_select_product_list_by_pickproid(self):
        self.raw_body()
        self.check_raw_body_status_code(200)
        self.check_raw_body_code(0)

    @inject(data[0])
    def test_expire(self):
        self.expire_token_for_select()
        self.check_expire_token_status_code(200)
        self.check_expire_token_code(401)

    def expire_token_for_select(self):
        """token is out of data"""
        self.res_token_out_of_date = http.expire_token_for_post(url=url_select_pick_productList,
                                                                data={'pickProId': self.pickProId},
                                                                session=self.session)

    def check_expire_token_status_code(self, status_code):
        self.assertEqual(self.res_token_out_of_date.status_code, status_code)

    def check_expire_token_code(self, code):
        self.assertEqual(self.res_token_out_of_date.json().get('code'), code)

    def raw_body(self):
        self.res_raw = http.post(url=url_select_pick_productList, data={}, session=self.session)

    def check_raw_body_status_code(self, status_code):
        self.assertEqual(self.res_raw.status_code, status_code)

    def check_raw_body_code(self, code):
        self.assertEqual(self.res_raw.json().get('code'), code)

    def select_pick_product_list(self):
        self.res_select = http.post(url=url_select_pick_productList, data={'pickProId': self.pickProId},
                                    session=self.session)

    def check_select_pick_product_list_status_code(self, status_code):
        self.assertEqual(self.res_select.status_code, status_code)

    def check_select_pick_product_list_code(self, code):
        self.assertEqual(self.res_select.json().get('code'), code)

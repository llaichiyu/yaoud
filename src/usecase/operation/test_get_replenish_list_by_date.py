#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.operation import url_get_replenish_list_by_date
import requests

data = get_data('src/data/csv/operation/get_replenish_list_by_date.csv')
http = Http()


@test
@injectable(title=['queryStartDate', 'queryEndDate'], session=True)
class TestGetReplenishListByDate(YaoudTestCase):
    @inject(data[0])
    def test_get_replenish(self):
        """date is normal"""
        self.get_replenish_list_by_date()
        self.check_get_replenish_list_by_date_status_code(200)
        self.check_get_replenish_list_by_date_code(0)

    def get_replenish_list_by_date(self):
        """for get replenish list by list"""
        self.res_get_replenish_list_by_date = http.post(url=url_get_replenish_list_by_date,
                                                        data={'queryStartDate': self.queryStartDate,
                                                              'queryEndDate': self.queryEndDate},
                                                        session=self.session)

    def check_get_replenish_list_by_date_status_code(self, status_code):
        """for check get replenish list status code"""
        self.assertEqual(self.res_get_replenish_list_by_date.status_code, status_code)

    def check_get_replenish_list_by_date_code(self, code):
        """for check code"""
        self.assertEqual(self.res_get_replenish_list_by_date.json().get('code'), code)

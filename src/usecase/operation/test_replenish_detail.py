#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.operation import url_replenish_detail
from src.url.auth_hub import url_choose_franc
from src.url.operation import url_choose_store

data = get_data('src/data/csv/operation/replenish_detail.csv')
http = Http()


@test
@injectable(['productCode'], session=True)
class TestReplenishDetail(YaoudTestCase):
    @inject(data[0], multi=False)
    def test_replenish_detail(self):
        """for check replenish detail"""
        self.choose_france()
        self.choose_store()
        self.replenish_detail()
        self.check_replenish_detail_status_code(200)
        self.check_replenish_detail_code(0)

    def choose_store(self):
        """for get stocode"""
        self.result_choose_strore = http.post(url=url_choose_store,
                                              data={"userId": "1071", "stoCode": "10000", "client": "10000005"},
                                              session=self.session)

    def choose_france(self):
        """for choose france"""
        self.result_choose_france = http.post(url=url_choose_franc, data={'client': '10000005', 'userId': '1071'},
                                              session=self.session)

    def replenish_detail(self):
        """for replenish detail"""
        self.result_replenish_detail = http.post(url=url_replenish_detail, data={'productCode': self.productCode},
                                                 session=self.session)

    def check_replenish_detail_status_code(self, status_code):
        """for check result detail status code"""
        self.assertEqual(self.result_replenish_detail.status_code, status_code)

    def check_replenish_detail_code(self, code):
        """for check result detail code"""
        self.assertEqual(self.result_replenish_detail.json().get('code'), code)

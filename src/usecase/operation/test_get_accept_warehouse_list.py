#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
@date 2021年07月26日2:44 下午
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.operation import url_get_accept_warehouse_list

http = Http()
data = get_data('src/data/csv/operation/get_accept_warehouse_list.csv')


@test
@injectable(
    title=['gsahPsVoucherId', 'gsahTotalAmt', 'gsahStatus', 'orderDate', 'gsadProId', 'batchNo', 'gsahEmp', 'gsahDate',
           'psStatus'], session=True)
class TestGetAcceptWarehouseList(YaoudTestCase):
    @inject(data[0])
    def test_get_accept_warehouse_list_by_psStatus(self):
        """psStatus == 0"""
        self.get_accept_warehouse_list()
        self.check_get_accept_warehouse_list_status_code(200)
        self.check_get_accept_warehouse_list_code(0)
        self.check_accept_status('0')

    @inject(data[1])
    def test_get_accept_warehouse_list_by_psStatus(self):
        """psStatus == 1"""
        self.get_accept_warehouse_list()
        self.check_get_accept_warehouse_list_status_code(200)
        self.check_get_accept_warehouse_list_code(0)
        self.check_accept_status('1')

    @inject(data[0])
    def test_expire_token(self):
        self.expire_for_token_for_get_accept_warehouse_list()
        self.check_expire_status_code(200)
        self.check_expire_code(401)

    def expire_for_token_for_get_accept_warehouse_list(self):
        self.res_expire = http.expire_token_for_post(url=url_get_accept_warehouse_list, data={
            "gsahPsVoucherId": self.gsahPsVoucherId,
            "gsahTotalAmt": self.gsahTotalAmt,
            "gsahStatus": self.gsahStatus,
            "orderDate": self.orderDate,
            "gsadProId": self.gsadProId,
            "batchNo": self.batchNo,
            "gsahEmp": self.gsahEmp,
            "gsahDate": self.gsahDate,
            "psStatus": self.psStatus}, session=self.session)

    def check_expire_status_code(self, status_code):
        self.assertEqual(self.res_expire.status_code, status_code)

    def check_expire_code(self, code):
        self.assertEqual(self.res_expire.json().get('code'), code)

    def check_accept_status(self, num):
        """for check the outcome is same as delivery"""
        self.a = self.res_get_accept_warehouse_list.json().get('data')['outData']['acceptStatus']
        # for i in self.outdata:
        #     print(i, type('我是i～～～～～～～'))
        #     a = i.json().get('acceptStatus')
        self.assertEqual(self.a, num)

    def get_accept_warehouse_list(self):
        """the method for get accept warehouse list"""
        self.res_get_accept_warehouse_list = http.post(url=url_get_accept_warehouse_list, data={
            "gsahPsVoucherId": self.gsahPsVoucherId,
            "gsahTotalAmt": self.gsahTotalAmt,
            "gsahStatus": self.gsahStatus,
            "orderDate": self.orderDate,
            "gsadProId": self.gsadProId,
            "batchNo": self.batchNo,
            "gsahEmp": self.gsahEmp,
            "gsahDate": self.gsahDate,
            "psStatus": self.psStatus}, session=self.session)

    def check_get_accept_warehouse_list_status_code(self, status_code):
        """for check res_get_accept_warehouse_list status code"""
        self.assertEqual(self.res_get_accept_warehouse_list.status_code, status_code)

    def check_get_accept_warehouse_list_code(self, code):
        """for check res_get_accept_warehouse_list code"""
        self.assertEqual(self.res_get_accept_warehouse_list.json().get('code'), code)

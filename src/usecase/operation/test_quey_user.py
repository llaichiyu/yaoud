#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
@date 2021年07月27日4:00 下午
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.operation import url_query_user

http = Http()
data = get_data('src/data/csv/operation/query_user.csv')


@test
@injectable(title=[], session=True)
class TestQueryUser(YaoudTestCase):
    @inject(data)
    def test_query_user(self):
        self.query_user()
        self.check_quey_user_status_code(200)
        self.check_quey_user_code(200)

    @inject(data)
    def test_expire_token(self):
        self.expire_token_for_query_user()
        self.check_expire_token_for_query_user_status_code(200)
        self.check_expire_token_for_query_user_code(401)

    def query_user(self):
        self.res_query_user = http.post(url=url_query_user, data={}, session=self.session)

    def check_quey_user_status_code(self, status_code):
        self.assertEqual(self.res_query_user.status_code, status_code)

    def check_quey_user_code(self, code):
        self.assertEqual(self.res_query_user.json().get('code'), code)

    def expire_token_for_query_user(self):
        self.expirt_token_res = http.expire_token_for_post(url=url_query_user, data={}, session=self.session)

    def check_expire_token_for_query_user_status_code(self, status_code):
        self.assertEqual(self.expirt_token_res.status_code, status_code)

    def check_expire_token_for_query_user_code(self, code):
        self.assertEqual(self.expirt_token_res.json().get('code'), code)

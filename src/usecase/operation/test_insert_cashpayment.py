#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.operation import url_insert_cashpayment

http = Http()
data = get_data('src/data/csv/operation/insert_cashpayment.csv')


@test
@injectable(
    title=['clientId', 'gsdhVoucherId', 'gsdhBrId', 'gsdhCheckDate', 'gsdhDepositId', 'gsdhBankId', 'gsdhBankAccount',
           'gsdhDepositAmt', 'gsdhStatus', 'detailOutDataList_clientId', 'detailOutDataList_gsddVoucherId',
           'detailOutDataList_gsdhBrId', 'detailOutDataList_gsddSaleDate', 'detailOutDataList_gsddEmpGroup',
           'detailOutDataList_gsddEmp', 'detailOutDataList_gsddRmbAmt', 'detailOutDataList_indexDetail'],
    session=True)
class TestInsertCashpayment(YaoudTestCase):
    # @inject(data)
    # def test_insert_error(self):
    #     """owing to the interface is wrong, so this interface can not communicate"""
    #     self.insert_cashpayment()
    #     self.check_insert_cashpayment_status_code(200)
    #     self.check_insert_cashpayment_code(0)

    def insert_cashpayment(self):
        """for insert cashpayment"""
        self.res_insert_cashpayment = http.post(url=url_insert_cashpayment, data={
            "clientId": "10000005",
            "gsdhVoucherId": "PD2021001416",
            "gsdhBrId": "10000",
            "gsdhCheckDate": "20210630",
            "gsdhDepositId": "001",
            "gsdhBankId": "bankid_0",
            "gsdhBankAccount": "bk_account_001",
            "gsdhDepositAmt": "1000",
            "gsdhStatus": "0",
            "detailOutDataList": [
                {
                    "clientId": "10000005",
                    "gsddVoucherId": "PD2021001416",
                    "gsddBrId": "10000",
                    "gsddSaleDate": "20210630",
                    "gsddEmpGroup": "0",
                    "gsddEmp": "0",
                    "gsddRmbAmt": "0",
                    "indexDetail": "0"
                }
            ]
        }, session=self.session)

    def check_insert_cashpayment_status_code(self, status_code):
        self.assertEqual(self.res_insert_cashpayment.status_code, status_code)

    def check_insert_cashpayment_code(self, code):
        self.assertEqual(self.res_insert_cashpayment.json().get('code'), code)

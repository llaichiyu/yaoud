#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
@date 2021年07月27日1:35 下午
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.operation import url_select_total_detail

http = Http()
data = get_data('src/data/csv/operation/select_total_detail.csv')


@test
@injectable(title=['pickProId'], session=True)
class TestSelectTotalDetail(YaoudTestCase):
    @inject(data[0], multi=False)
    def test_pick_proId_exists(self):
        """pickproId is exists in db"""
        self.select_total_detail()
        self.check_select_total_detail_status_code(200)
        self.check_select_total_detail_code(0)
        self.check_pick_pro_id('202107130003-01')

    @inject(data[1], multi=False)
    def test_pick_proId_exists(self):
        """pickproId is not exists in db"""
        self.select_total_detail()
        self.check_select_total_detail_status_code(200)
        self.check_select_total_detail_code(0)

    @inject(data[0], multi=False)
    def test_expire_token_for_get_pick_pro_id(self):
        self.expire_token_for_get_pick_pro_id()
        self.check_expire_token_for_get_pick_pro_id_status_code(200)
        self.check_expire_token_for_get_pick_pro_id_code(401)

    @inject(data[0], multi=False)
    def test_None_pick_pro_id(self):
        self.None_pick_pro_id()
        self.check_None_pro_id_status_code(200)
        self.check_None_pro_id_code(0)

    def None_pick_pro_id(self):
        self.res_None_pick_pro_id = http.post(url=url_select_total_detail, data={}, session=self.session)

    def check_None_pro_id_status_code(self, status_code):
        self.assertEqual(self.res_None_pick_pro_id.status_code, status_code)

    def check_None_pro_id_code(self, code):
        self.assertEqual(self.res_None_pick_pro_id.json().get('code'), code)

    def expire_token_for_get_pick_pro_id(self):
        self.res_expire_token = http.expire_token_for_post(url=url_select_total_detail,
                                                           data={'pickProId': self.pickProId}, session=self.session)

    def check_expire_token_for_get_pick_pro_id_status_code(self, status_code):
        self.assertEqual(self.res_expire_token.status_code, status_code)

    def check_expire_token_for_get_pick_pro_id_code(self, code):
        self.assertEqual(self.res_expire_token.json().get('code'), code)

    def check_none(self):
        """for check nothing to get in data"""
        j_res_select = self.res_select.json().get('data').json()
        res_or = j_res_select['orderList']
        self.assertIsNone(obj=res_or)

    def check_pick_pro_id(self, pick_pro_id):
        self.assertEqual(self.res_select.json().get('data').json().get('pickProId'), pick_pro_id)

    def select_total_detail(self):
        self.res_select = http.post(url=url_select_total_detail, data={'pickProId': self.pickProId},
                                    session=self.session)

    def check_select_total_detail_status_code(self, status_code):
        self.assertEqual(self.res_select.status_code, status_code)

    def check_select_total_detail_code(self, code):
        self.assertEqual(self.res_select.json().get('code'), code)

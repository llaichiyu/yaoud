#! /usr/bin/env python3
# -*- coding:utf-8 -*-
import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.operation import url_get_base_info

data = get_data('src/data/csv/operation/get_base_info.csv')
http = Http()


@test
@injectable(title=['type', 'storeId', 'newStoreSize', 'expectDrugSaleProp'], session=True)
class TestGetBaseInfo(YaoudTestCase):
    @inject(data[0])
    def test_get_base_info(self):
        self.get_base_info()
        self.check_get_base_info_status_code(200)
        self.check_get_base_info_code(0)

    @inject(data[0])
    def test_None_type(self):
        self.None_type()
        self.check_None_type_status_code(200)
        self.check_None_type_code(1001)

    @inject(data[0])
    def test_None_storeId(self):
        self.None_storeId()
        self.check_None_storeId_status_code(200)
        self.check_None_storeId_code(1001)

    @inject(data[0])
    def test_None_newStoreSize(self):
        self.None_newStoreSize()
        self.check_None_newStoreSize_status_code(200)
        self.check_None_newStoreSize_code(1001)

    @inject(data[0])
    def test_None_expectDrugSaleProp(self):
        self.None_expectDrugSaleProp()
        self.check_None_expectDrugSaleProp_status_code(200)
        self.check_None_expectDrugSaleProp_code(1001)

    def None_expectDrugSaleProp(self):
        self.res_None_expectDrugSaleProp = http.post(url=url_get_base_info,
                                                     data={'type': self.type, 'storeId': self.storeId,
                                                           'newStoreSize': self.newStoreSize}, session=self.session)

    def check_None_expectDrugSaleProp_status_code(self, status_code):
        self.assertEqual(self.res_None_expectDrugSaleProp.status_code, status_code)

    def check_None_expectDrugSaleProp_code(self, code):
        self.assertEqual(self.res_None_expectDrugSaleProp.json().get('code'), code)

    def None_newStoreSize(self):
        self.res_None_newStoreSize = http.post(url=url_get_base_info, data={'type': self.type, 'storeId': self.storeId,
                                                                            'expectDrugSaleProp': self.expectDrugSaleProp},
                                               session=self.session)

    def check_None_newStoreSize_status_code(self, status_code):
        self.assertEqual(self.res_None_newStoreSize.status_code, status_code)

    def check_None_newStoreSize_code(self, code):
        self.assertEqual(self.res_None_newStoreSize.json().get('code'), code)

    def None_storeId(self):
        self.res_None_storeId = http.post(url=url_get_base_info, data={'type': self.type,
                                                                       'newStoreSize': self.newStoreSize,
                                                                       'expectDrugSaleProp': self.expectDrugSaleProp},
                                          session=self.session)

    def check_None_storeId_status_code(self, status_code):
        self.assertEqual(self.res_None_storeId.status_code, status_code)

    def check_None_storeId_code(self, code):
        self.assertEqual(self.res_None_storeId.json().get('code'), code)

    def None_type(self):
        self.res_None_type = http.post(url=url_get_base_info, data={'storeId': self.storeId,
                                                                    'newStoreSize': self.newStoreSize,
                                                                    'expectDrugSaleProp': self.expectDrugSaleProp},
                                       session=self.session)

    def check_None_type_status_code(self, status_code):
        self.assertEqual(self.res_None_type.status_code, status_code)

    def check_None_type_code(self, code):
        self.assertEqual(self.res_None_type.json().get('code'), code)

    def get_base_info(self):
        self.res_get_base_info = http.post(url=url_get_base_info, data={'type': self.type, 'storeId': self.storeId,
                                                                        'newStoreSize': self.newStoreSize,
                                                                        'expectDrugSaleProp': self.expectDrugSaleProp},
                                           session=self.session)

    def check_get_base_info_status_code(self, status_code):
        self.assertEqual(self.res_get_base_info.status_code, status_code)

    def check_get_base_info_code(self, code):
        self.assertEqual(self.res_get_base_info.json().get('code'), code)

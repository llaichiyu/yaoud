#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.operation import url_query_pro_thirdly
from src.usecase.auth_hub.test_select_france_list import TestSelectFrance
from src.usecase.auth_hub.test_choose_france import TestChooseFrance
from src.url.auth_hub import url_choose_franc

data = get_data('src/data/csv/operation/query_pro_thirdly.csv')
http = Http()


@test
@injectable(title=['content', 'isSpecial', 'mateType', 'pageNum', 'pageSize', 'proSite', 'type'], session=True)
class TestQueryProThirdly(YaoudTestCase):
    @inject(data[0], multi=False)
    def test_normally_get_pro_thirdly(self):
        """0.login
           1.selectFrance
           2.chooseFrance
           2.query_pro_thirdly
        """
        self.cur_user = TestSelectFrance.select_france_list(self)
        self.choose_france()
        self.query_pro_thirdly()
        self.check_query_thirdly_status_code(200)
        self.check_query_thirdly_code(0)

    def choose_france(self):
        """the method for choose france"""
        self.result_choose_france = http.post(url=url_choose_franc, data={'client': '10000005', 'userId': '1071'},
                                              session=self.session)

    def query_pro_thirdly(self):
        """for query pro thirdly"""
        self.result_query_pro_thirdly = http.post(url=url_query_pro_thirdly, data={
            "content": self.content,
            "isSpecial": self.isSpecial,
            "mateType": self.mateType,
            "pageNum": self.pageNum,
            "pageSize": self.pageSize,
            "proSite": self.proSite,
            "type": self.type}, session=self.session)

    def check_query_thirdly_status_code(self, status_code):
        """for check thirdly status code"""
        self.assertEqual(self.result_query_pro_thirdly.status_code, status_code)

    def check_query_thirdly_code(self, code):
        """for check the query of thirdly code"""
        self.assertEqual(self.result_query_pro_thirdly.json().get('code'), code)

#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.operation import url_choose_store

http = Http()

# todo:for choose store
data = get_data('src/data/csv/operation/get_choose_store.csv')


# todo:'userId == 用户','stoCode == 门店','client ==当前用户 '
@test
@injectable(['userId', 'stoCode', 'client'], session=True)
class TestChooseStore(YaoudTestCase):
    @inject(data[0])
    def test_normally_choose_store(self):
        """userId,stoCode,client are normal"""
        self.choose_store()
        self.check_choose_store_status_code(200)
        self.check_choose_store_code(0)

    @inject(data[1])
    def test_blank_userId(self):
        """userId is a blank str"""
        self.choose_store()
        self.check_choose_store_status_code(200)
        self.check_choose_store_code(1001)

    @inject(data[2])
    def test_blank_userId(self):
        """userId is not exists in db"""
        self.choose_store()
        self.check_choose_store_status_code(200)
        self.check_choose_store_code(1001)

    @inject(data[3])
    def test_blank_stoCode(self):
        """stoCode is a blank str"""
        self.choose_store()
        self.check_choose_store_status_code(200)
        self.check_choose_store_code(1001)

    @inject(data[4])
    def test_blank_client(self):
        """client is a blank str"""
        self.choose_store()
        self.check_choose_store_status_code(200)
        self.check_choose_store_code(1001)

    @inject(data[0])
    def test_None_userId(self):
        """userId == None"""
        self.None_userId()
        self.check_None_userId_status_code(200)
        self.check_None_userId_code(500)

    @inject(data[0])
    def test_None_stoCode(self):
        """stoCode == None"""
        self.None_stoCode()
        self.check_None_stoCode_status_code(200)
        self.check_None_stoCode_code(500)

    @inject(data[0])
    def test_None_client(self):
        """client == None"""
        self.None_client()
        self.check_None_client_status_code(200)
        self.check_None_client_code(500)

    def None_client(self):
        """client == None"""
        self.res_None_client = http.post(url=url_choose_store,
                                         data={'userId': self.userId, 'stoCode': self.stoCode},
                                         session=self.session)

    def check_None_client_status_code(self, status_code):
        """for check None client status code"""
        self.assertEqual(self.res_None_client.status_code, status_code)

    def check_None_client_code(self, code):
        """check None client code"""
        self.assertEqual(self.res_None_client.json().get('code'), code)

    def None_stoCode(self):
        """stoCode == None"""
        self.res_None_stoCode = http.post(url=url_choose_store,
                                          data={'userId': self.userId, 'client': self.client},
                                          session=self.session)

    def check_None_stoCode_status_code(self, status_code):
        self.assertEqual(self.res_None_stoCode.status_code, status_code)

    def check_None_stoCode_code(self, code):
        self.assertEqual(self.res_None_stoCode.json().get('code'), code)

    def None_userId(self):
        """userId is None"""
        self.res_None_userId = http.post(url=url_choose_store,
                                         data={'stoCode': self.stoCode, 'client': self.client},
                                         session=self.session)

    def check_None_userId_status_code(self, status_code):
        self.assertEqual(self.res_None_userId.status_code, status_code)

    def check_None_userId_code(self, code):
        self.assertEqual(self.res_None_userId.json().get('code'), code)

    def choose_store(self):
        """for choose store"""
        self.res_choose_store = http.post(url=url_choose_store,
                                          data={'userId': self.userId, 'stoCode': self.stoCode, 'client': self.client},
                                          session=self.session)

    def check_choose_store_status_code(self, status_code):
        """for check choose store status code"""
        self.assertEqual(self.res_choose_store.status_code, status_code)

    def check_choose_store_code(self, code):
        """for check store code"""
        self.assertEqual(self.res_choose_store.json().get('code'), code)

#! /usr/bin/env python3
# -*- coding:utf-8 -*-
import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.operation import url_export

data = get_data('src/data/csv/operation/export.csv')
http = Http()


@test
@injectable(title=['storeId'], session=True)
class TestExport(YaoudTestCase):
    @inject(data[0])
    def test_export_normally(self):
        self.export()
        self.check_export_status_code(200)
        self.check_export_code(0)

    @inject(data[0])
    def test_None_storeId(self):
        self.None_storeId()
        self.check_None_storeId_status_code(200)
        self.check_None_storeId_code(1001)

    @inject(data)
    def test_blank_storeId(self):
        self.blank_storeId()
        self.check_blank_storeId_status_code(200)
        self.check_blank_storeId_code(1001)

    @inject(data[1])
    def test_not_exists_store(self):
        """storeId is not exists in db"""
        self.export()
        self.check_export_status_code(200)
        self.check_export_code(0)
        self.check_blank_data("导出数据为空")

    def check_blank_data(self, data):
        self.assertEqual(self.res_export.json().get('data'), data)

    def blank_storeId(self):
        self.res_blank_storeId = http.post(url=url_export, data={"storeId": ""}, session=self.session)

    def check_blank_storeId_status_code(self, status_code):
        self.assertEqual(self.res_blank_storeId.status_code, status_code)

    def check_blank_storeId_code(self, code):
        self.assertEqual(self.res_blank_storeId.json().get('code'), code)

    def None_storeId(self):
        self.res_None_StoreId = http.post(url=url_export, session=self.session)

    def check_None_storeId_status_code(self, status_code):
        self.assertEqual(self.res_None_StoreId.status_code, status_code)

    def check_None_storeId_code(self, code):
        self.assertEqual(self.res_None_StoreId.json().get('code'), code)

    def export(self):
        self.res_export = http.post(url=url_export, data={'storeId': self.storeId}, session=self.session)

    def check_export_status_code(self, status_code):
        self.assertEqual(self.res_export.status_code, status_code)

    def check_export_code(self, code):
        self.assertEqual(self.res_export.json().get('code'), code)

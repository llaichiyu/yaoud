#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
@date 2021年08月05日3:56 下午
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.operation import url_get_franchisee_by_name

http = Http()
data = get_data('src/data/csv/operation/get_franchisee_by_name.csv')


@test
@injectable(title=['francName'], session=True)
class TestGetFranchiseeByName(YaoudTestCase):
    @inject(data[0])
    def test_normally_get_franchisee_by_name(self):
        """for check normally ger franchisee by name"""
        self.get_franchisee_by_name()
        self.check_get_franchises_by_name_status_code(200)
        self.check_get_franchises_by_name_code(0)

    @inject(data[0])
    def test_None_france_name(self):
        """for check None france name"""
        self.None_francName()
        self.check_None_francName_status_code(200)
        self.check_None_francName_code(0)

    @inject(data[0])
    def test_None_body(self):
        """for check None body"""
        self.None_body()
        self.check_None_body_status_code(200)
        self.check_None_body_code(500)

    @inject(data[0])
    def test_expire_token_for_get(self):
        self.expire_token_for_get()
        self.check_expire_token_for_get_status_code(200)
        self.check_expire_token_for_get_code(401)

    def expire_token_for_get(self):
        self.res_expire_token = http.expire_token_for_post(url=url_get_franchisee_by_name,
                                                           data={'francName': self.francName},
                                                           session=self.session)

    def check_expire_token_for_get_status_code(self, status_code):
        self.assertEqual(self.res_expire_token.status_code, status_code)

    def check_expire_token_for_get_code(self, code):
        self.assertEqual(self.res_expire_token.json().get('code'), code)

    def None_body(self):
        self.res_None_body = http.post(url=url_get_franchisee_by_name, data=None, session=self.session)

    def check_None_body_status_code(self, status_code):
        self.assertEqual(self.res_None_body.status_code, status_code)

    def check_None_body_code(self, code):
        self.assertEqual(self.res_None_body.json().get('code'), code)

    def None_francName(self):
        self.res_None_franc_name = http.post(url=url_get_franchisee_by_name, data={}, session=self.session)

    def check_None_francName_status_code(self, status_code):
        self.assertEqual(self.res_None_franc_name.status_code, status_code)

    def check_None_francName_code(self, code):
        self.assertEqual(self.res_None_franc_name.json().get('code'), code)

    def get_franchisee_by_name(self):
        self.res_get = http.post(url=url_get_franchisee_by_name, data={'francName': self.francName},
                                 session=self.session)

    def check_get_franchises_by_name_status_code(self, status_code):
        """for check status_code"""
        self.assertEqual(self.res_get.status_code, status_code)

    def check_get_franchises_by_name_code(self, code):
        """for check status_code"""
        self.assertEqual(self.res_get.json().get('code'), code)

#! /usr/bin/env python3
# -*- coding:utf-8 -*-
import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.operation import url_get_cal_list

data = get_data('src/data/csv/operation/get_cal_list.csv')
http = Http()


@test
@injectable(title=['storeIdList', 'drugsDistributionCount', 'storeId'], session=True)
class TestGetCalList(YaoudTestCase):
    @inject(data[0])
    def test_get_cal_list_normally(self):
        self.get_cal_list()
        self.check_get_cal_list_status_code(200)
        self.check_get_cal_list_code(0)

    @inject(data[0])
    def test_blank_storeId(self):
        self.get_cal_list()
        self.check_get_cal_list_status_code(200)
        self.check_get_cal_list_code(1001)

    @inject(data[0])
    def test_None_drugsDistributionCount(self):
        self.None_drugsDistributionCount()
        self.check_None_drugsDistributionCount_status_code(200)
        self.check_None_drugsDistributionCount_code(1001)

    @inject(data[0])
    def test_None_storeId(self):
        self.None_storeId()
        self.check_None_storeId_status_code(200)
        self.check_None_storeId_code(1001)

    def None_storeId(self):
        self.res_None_storeId = http.post(url=url_get_cal_list, data={'storeIdList': self.storeIdList,
                                                                      'drugsDistributionCount': self.drugsDistributionCount},
                                          session=self.session)

    def check_None_storeId_status_code(self, status_code):
        self.assertEqual(self.res_None_storeId.status_code, status_code)

    def check_None_storeId_code(self, code):
        self.assertEqual(self.res_None_storeId.json().get('code'), code)

    def None_drugsDistributionCount(self):
        self.res_None_drugsDistributionCount = http.post(url=url_get_cal_list, data={'storeIdList': self.storeIdList,
                                                                                     'storeId': self.storeId},
                                                         session=self.session)

    def check_None_drugsDistributionCount_status_code(self, status_code):
        self.assertEqual(self.res_None_drugsDistributionCount.status_code, status_code)

    def check_None_drugsDistributionCount_code(self, code):
        self.assertEqual(self.res_None_drugsDistributionCount.json().get('code'), code)

    def get_cal_list(self):
        self.res_get_cal_list = http.post(url=url_get_cal_list, data={'storeIdList': self.storeIdList,
                                                                      'drugsDistributionCount': self.drugsDistributionCount,
                                                                      'storeId': self.storeId},
                                          session=self.session)

    def check_get_cal_list_status_code(self, status_code):
        self.assertEqual(self.res_get_cal_list.status_code, status_code)

    def check_get_cal_list_code(self, code):
        self.assertEqual(self.res_get_cal_list.json().get('code'), code)

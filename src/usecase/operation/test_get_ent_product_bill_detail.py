#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
@date 2021年08月09日3:28 下午
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.operation import url_get_ent_product_bill_detail

http = Http()
data = get_data('src/data/csv/operation/get_ent_product_bill_detail.csv')


@test
@injectable(title=['billCode', 'dealStatus'], session=True)
class TestGetEntProductBillDetail(YaoudTestCase):
    @inject(data[0])
    def test_get_ent_product_bill_detail(self):
        self.get_ent_product_bill_detail()
        self.check_get_ent_product_bill_detail_status_code(200)
        self.check_get_ent_product_bill_detail_code(0)

    def get_ent_product_bill_detail(self):
        self.res_get_bill_detail = http.post(url=url_get_ent_product_bill_detail,
                                             data={'billCode': self.billCode, 'dealStatus': self.dealStatus},
                                             session=self.session)

    def check_get_ent_product_bill_detail_status_code(self, status_code):
        self.assertEqual(self.res_get_bill_detail.status_code, status_code)

    def check_get_ent_product_bill_detail_code(self, code):
        self.assertEqual(self.res_get_bill_detail.json().get('code'), code)

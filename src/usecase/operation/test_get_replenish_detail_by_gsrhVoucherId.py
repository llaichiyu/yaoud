#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.operation import url_get_detail_by_gsrhVoucherId

data = get_data('src/data/csv/operation/get_replenish_detail_by_gsrhVoucherId.csv')
http = Http()


@test
@injectable(title=['gsrhVoucherId'], session=True)
class TestGetReplenishByVourcherId(YaoudTestCase):
    @inject(data[0])
    def test_replenish_by_vourcherId(self):
        """0.login to get token for choose_store()
           1.choose_store() to get token for deliver token to relevant request"""
        self.get_replenish_by_vourcherId()
        self.check_replenish_by_vourcherId_status_code(200)
        self.check_replenish_by_vourcherId_code(0)

    def get_replenish_by_vourcherId(self):
        """for get replenish detail by gsrhvoucherId"""
        self.res_get_replenish_detail_by_vourcherId = http.post(url=url_get_detail_by_gsrhVoucherId,
                                                                data={
                                                                    'gsrhVoucherId': self.gsrhVoucherId},
                                                                session=self.session)

    def check_replenish_by_vourcherId_status_code(self, status_code):
        self.assertEqual(self.res_get_replenish_detail_by_vourcherId.status_code, status_code)

    def check_replenish_by_vourcherId_code(self, code):
        self.assertEqual(self.res_get_replenish_detail_by_vourcherId.json().get('code'), code)

#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.operation import url_get_member, url_choose_store

data = get_data('src/data/csv/operation/get_member.csv')
http = Http()


@test
@injectable(['phoneOrCardId', 'brId', 'cardId', 'clientId', 'givePoint', 'memberId', 'phone'], session=True)
class TestGetMember(YaoudTestCase):
    @inject(data[0])
    def test_get_member_normally(self):
        """phoneOrCardId is exists in db && brId is exists in db"""
        self.choose_store()
        self.get_member()
        self.check_get_member_status_code(200)
        self.check_get_member_code(0)
        # self.check_phone_is_exists_in_data(17900000000)

    def choose_store(self):
        """for mock choose store for java fx"""
        self.result_choose_store = http.post(url=url_choose_store, data={"userId": "1005",
                                                                         "stoCode": "10000",
                                                                         "client": "10000005"}, session=self.session)

    def get_member(self):
        """for get member"""
        self.result_get_member = http.post(url=url_get_member,
                                           data={'phoneOrCardId': self.phoneOrCardId, 'brId': self.brId},
                                           session=self.session)

    def check_get_member_status_code(self, status_code):
        """for get member status code"""
        self.assertEqual(self.result_get_member.status_code, status_code)

    def check_get_member_code(self, code):
        """for get member code"""
        self.assertEqual(self.result_get_member.json().get('code'), code)

    # todo：这是一个有问题的方法，NoneType object is not subscriptable
    def check_phone_is_exists_in_data(self, phone):
        """for check json owing to the outcome of select maybe None"""
        self.result_data_json = self.result_get_member.json()
        self.phone = self.result_data_json['data']['phone']
        self.assertEqual(self.phone, phone)

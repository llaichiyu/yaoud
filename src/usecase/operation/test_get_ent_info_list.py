#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
@date 2021年08月10日1:52 下午
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.operation import url_get_list_ent_product_bill

http = Http()
data = get_data('src/data/csv/operation/get_ent_info_list.csv')


@test
@injectable(title=['billCode', 'dealStatus', 'status', 'startTime', 'endTime'], session=True)
class TestGetEntInfoList(YaoudTestCase):
    @inject(data[0])
    def test_get_ent_info_list(self):
        self.get_ent_info_list()
        self.check_get_ent_info_list_status_code(200)
        self.check_get_ent_info_list_code(0)

    @inject(data[0])
    def test_expire_token_for_get_ent_info_list(self):
        self.expire_token_for_get_ent_info_list()
        self.check_expire_token_for_get_ent_info_list_status_code(200)
        self.check_expire_token_for_get_ent_info_list_code(401)

    def expire_token_for_get_ent_info_list(self):
        self.res_expire_token = http.expire_token_for_post(url=url_get_list_ent_product_bill,
                                                           data={'billCode': self.billCode,
                                                                 'dealStatus': self.dealStatus, 'status': self.status,
                                                                 'startTime': self.startTime, 'endTime': self.endTime},
                                                           session=self.session)

    def check_expire_token_for_get_ent_info_list_status_code(self, status_code):
        self.assertEqual(self.res_expire_token.status_code, status_code)

    def check_expire_token_for_get_ent_info_list_code(self, code):
        self.assertEqual(self.res_expire_token.json().get('code'), code)

    def get_ent_info_list(self):
        self.res_get_ent_info_list = http.post(url=url_get_list_ent_product_bill, data={'billCode': self.billCode,
                                                                                        'dealStatus': self.dealStatus,
                                                                                        'status': self.status,
                                                                                        'startTime': self.startTime,
                                                                                        'endTime': self.endTime},
                                               session=self.session)

    def check_get_ent_info_list_status_code(self, status_code):
        self.assertEqual(self.res_get_ent_info_list.status_code, status_code)

    def check_get_ent_info_list_code(self, code):
        self.assertEqual(self.res_get_ent_info_list.json().get('code'), code)

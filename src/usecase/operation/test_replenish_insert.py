#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.operation import url_replenish_insert
from src.url.auth_hub import url_login
from src.url.operation import url_choose_store

http = Http()
data = get_data('src/data/csv/operation/replenish_insert.csv')


@test
@injectable(
    ['gsrhPattern', 'gsrhType', 'gsrhDate', 'detailList_XID', 'detailList_voucherAmt',
     'detailList_proStorageArea',
     'detailList_proMidPackage',
     'detailList_noPurchase', 'detailList_minClass', 'detailList_midClass', 'detailList_matTotalQty',
     'detailList_matTotalAmt', 'detailList_matRateAmt', 'detailList_matMovPrice', 'detailList_isSpecial',
     'detailList_isMed', 'detailList_isChineseMedicine', 'detailList_gsrdTaxTate', 'detailList_gsrdStockStore',
     'detailList_gsrdStockDepot', 'detailList_gsrdSerial', 'detailList_gsrdSalesDays3',
     'detailList_gsrdSalesDays2', 'detailList_gsrdSalesDays1', 'detailList_gsrdProposeQty',
     'detailList_gsrdProUnit', 'detailList_gsrdProPrice', 'detailList_gsrdProPlace', 'detailList_gsrdProPec',
     'detailList_gsrdProName', 'detailList_gsrdProId', 'detailList_gsrdProCompany',
     'detailList_gsrdProCommonName', 'detailList_gsrdPegisterNo', 'detailList_gsrdPayTerm',
     'detailList_gsrdPackMidsize', 'detailList_gsrdNeedQty', 'detailList_gsrdLastSupid',
     'detailList_gsrdLastSupName', 'detailList_gsrdLastPrice', 'detailList_gsrdInputTaxValue',
     'detailList_gsrdInputTax', 'detailList_gsrdDisplayMin', 'detailList_checkOutAmt', 'detailList_bigClass'],
    session=True)
class TestReplenishInsert(YaoudTestCase):
    # @inject(data)
    def test_replenish_insert(self):
        self.replenish_insert()
        self.check_replenish_insert_status_code(200)
        self.check_replenish_insert_code(0)

    def replenish_insert(self):
        """for replenish insert"""
        self.result_replenish = http.post(url=url_replenish_insert, data={
            "detailList": [{
                "addAmt": "",
                "addRate": "",
                "allStoreStock": None,
                "apAddAmt": "",
                "apAddRate": "",
                "bigClass": "1-处方药",
                "checkOutAmt": "0",
                "clientId": "",
                "dcCode": "",
                "gsrdAverageCost": None,
                "gsrdBrId": "",
                "gsrdBrName": "",
                "gsrdDate": "",
                "gsrdDisplayMin": "2",
                "gsrdDnQty": "",
                "gsrdFlag": "",
                "gsrdGrossMargin": None,
                "gsrdInputTax": "J7",
                "gsrdInputTaxValue": "13%",
                "gsrdLastPrice": "3.0000",
                "gsrdLastSupName": "三株福尔制药有限公司1",
                "gsrdLastSupid": "GYS031103",
                "gsrdMinPrice": "",
                "gsrdMinSupName": "",
                "gsrdMinSupid": "",
                "gsrdNeedQty": "0",
                "gsrdPackMidsize": "1",
                "gsrdPayTerm": "",
                "gsrdPegisterNo": "",
                "gsrdProCommonName": "非洛地平缓释片",
                "gsrdProCompany": "阿斯利康制药有限公司",
                "gsrdProId": "01000000002",
                "gsrdProLife": "",
                "gsrdProName": "波依定",
                "gsrdProPec": "2.5mg*10片",
                "gsrdProPlace": "上海",
                "gsrdProPrice": "25.1000",
                "gsrdProUnit": "盒",
                "gsrdProposeQty": "0",
                "gsrdSalesDays1": "1",
                "gsrdSalesDays2": "0",
                "gsrdSalesDays3": "0",
                "gsrdSerial": 1,
                "gsrdStockDepot": "300.0000",
                "gsrdStockStore": "3",
                "gsrdSupName": "",
                "gsrdSupid": "",
                "gsrdTaxTate": "13%",
                "gsrdVoucherId": "",
                "gsrdpProForm": "",
                "index": None,
                "isChineseMedicine": "西药",
                "isMed": "✔",
                "isSpecial": "0",
                "matMovPrice": "0.2212",
                "matRateAmt": "2.5597",
                "matTotalAmt": "19.6903",
                "matTotalQty": "89.0000",
                "midClass": "103-高血压用药",
                "minClass": "10303-10303高血压用药-钙通道拮抗剂",
                "noPurchase": "0",
                "poPrice": "",
                "proMidPackage": "1",
                "proNoPurchase": "",
                "proPosition": "",
                "proStorageArea": "14",
                "stoNum": "",
                "supCode1": "",
                "supCode2": "",
                "supCode3": "",
                "supName1": "",
                "supName2": "",
                "supName3": "",
                "supPrice1": "",
                "supPrice2": "",
                "supPrice3": "",
                "supUpdateDate1": "",
                "supUpdateDate2": "",
                "supUpdateDate3": "",
                "voucherAmt": "0.2212",
                "_XID": "row_386"
            }],
            "gsrhDate": "2021-06-15",
            "gsrhType": "1",
            "gsrhVoucherId": "",
            "gsrhPattern": "1"
        }, session=self.session)

    def check_replenish_insert_status_code(self, status_code):
        self.assertEqual(self.result_replenish.status_code, status_code)

    def check_replenish_insert_code(self, code):
        self.assertEqual(self.result_replenish.json().get('code'), code)

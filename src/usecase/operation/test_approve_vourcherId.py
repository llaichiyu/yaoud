#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.operation import url_approve_vourchId, url_choose_store
from src.url.auth_hub import url_login

http = Http()
data = get_data('src/data/csv/operation/approve_vourcherId.csv')


@test
@injectable(['gsrhVoucherId', 'gsrhEmp'], session=True)
class TestApproveVourcherId(YaoudTestCase):
    @inject(data[0])
    def test_approve_vourcherId(self):
        """vourcherId has been approved"""
        self.approve_vourcherId()
        self.check_approve_status_code(200)
        self.check_approve_code(1001)

    def approve_vourcherId(self):
        """approve vourcherId"""
        self.result_approve = http.post(url=url_approve_vourchId,
                                        data={'gsrhVoucherId': self.gsrhVoucherId, 'gsrhEmp': self.gsrhEmp},
                                        session=self.session)

    def check_approve_status_code(self, status_code):
        """for check status code"""
        self.assertEqual(self.result_approve.status_code, status_code)

    def check_approve_code(self, code):
        """for check code"""
        self.assertEqual(self.result_approve.json().get('code'), code)

#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.operation import url_allocation

http = Http()
data = get_data('src/data/csv/operation/allocation.csv')


# todo:补货 》新增 》紧急补货 》输入补货量 》审核》物流管理》分配》开单执行 》出货管理 》调拨开单 》拣货释放》拣货作业》拣货结案》发货过账》收货分为（0.）》调取配送单


@test
@injectable(['flag'], session=True)
class TestAllocation(YaoudTestCase):
    @inject(data[0])
    def test_allocation(self):
        """get_flag == false"""
        self.allocation()
        self.check_allocation_status_code(200)
        self.check_allocation_code(0)

    @inject(data[1])
    def test_allocation(self):
        """get_flag == true"""
        self.allocation()
        self.check_allocation_status_code(200)
        self.check_allocation_code(0)

    @inject(data[0])
    def test_expire_token_for_get_allocation(self):
        self.expire_token_for_get_allocation()
        self.check_expire_token_for_allocation_status_code(200)
        self.check_expire_token_for_allocation_code(401)

    @inject(data[0])
    def test_None_flag(self):
        """flag == None"""
        self.None_flag()
        self.check_None_flag_status_code(200)
        self.check_None_flag_code(500)

    def None_flag(self):
        self.res_None_flag = http.post(url=url_allocation, data={}, session=self.session)

    def check_None_flag_status_code(self, status_code):
        self.assertEqual(self.res_None_flag.status_code, status_code)

    def check_None_flag_code(self, code):
        self.assertEqual(self.res_None_flag.json().get('code'), code)

    def expire_token_for_get_allocation(self):
        self.res_expire_token = http.expire_token_for_post(url=url_allocation, data={'flag': self.flag},
                                                           session=self.session)

    def check_expire_token_for_allocation_status_code(self, status_code):
        self.assertEqual(self.res_expire_token.status_code, status_code)

    def check_expire_token_for_allocation_code(self, code):
        self.assertEqual(self.res_expire_token.json().get('code'), code)

    def allocation(self):
        """for allocation"""
        self.result_allocation = http.post(url=url_allocation, data={'flag': self.flag}, session=self.session)

    def check_allocation_status_code(self, status_code):
        """for check allocation status code"""
        self.assertEqual(self.result_allocation.status_code, status_code)

    def check_allocation_code(self, code):
        """for check code"""
        self.assertEqual(self.result_allocation.json().get('code'), code)

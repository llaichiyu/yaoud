#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.operation import url_replenish

data = get_data('src/data/csv/operation/replenish_or_not.csv')
http = Http()


@test
@injectable(['gsrhPattern'], session=True)
class TestReplenish(YaoudTestCase):
    @inject(data[0])
    def test_replenish_normally(self):
        """gsrhPattern == 1"""
        self.replenish()
        self.check_replenish_status_code(200)
        self.check_replenish_code(0)

    def replenish(self):
        """
        for replenish the method"""
        self.result_replenish = http.post(url=url_replenish, data={'gsrhPattern': self.gsrhPattern},
                                          session=self.session)

    def check_replenish_status_code(self, status_code):
        """ for check replenish status code"""
        self.assertEqual(self.result_replenish.status_code, status_code)

    def check_replenish_code(self, code):
        """ for check replenish code"""
        self.assertEqual(self.result_replenish.json().get('code'), code)

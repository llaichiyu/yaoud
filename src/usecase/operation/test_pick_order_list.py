#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
@date 2021年07月27日2:56 下午
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.operation import url_pick_order_list

http = Http()
# data = get_data('src/data/csv/operation/pick_order_list.csv')


@test
@injectable(title=[], session=True)
class TestPickOrderList(YaoudTestCase):
    # @inject(data[0])
    def test_pick_normally(self):
        self.pick_order_list()
        self.check_pick_order_list_status_code(200)
        self.check_pick_order_list_code(0)

    # @inject(data[0])
    def test_expire_token_for_pick_order_list(self):
        self.expire_token_for_pick_order_list()
        self.check_expire_token_for_pick_order_list_status_code(200)
        self.check_expire_token_for_pick_order_list_code(401)

    def pick_order_list(self):
        self.res_pick = http.post(url=url_pick_order_list, data={}, session=self.session)

    def check_pick_order_list_status_code(self, status_code):
        self.assertEqual(self.res_pick.status_code, status_code)

    def check_pick_order_list_code(self, code):
        self.assertEqual(self.res_pick.json().get('code'), code)

    def expire_token_for_pick_order_list(self):
        self.res_expire_token = http.post(url=url_pick_order_list, data={}, session=self.session)

    def check_expire_token_for_pick_order_list_status_code(self, status_code):
        self.assertEqual(self.res_expire_token.status_code, status_code)

    def check_expire_token_for_pick_order_list_code(self, code):
        self.assertEqual(self.res_expire_token.json().get('code'), code)

#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.operation import url_add_member, url_choose_store
from src.utils.sql_class import ExecuteDb
from src.sql.operation.del_new_member import del_new_member_sql
import unittest

"""
0. login()
1. choose_store()
2. add_member()
"""

http = Http()
data = get_data('src/data/csv/operation/add_member.csv')


@test
@injectable(
    ['gmbCardClass', 'gmbCardId', 'gmbMobile', 'storedValueCard'],
    session=True)
class TestAddMember(YaoudTestCase):
    @inject(data[0])
    def test_add_a_new_member(self):
        """for add a new member"""
        self.choose_store()
        self.add_member()
        self.check_add_member_status_code(200)
        self.check_add_member_code(0)
        self.del_member_from_GAIA_SD_MEMBER_CARD()

    @unittest.skip(reason='this method maybe not play')
    @inject(data[1])
    def test_duplicate_gmbCardId(self):
        """gmbCardId is exists in db"""
        self.choose_store()
        self.add_member()
        self.check_add_member_status_code(200)
        self.check_add_member_code(1001)

    @unittest.skip(reason='the same reason as above')
    @inject(data[2])
    def test_duplicate_gmbMobile(self):
        """gmbMobile is exists in db"""
        self.choose_store()
        self.add_member()
        self.check_add_member_status_code(200)
        self.check_add_member_code(1001)

    @unittest.skip(reason='暂时不执行该条用例')
    # todo:预留一个case为 "storedValueCard"
    @inject(data[0])
    def test_storedValueCard(self):
        """storedValueCard is not defined districtly by developer"""
        self.choose_store()
        self.add_member()
        self.check_add_member_status_code(200)
        self.check_add_member_code(0)

    def del_member_from_GAIA_SD_MEMBER_CARD(self):
        """for del a new member in table GAIA_SD_MEMBER_CARD"""
        sql = del_new_member_sql
        a = ExecuteDb()
        a.execute_sql(command='delete', sql=sql)

    def choose_store(self):
        """because we need get an client,so we need to use this interface"""
        self.res_choose_store = http.post(url=url_choose_store,
                                          data={'userId': '1005', 'stoCode': '10000', 'client': '10000005'},
                                          session=self.session)

    def add_member(self):
        """the method for add member"""
        self.res_add_member = http.post(url=url_add_member,
                                        data={'gmbCardClass': self.gmbCardClass, 'gmbCardId': self.gmbCardId,
                                              'gmbMobile': self.gmbMobile, 'storedValueCard': self.storedValueCard},
                                        session=self.session)

    def check_add_member_status_code(self, status_code):
        """for check add member status code"""
        self.assertEqual(self.res_choose_store.status_code, status_code)

    def check_add_member_code(self, code):
        """for check add member code"""
        self.assertEqual(self.res_choose_store.json().get('code'), code)

#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.operation import url_get_store_list

data = get_data('src/data/csv/operation/get_store_list.csv')
http = Http()


@test
@injectable(title=['voucherId', 'nameOrCode', 'batchNo', 'supNameOrCode', 'startDate', 'endDate', 'brIdList'],
            session=True)
class TestGetStoreList(YaoudTestCase):
    @inject(data[0])
    def test_get_store_list(self):
        """from startdate to enddate"""
        self.get_store_list()
        self.check_get_store_list_status_code(200)
        self.check_get_store_list_code(0)

    def get_store_list(self):
        """for get store list"""
        self.res_get_store_list = http.post(url=url_get_store_list, data={
            "voucherId": self.voucherId,
            "nameOrCode": self.nameOrCode,
            "batchNo": self.batchNo,
            "supNameOrCode": self.supNameOrCode,
            "startDate": self.startDate,
            "endDate": self.endDate,
            "brIdList": self.brIdList}, session=self.session)

    def check_get_store_list_status_code(self, status_code):
        self.assertEqual(self.res_get_store_list.status_code, status_code)

    def check_get_store_list_code(self, code):
        self.assertEqual(self.res_get_store_list.json().get('code'), code)

#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.operation import url_showWareHouseStock

http = Http()
data = get_data('src/data/csv/operation/showWareHouseStock.csv')


@test
@injectable(session=True)
class TestShowWareHouseStock(YaoudTestCase):
    @inject(data)
    def test_showwarehousestock(self):
        """we have no param"""
        self.showwarehousestock()
        self.check_showwarehousestock_statuc_code(200)
        self.check_showwarehousestock_code(0)

    def showwarehousestock(self):
        """the method for showwarehousestock"""
        self.result_showwarehousestock = http.post(url=url_showWareHouseStock, data={}, session=self.session)

    def check_showwarehousestock_statuc_code(self, status_code):
        """the method for check status code"""
        self.assertEqual(self.result_showwarehousestock.status_code, status_code)

    def check_showwarehousestock_code(self, code):
        """for check code"""
        self.assertEqual(self.result_showwarehousestock.json().get('code'), code)

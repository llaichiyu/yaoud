#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
@date 2021年07月27日10:05 上午
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.operation import url_get_tax

http = Http()
data = get_data('src/data/csv/operation/get_tax.csv')


@test
@injectable(title=['codeClass'], session=True)
class TestGetTax(YaoudTestCase):

    @inject(data[0])
    def test_get_tax_in(self):
        """进项税"""
        self.get_tax()
        self.check_get_tax_status_code(200)
        self.check_get_tax_status_code(0)

    @inject(data[1])
    def test_get_tax_in(self):
        """销项税"""
        self.get_tax()
        self.check_get_tax_status_code(200)
        self.check_get_tax_code(0)

    @inject(data[2])
    def test_get_tax_out_of_range(self):
        """out of range of tax"""
        self.get_tax()
        self.check_get_tax_status_code(200)
        self.check_get_tax_code(0)

    @inject(data[0])
    def test_expire_token_for_get_tax(self):
        """expire for token"""
        self.expire_token_for_get_tax()
        self.check_expire_token_for_get_tax_status_code(200)
        self.check_expire_token_for_get_tax_code(401)

    @inject(data[0])
    def test_None_codeClass(self):
        """codeClass == None"""
        self.None_codeClass()
        self.check_None_codeClass_status_code(200)
        self.check_None_codeClass_code(0)

    def get_tax(self):
        self.res_get_tax = http.post(url=url_get_tax, data={'codeClass': self.codeClass}, session=self.session)

    def check_get_tax_status_code(self, status_code):
        self.assertEqual(self.res_get_tax.status_code, status_code)

    def check_get_tax_code(self, code):
        self.assertEqual(self.res_get_tax.json().get('code'), code)

    def expire_token_for_get_tax(self):
        self.res_expire_token = http.expire_token_for_post(url=url_get_tax, data={'codeClass': self.codeClass},
                                                           session=self.session)

    def check_expire_token_for_get_tax_status_code(self, status_code):
        self.assertEqual(self.res_expire_token.status_code, status_code)

    def check_expire_token_for_get_tax_code(self, code):
        self.assertEqual(self.res_expire_token.json().get('code'), code)

    def None_codeClass(self):
        self.res_None_codeClass = http.post(url=url_get_tax, data={}, session=self.session)

    def check_None_codeClass_status_code(self, status_code):
        self.assertEqual(self.res_None_codeClass.status_code, status_code)

    def check_None_codeClass_code(self, code):
        self.assertEqual(self.res_None_codeClass.json().get('code'), code)

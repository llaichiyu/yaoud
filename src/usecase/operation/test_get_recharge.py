#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

# todo:java fx 的接口
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.operation import url_recharge

data = get_data('src/data/csv/operation/get_recharge.csv')
http = Http()


@test
@injectable(['queryParam'], session=True)
class TestGetRecharge(YaoudTestCase):
    @inject(data[0])
    def test_get_recharge(self):
        """all params are necessary
        """
        self.get_recharge()
        self.check_get_recharge_status_code(200)
        self.check_get_recharge_code(0)

    def get_recharge(self):
        """for get recharge"""
        self.res_recharge = http.post(url=url_recharge, data={'queryParam': self.queryParam}, session=self.session)

    def check_get_recharge_status_code(self, status_code):
        """for check status code"""
        self.assertEqual(self.res_recharge.status_code, status_code)

    def check_get_recharge_code(self, code):
        """for check code"""
        self.assertEqual(self.res_recharge.json().get('code'), code)

#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.operation import url_get_accept
from src.url.auth_hub import url_select_franc_list

data = get_data('src/data/csv/operation/get_accept_list.csv')
http = Http()


@test
@injectable(['gsahVoucherId', 'gsahTotalAmt', 'gsahStatus', 'orderDate', 'gsadProId', 'batchNo', 'gsahEmp', 'gsahDate'],
            session=True)
class TestGetAcceptList(YaoudTestCase):
    @inject(data[0])
    def test_get_accept_list(self):
        """ 0.login and choose_store for get token()
            1./select_france
            2.get_accept_list
        """
        self.select_fance()
        self.get_accept_list()
        self.check_get_accept_list_status_code(200)
        self.check_get_accept_list_code(0)

    @inject(data[1])
    def test_all_params_blank(self):
        """ all params are blank
        """
        self.select_fance()
        self.get_accept_list()
        self.check_get_accept_list_status_code(200)
        self.check_get_accept_list_code(0)

    @inject(data[2])
    def test_vourcherId_is_not_exists_in_db(self):
        """vourcherId is not exists in db"""
        self.select_fance()
        self.get_accept_list()
        self.check_get_accept_list_status_code(200)
        self.check_total_is_none(0)

    @inject(data[0])
    def test_expire_token(self):
        """token is defined in http_class"""
        self.expire_token_for_get_accept_list()
        self.check_expire_token_for_get_accept_list_status_code(200)
        self.check_expire_token_for_get_accept_list_code(401)

    def check_total_is_none(self, total):
        """owing to vourcherId is not exists in db ,
        so we can get the outcome for data is total equals to 0"""
        self.data = self.result_get_accept_list.json().get('data')
        self.total = self.data['total']
        self.assertEqual(self.total, total)

    def expire_token_for_get_accept_list(self):
        """for token is expire"""
        self.res_expire_token = http.expire_token_for_post(url=url_get_accept,
                                                           data={"gsahVoucherId": self.gsahVoucherId,
                                                                 "gsahTotalAmt": self.gsahTotalAmt,
                                                                 "gsahStatus": self.gsahStatus,
                                                                 "orderDate": self.orderDate,
                                                                 "gsadProId": self.gsadProId,
                                                                 "batchNo": self.batchNo,
                                                                 "gsahEmp": self.gsahEmp, "gsahDate": self.gsahDate},
                                                           session=self.session)

    def check_expire_token_for_get_accept_list_status_code(self, status_code):
        """owing to token is defined in http_class for mock we can not get token"""
        self.assertEqual(self.res_expire_token.status_code, status_code)

    def check_expire_token_for_get_accept_list_code(self, code):
        """the same function as check_expire_token_for_get_accept_list_status_code(),for data code """
        self.assertEqual(self.res_expire_token.json().get('code'), code)

    def select_fance(self):
        """for select france"""
        self.res_select_france = http.post(url=url_select_franc_list, data={}, session=self.session)

    def get_accept_list(self):
        """get accept list """
        self.result_get_accept_list = http.post(url=url_get_accept,
                                                data={"gsahVoucherId": self.gsahVoucherId,
                                                      "gsahTotalAmt": self.gsahTotalAmt,
                                                      "gsahStatus": self.gsahStatus, "orderDate": self.orderDate,
                                                      "gsadProId": self.gsadProId,
                                                      "batchNo": self.batchNo,
                                                      "gsahEmp": self.gsahEmp, "gsahDate": self.gsahDate},
                                                session=self.session)

    def check_get_accept_list_status_code(self, status_code):
        """for check get accept list status code"""
        self.assertEqual(self.result_get_accept_list.status_code, status_code)

    def check_get_accept_list_code(self, code):
        """for check code"""
        self.assertEqual(self.result_get_accept_list.json().get('code'), code)

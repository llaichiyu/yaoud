#! /usr/bin/env python3
# -*- coding:utf-8 -*-
import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.gys_store_web import url_select_export_shop_list

http = Http()
data = get_data('src/data/csv/gys_store_web/select_export_shop_list.csv')


@test
@injectable(title=['shopNo'], session=True)
class TestSelectExportShopList(YaoudTestCase):
    @inject(data[0])
    def test_shopNo_exists(self):
        """shopNo exists in db"""
        self.select_export_shop_list()
        self.check_select_export_shop_list_status_code(200)
        self.check_select_export_shop_list_code(200)

    @inject(data[1])
    def test_shopNo_not_exists(self):
        """shopNo is not exists in db"""
        self.select_export_shop_list()
        self.check_select_export_shop_list_status_code(500)
        self.check_select_export_shop_list_code(500)

    @inject(data[0])
    def test_None_shopNo(self):
        """shopNo == None"""
        self.None_shopNo()
        self.check_None_shopNo_status_code(500)
        self.check_None_shopNo_code(500)

    def None_shopNo(self):
        self.res_None_shopNo = http.post(url=url_select_export_shop_list, session=self.session)

    def check_None_shopNo_status_code(self, status_code):
        self.assertEqual(self.res_None_shopNo.status_code, status_code)

    def check_None_shopNo_code(self, code):
        self.assertEqual(self.res_None_shopNo.json().get('code'), code)

    def select_export_shop_list(self):
        self.res_export_shop_list = http.post(url=url_select_export_shop_list, data={'shopNo': self.shopNo},
                                              session=self.session)

    def check_select_export_shop_list_status_code(self, status_code):
        self.assertEqual(self.res_export_shop_list.status_code, status_code)

    def check_select_export_shop_list_code(self, code):
        self.assertEqual(self.res_export_shop_list.json().get('code'), code)

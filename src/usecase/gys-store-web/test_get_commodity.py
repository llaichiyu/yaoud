#! /usr/bin/env python3
# -*- coding:utf-8 -*-
import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.gys_store_web import url_get_commodity_info

http = Http()
data = get_data('src/data/csv/gys_store_web/get_commodity.csv')


@test
@injectable(title=['commodityInfo', 'shopNo', 'shopInNo'], session=True)
class TestGetCommodity(YaoudTestCase):
    @inject(data[0])
    def test_get_commodity(self):
        self.get_commodity()
        self.check_get_commodity_status_code(200)
        self.check_get_commodity_code(200)

    @inject(data[1])
    def test_not_exists_commodity_info(self):
        self.get_commodity()
        self.check_get_commodity_status_code(400)
        self.check_get_commodity_code(400)

    @inject(data[2])
    def test_blank_shopNo(self):
        self.get_commodity()
        self.check_get_commodity_status_code(400)
        self.check_get_commodity_code(400)

    @inject(data[0])
    def test_None_commodity_info(self):
        self.None_commodity_info()
        self.check_None_commodity_info_status_code(400)
        self.check_None_commodity_info_code(400)

    @inject(data[0])
    def test_None_shopNo(self):
        self.None_shopNo()
        self.check_None_shopNo_status_code(400)
        self.check_None_shopNo_code(400)

    def None_shopNo(self):
        self.res_None_shopNo = http.post(url=url_get_commodity_info,
                                         data={'commodityInfo': self.commodityInfo, 'shopInNo': self.shopInNo},
                                         session=self.session)

    def check_None_shopNo_status_code(self, status_code):
        self.assertEqual(self.res_None_shopNo.status_code, status_code)

    def check_None_shopNo_code(self, code):
        self.assertEqual(self.res_None_shopNo.json().get('code'), code)

    def None_commodity_info(self):
        self.res_None_commodity_info = http.post(url=url_get_commodity_info,
                                                 data={'shopNo': self.shopNo, 'shopInNo': self.shopInNo},
                                                 session=self.session)

    def check_None_commodity_info_status_code(self, status_code):
        self.assertEqual(self.res_None_commodity_info.status_code, status_code)

    def check_None_commodity_info_code(self, code):
        self.assertEqual(self.res_None_commodity_info.json().get('code'), code)

    def get_commodity(self):
        self.res_get_commodity = http.post(url=url_get_commodity_info,
                                           data={'commodityInfo': self.commodityInfo, 'shopNo': self.shopNo,
                                                 'shopInNo': self.shopInNo},
                                           session=self.session)

    def check_get_commodity_status_code(self, status_code):
        self.assertEqual(self.res_get_commodity.status_code, status_code)

    def check_get_commodity_code(self, code):
        self.assertEqual(self.res_get_commodity.json().get('code'), code)

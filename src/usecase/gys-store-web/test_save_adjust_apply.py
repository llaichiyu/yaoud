#! /usr/bin/env python3
# -*- coding:utf-8 -*-
import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.gys_store_web import url_save_adjust_apply

http = Http()
data = get_data('src/data/csv/gys_store_web/save_adjust_apply.csv')


@test
@injectable(title=['foldShopNo', 'exportShopNo', 'adjustApplyList_commodityCode', 'adjustApplyList_quantity',
                   'adjustApplyList_batchNo', 'adjustApplyList_expiryDate'],
            session=True)
class TestSaveAdjustApply(YaoudTestCase):

    @inject(data)
    def test_save_adjust_apply(self):
        self.save_adjust_apply()
        self.check_save_adjust_apply_status_code(200)
        self.check_save_adjust_apply_code(200)
        self.check_assert_true()

    def check_assert_true(self):
        self.assertTrue(expr=self.res_save_adjust_apply.json().get('success'))
        print("assertTrue 没问题")

    def save_adjust_apply(self):
        self.res_save_adjust_apply = http.post(url=url_save_adjust_apply,
                                               data={'foldShopNo': self.foldShopNo, 'exportShopNo': self.exportShopNo,
                                                     'adjustApplyList_commodityCode': self.adjustApplyList_commodityCode,
                                                     'adjustApplyList_quantity': self.adjustApplyList_quantity,
                                                     'adjustApplyList_batchNo': self.adjustApplyList_batchNo,
                                                     'adjustApplyList_expiryDate': self.adjustApplyList_expiryDate},
                                               session=self.session)

    def check_save_adjust_apply_status_code(self, status_code):
        self.assertEqual(self.res_save_adjust_apply.status_code, status_code)

    def check_save_adjust_apply_code(self, code):
        self.assertEqual(self.res_save_adjust_apply.json().get('code'), code)

#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.report import url_choose_user_and_get_list

http = Http()
data = get_data('src/data/csv/report/choose_user_and_get_list.csv')


@test
@injectable(['clientId', 'pageNum', 'pageSize', 'stoCode', 'userId'], session=True)
class TestChooseUserAndGetListMessage(YaoudTestCase):
    @inject(data[0])
    def test_choose_user_and_get_listmessage_normally(self):
        """for change user to '员工编号' == '1071' """
        self.choose_user_and_get_listmessage()
        self.check_choose_user_and_get_listmessage_status_code(200)
        self.check_choose_user_and_get_listmessage_code(0)

    def choose_user_and_get_listmessage(self):
        """for choose user and get listmessge"""
        self.result_choose_user_and_get_listmessage = http.post(url=url_choose_user_and_get_list,
                                                                data={'clientId': self.clientId,
                                                                      'pageNum': self.pageNum,
                                                                      'pageSize': self.pageSize,
                                                                      'stoCode': self.stoCode, 'userId': self.userId},
                                                                session=self.session)

    def check_choose_user_and_get_listmessage_status_code(self, status_code):
        """for check choose user and get listmessages status code"""
        self.assertEqual(self.result_choose_user_and_get_listmessage.status_code, status_code)

    def check_choose_user_and_get_listmessage_code(self, code):
        """for check choose user and get listmessages code"""
        self.assertEqual(self.result_choose_user_and_get_listmessage.json().get('code'), code)

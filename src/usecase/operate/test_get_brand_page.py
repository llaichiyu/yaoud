# #! /usr/bin/env python3
# # -*- coding:utf-8 -*-
# """
# @author: pauline
# @date 2021年08月19日5:00 下午
# """
#
# import requests, json
# from src.base.http_class import Http
# from src.base.run_test import injectable, test, YaoudTestCase, inject
# from src.data.csv import get_data
# from src.url.operate import url_get_brand_page
#
# http = Http()
# data = get_data('src/data/csv/get_brand_page.csv')
#
#
# @test
# @injectable(title=['queryString', 'pageNum', 'pageSize'], session=True)
# class TestGetBrandPage(YaoudTestCase):
#
#     @inject(data[0])
#     def test_get_brand_page(self):
#         self.get_brand_page()
#         self.check_get_brand_page_status_code(200)
#         self.check_get_brand_page_code()
#
#     def get_brand_page(self):
#         """for get brand page"""
#         self.res_get_brand_page = http.post(url=url_get_brand_page,
#                                             data={'queryString': self.queryString, 'pageNum': self.pageNum,
#                                                   'pageSize': self.pageSize},
#                                             session=self.session)
#
#     def check_get_brand_page_status_code(self, status_code):
#         self.assertEqual(self.res_get_brand_page.status_code, status_code)
#
#     def check_get_brand_page_code(self, code):
#         self.assertEqual(self.res_get_brand_page.json().get('code'), code)

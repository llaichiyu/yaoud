#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
@date 2021年08月19日4:11 下午
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.operate import url_get_member_list

http = Http()
data = get_data('src/data/csv/operate/get_member_list.csv')


@test
@injectable(
    title=['commonDate', 'startDate', 'endDate', 'gsmbcCreateDate', 'gsmbcCreateDateStart', 'gsmbcCreateDateEnd',
           'storeList', 'classId', 'sex', 'minAge', 'maxAge', 'memberBirth', 'memberBirthStartDate',
           'memberBirthEndDate', 'score', 'minIntegral', 'constellationType', 'ageLevelType',
           'consumptionPaymentCodeList', 'consume', 'consumeType', 'consumeDate', 'consumeStartDate', 'consumeEndDate',
           'consumptionTimesMin', 'consumptionTimesMax', 'consumptionAmtMin', 'consumptionAmtMax', 'gsmtgId',
           'proSelfCode', 'proName', 'proCompclass', 'proCompclassName', 'proClass', 'gsyGroup', 'proFactoryName',
           'proBrandCode', 'diseaseTag', 'gsmMarketid', 'gsmStore', 'proTag', 'eliminateType', 'eliminateList',
           'gsmsNormal', 'pageNum', 'pageSize', 'total', 'diseaseCodeList', 'gsmsId'],
    session=True)
class TestGetMemberList(YaoudTestCase):
    @inject(data)
    def test_get_member_list(self):
        self.get_member_list()
        self.check_get_member_list_status_code(200)
        self.check_get_member_list_code("0")

    @inject(data)
    def test_None_param(self):
        """for body == None"""
        self.None_param()
        self.check_None_param_status_code(200)
        self.check_None_param_code("0034")

    def None_param(self):
        """for none param"""
        self.res_None_param = http.post(url=url_get_member_list, session=self.session)

    def check_None_param_status_code(self, status_code):
        self.assertEqual(self.res_None_param.status_code, status_code)

    def check_None_param_code(self, code):
        self.assertEqual(self.res_None_param.json().get('code'), code)

    def get_member_list(self):
        """for get member list"""
        self.res_get_member_list = http.post(url=url_get_member_list,
                                             data={"commonDate": ["19950901", "20211231"], "startDate": "19950901",
                                                   "endDate": "20211231", "gsmbcCreateDate": ["19950701", "20211231"],
                                                   "gsmbcCreateDateStart": "19950701", "gsmbcCreateDateEnd": "20211231",
                                                   "storeList": ["10000"], "classId": ["5"], "sex": "1", "minAge": 22,
                                                   "maxAge": 31, "memberBirth": ["0823", "0922"],
                                                   "memberBirthStartDate": "0823", "memberBirthEndDate": "0922",
                                                   "score": "-1", "minIntegral": "0", "maxIntegral": "5000",
                                                   "constellationType": "8", "ageLevelType": "9",
                                                   "consumptionPaymentCodeList": ["5000"], "consume": "4",
                                                   "consumeType": "4", "consumptionTimesMin": "0",
                                                   "consumptionTimesMax": "1000", "consumptionAmtMin": "0",
                                                   "consumptionAmtMax": "10000", "gsmtgId": ["3"],
                                                   "proSelfCode": "01030090009", "proName": "", "proCompclass": "",
                                                   "proCompclassName": "", "proClass": "", "gsyGroup": "",
                                                   "proFactoryName": "", "proBrandCode": "", "diseaseTag": "",
                                                   "gsmMarketid": "", "gsmStore": "", "proTag": "", "eliminateList": [],
                                                   "gsmsNormal": "0", "pageNum": 1, "pageSize": 10, "total": 1,
                                                   "diseaseCode": "", "diseaseName": "", "gsmsId": ""},
                                             session=self.session)

    def check_get_member_list_status_code(self, status_code):
        self.assertEqual(self.res_get_member_list.status_code, status_code)

    def check_get_member_list_code(self, code):
        self.assertEqual(self.res_get_member_list.json().get('code'), code)

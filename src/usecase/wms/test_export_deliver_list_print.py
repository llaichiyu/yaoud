#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
@date 2021年07月21日3:18 下午
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.wms import url_export_deliver_list_print

http = Http()
data = get_data('src/data/csv/wms/export_deliver_list_print.csv')


@test
@injectable(title=[], session=True)
class TestExportDeliverListPrint(YaoudTestCase):

    def test_export_deliver_list_print(self):
        self.export_deliver_list_print()
        self.check_export_normally_status_code(200)

    def test_None_body(self):
        self.None_body()
        self.check_None_body_status_code(500)
        self.check_None_body_code(500)

    def test_None_startRange(self):
        self.None_startRange()
        self.check_None_startRange_status_code(200)

    def None_startRange(self):
        """all body has no startRange cause a error:"""
        self.res_None_startRange = http.post_for_wms(url=url_export_deliver_list_print, data={
            "pageIndex": 1,
            "pageSize": 100,
            "conditions": [
                {
                    "isPrint": [
                        {
                            "endRange": ""
                        }
                    ],
                    "waveNo": [
                        {
                            "startRange": "20210721-001",
                            "endRange": ""
                        }
                    ],
                    "shopName": [
                        {
                            "startRange": "10000",
                            "endRange": ""
                        }
                    ],
                    "deliverNo": [
                        {
                            "startRange": "PY2021001130",
                            "endRange": ""
                        }
                    ],
                    "transportationRoute": [
                        {
                            "startRange": "X800",
                            "endRange": ""
                        }
                    ],
                    "requirementType": [
                        {
                            "startRange": "2",
                            "endRange": ""
                        }
                    ],
                    "pickNo": [
                        {
                            "startRange": "20210721-001-01",
                            "endRange": "20210721-001-02",
                            "v": None
                        }
                    ],
                    "releasedDate": [
                        {
                            "startRange": "20210721",
                            "endRange": "20210721"
                        }
                    ]
                }
            ],
            "total": 1
        }, session=self.session)

    def check_None_startRange_status_code(self, status_code):
        self.assertEqual(self.res_None_startRange.status_code, status_code)


    def None_body(self):
        self.res_None_body = http.post_for_wms(url=url_export_deliver_list_print, data={}, session=self.session)

    def check_None_body_status_code(self, status_code):
        self.assertEqual(self.res_None_body.status_code, status_code)

    def check_None_body_code(self, code):
        self.assertEqual(self.res_None_body.json().get('code'), code)

    def export_deliver_list_print(self):
        self.res_deliver = http.post_for_wms(url=url_export_deliver_list_print, data={
            "pageIndex": 1,
            "pageSize": 100,
            "conditions": [
                {
                    "isPrint": [
                        {
                            "startRange": "0",
                            "endRange": ""
                        }
                    ],
                    "waveNo": [
                        {
                            "startRange": "20210721-001",
                            "endRange": ""
                        }
                    ],
                    "shopName": [
                        {
                            "startRange": "10000",
                            "endRange": ""
                        }
                    ],
                    "deliverNo": [
                        {
                            "startRange": "PY2021001130",
                            "endRange": ""
                        }
                    ],
                    "transportationRoute": [
                        {
                            "startRange": "X800",
                            "endRange": ""
                        }
                    ],
                    "requirementType": [
                        {
                            "startRange": "2",
                            "endRange": ""
                        }
                    ],
                    "pickNo": [
                        {
                            "startRange": "20210721-001-01",
                            "endRange": "20210721-001-02",
                            "v": None
                        }
                    ],
                    "releasedDate": [
                        {
                            "startRange": "20210721",
                            "endRange": "20210721"
                        }
                    ]
                }
            ],
            "total": 1
        }, session=self.session)

    def check_export_normally_status_code(self, status_code):
        self.assertEqual(self.res_deliver.status_code, status_code)

#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.wms import url_create_picking_list
from src.utils.sql_class import ExecuteDb
from src.sql.wms.update_release_status import sql_update_release_or_not_status

http = Http()
data = get_data('src/data/csv/wms/create_picking_list.csv')
sql = ExecuteDb()


@test
@injectable(title=['deliveryNumberList_deliveryNo'], session=True)
class TestCreatePickingList(YaoudTestCase):
    # @inject(data)
    def test_create_picking_list(self):
        self.create_picking_list()
        self.check_create_picking_list_status_code(200)
        self.check_create_picking_list_code(200)
        self.update_release_or_not_status()

    # @inject(data)
    def test_deliveryNo_not_exists_in_db(self):
        self.deliveryNo_not_exists_in_db()
        self.check_deliveryNo_not_exists_in_db_status_code(500)
        self.check_deliveryNo_not_exists_in_db_code(500)

    # @inject(data)
    def test_blank_deliveryNo(self):
        """deliveryNo ==  a blank str"""
        self.blank_deliveryNo()
        self.check_blank_deliveryNo_status_code(500)
        self.check_blank_deliveryNo_code(500)

    # @inject(data)
    def test_has_deliveried(self):
        """deliverNo has been deliveried"""
        self.has_deliveried()
        self.check_has_deliveried_status_code(400)
        self.check_has_deliveried_code(400)

    # @inject(data)
    def test_blank_body(self):
        """body == {}"""
        self.blank_body()
        self.check_blank_body_status_code(500)
        self.check_blank_body_code(500)

    # @inject(data)
    def test_expire_token_create(self):
        """token is out of date"""
        self.expire_token_create()
        self.check_expire_create_picking_list_status_code(200)
        self.check_expire_create_picking_list_code(401)

    # @inject(data)
    def test_wrong_x_site_for_create_picking_list(self):
        self.wrong_x_site_for_create_picking_list()
        self.check_wrong_x_site_for_create_picking_list_status_code(401)
        self.check_wrong_x_site_for_create_picking_list_code(403)

    def wrong_x_site_for_create_picking_list(self):
        self.res_wrong_x_site_for_create_picking_list = http.post_for_unauthorized_wms(url=url_create_picking_list,
                                                                                       data={
                                                                                           "deliveryNumberList": [
                                                                                               {
                                                                                                   "deliveryNo": "PY2021001130"
                                                                                               }
                                                                                           ]
                                                                                       }, session=self.session)

    def check_wrong_x_site_for_create_picking_list_status_code(self, status_code):
        self.assertEqual(self.res_wrong_x_site_for_create_picking_list.status_code, status_code)

    def check_wrong_x_site_for_create_picking_list_code(self, code):
        self.assertEqual(self.res_wrong_x_site_for_create_picking_list.json().get('code'), code)

    def expire_token_create(self):
        self.res_expire_token_create = http.expire_token_for_post_wms(url=url_create_picking_list,
                                                                      data={
                                                                          "deliveryNumberList": [
                                                                              {
                                                                                  "deliveryNo": "PY2021001130"
                                                                              }
                                                                          ]
                                                                      },
                                                                      session=self.session)

    def check_expire_create_picking_list_status_code(self, status_code):
        self.assertEqual(self.res_expire_token_create.status_code, status_code)

    def check_expire_create_picking_list_code(self, code):
        self.assertEqual(self.res_expire_token_create.json().get('code'), code)

    def blank_body(self):
        self.res_blank_body = http.post_for_wms(url=url_create_picking_list,
                                                data={},
                                                session=self.session)

    def check_blank_body_status_code(self, status_code):
        self.assertEqual(self.res_blank_body.status_code, status_code)

    def check_blank_body_code(self, code):
        self.assertEqual(self.res_blank_body.json().get('code'), code)

    def has_deliveried(self):
        self.res_has_deliveried = http.post_for_wms(url=url_create_picking_list,
                                                    data={
                                                        "deliveryNumberList": [
                                                            {
                                                                "deliveryNo": "WP2021000337"
                                                            }
                                                        ]
                                                    },
                                                    session=self.session)

    def check_has_deliveried_status_code(self, status_code):
        self.assertEqual(self.res_has_deliveried.status_code, status_code)

    def check_has_deliveried_code(self, code):
        self.assertEqual(self.res_has_deliveried.json().get('code'), code)

    def blank_deliveryNo(self):
        self.res_blank_deliveryNo = http.post_for_wms(url=url_create_picking_list,
                                                      data={
                                                          "deliveryNumberList": [
                                                              {
                                                                  "deliveryNo": ""
                                                              }
                                                          ]
                                                      },
                                                      session=self.session)

    def check_blank_deliveryNo_status_code(self, status_code):
        self.assertEqual(self.res_blank_deliveryNo.status_code, status_code)

    def check_blank_deliveryNo_code(self, code):
        self.assertEqual(self.res_blank_deliveryNo.json().get('code'), code)

    def deliveryNo_not_exists_in_db(self):
        self.res_deliveryNo_not_exists_in_db = http.post_for_wms(url=url_create_picking_list,
                                                                 data={
                                                                     "deliveryNumberList": [
                                                                         {
                                                                             "deliveryNo": "PY20210011301111111"
                                                                         }
                                                                     ]
                                                                 },
                                                                 session=self.session)

    def check_deliveryNo_not_exists_in_db_status_code(self, status_code):
        self.assertEqual(self.res_deliveryNo_not_exists_in_db.status_code, status_code)

    def check_deliveryNo_not_exists_in_db_code(self, code):
        self.assertEqual(self.res_deliveryNo_not_exists_in_db.json().get('code'), code)

    def create_picking_list(self):
        self.res_create_picking_list = http.post_for_wms(url=url_create_picking_list,
                                                         data={
                                                             "deliveryNumberList": [
                                                                 {
                                                                     "deliveryNo": "PY2021001130"
                                                                 }
                                                             ]
                                                         },
                                                         session=self.session)

    def check_create_picking_list_status_code(self, status_code):
        self.assertEqual(self.res_create_picking_list.status_code, status_code)

    def check_create_picking_list_code(self, code):
        self.assertEqual(self.res_create_picking_list.json().get('code'), code)

    def update_release_or_not_status(self):
        update = sql.execute_sql(command='update', sql=sql_update_release_or_not_status)
        return update

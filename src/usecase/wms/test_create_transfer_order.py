#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.wms import url_create_transfer_order

http = Http()
data = get_data('src/data/csv/wms/create_transfer_order.csv')


# todo:"""比较复杂的数据结构我使用直接写死在request中，替代了使用csv去维护,且每一个参数的校验都覆盖到了"""
@test
@injectable(title=[], session=True)
class TestCreateTransferOrder(YaoudTestCase):
    @inject(data)
    def test_create_order_normally(self):
        """这里需要搞一组幂等数据，所以现在是400"""
        self.create_transfer_order()
        self.check_create_status_code(200)
        self.check_create_code(200)

    @inject(data)
    def test_pro_max_quantity_more_than_1(self):
        """proMaxQty >1 and i can get the error:'商品已禁止配送，分配数量请改为0'"""
        pass

    @inject(data)
    def test_not_optional_in_an_order(self):
        """response == -4003"""
        self.not_optional_in_an_order()
        self.check_not_selected_status_code(400)
        self.check_not_selected_code(-4003)

    @inject(data)
    def test_blank_orderNo(self):
        self.blank_orderNo()
        self.check_blank_orderNo_status_code(400)
        self.check_blank_orderNo_code(-4005)

    def blank_orderNo(self):
        self.res_blank_orderNo = http.post_for_wms(url=url_create_transfer_order, data={
            "createTransferOrder": [
                {
                    "demandType": "1",
                    "orderNo": "",
                    "uploadedDate": "20210712",
                    "gsrhTime": "16:32:10",
                    "remark": None,
                    "transportationRoute": "X800",
                    "customerNo": "10000",
                    "customerName": "二寿堂",
                    "customerNo2": "10000",
                    "customerName2": "二寿堂",
                    "storeStockType": "2",
                    "commodityCode": "01000000002",
                    "commodityName": "(波依定)非洛地平缓释片",
                    "specifications": "2.5mg*10片",
                    "unit": "盒",
                    "manufacturer": "阿斯利康制药有限公司",
                    "demandQuantity": 2,
                    "actualDistributionQuantity": "1",
                    "storeStockQuantity": 0,
                    "isRemoved": None,
                    "storeStockDays": 0,
                    "isSatisfied": None,
                    "warehouseStockQuantity": 0,
                    "warehouseAvailableStockQuantity": 0,
                    "stockStatus": "",
                    "maxQuantityOfContainingHemp": None,
                    "distributionByMediumPackage": None,
                    "pleaseOrderNo": "PD2021001434",
                    "openOrderUser": None,
                    "openOrderTime": None,
                    "deliveryNo": None,
                    "orderSerialNo": "1",
                    "onlyKey": "PD20210014341",
                    "poPrice": 10,
                    "gsrhType": "2",
                    "wmCbj": 10,
                    "proLsj": 25.9,
                    "alpAddAmt": None,
                    "alpAddRate": None,
                    "addRate": "",
                    "proMidPackage": "1",
                    "updatePrice": "0",
                    "alpCataloguePrice": None,
                    "hwh": None,
                    "productBatchNo": None,
                    "rowId": "0_0_PD20210014341_"
                }
            ]
        }, session=self.session)

    def check_blank_orderNo_status_code(self, status_code):
        self.assertEqual(self.res_blank_orderNo.status_code, status_code)

    def check_blank_orderNo_code(self, code):
        self.assertEqual(self.res_blank_orderNo.json().get('code'), code)

    def not_optional_in_an_order(self):
        """all columns are not selected in an order"""
        self.res_not_selected = http.post_for_wms(url=url_create_transfer_order, data={
            "createTransferOrder": [
                {
                    "demandType": "0",
                    "orderNo": "PD2021001132",
                    "uploadedDate": "20210329",
                    "gsrhTime": "16:33:14",
                    "remark": None,
                    "transportationRoute": "X800",
                    "customerNo": "10000",
                    "customerName": "二寿堂",
                    "customerNo2": "10000",
                    "customerName2": "二寿堂",
                    "storeStockType": "2",
                    "commodityCode": "02030000207",
                    "commodityName": "(猴头菇)猴头菇",
                    "specifications": "g",
                    "unit": "个",
                    "manufacturer": "苏州天灵中药饮片有限公司",
                    "demandQuantity": 500,
                    "actualDistributionQuantity": 0,
                    "storeStockQuantity": 0,
                    "isRemoved": None,
                    "storeStockDays": 0,
                    "isSatisfied": None,
                    "warehouseStockQuantity": 0,
                    "warehouseAvailableStockQuantity": 0,
                    "stockStatus": "",
                    "maxQuantityOfContainingHemp": None,
                    "distributionByMediumPackage": None,
                    "pleaseOrderNo": "PD2021001132",
                    "openOrderUser": None,
                    "openOrderTime": None,
                    "deliveryNo": None,
                    "orderSerialNo": "1",
                    "onlyKey": "PD20210011321",
                    "poPrice": None,
                    "gsrhType": "2",
                    "wmCbj": None,
                    "proLsj": 0.2,
                    "alpAddAmt": None,
                    "alpAddRate": None,
                    "addRate": "",
                    "proMidPackage": "1",
                    "updatePrice": "0",
                    "alpCataloguePrice": None,
                    "hwh": None,
                    "productBatchNo": None,
                    "rowId": "0_0_PD20210011321_",
                    "background": "lightcoral"
                }
            ]
        }, session=self.session)

    def check_not_selected_status_code(self, status_code):
        self.assertEqual(self.res_not_selected.status_code, status_code)

    def check_not_selected_code(self, code):
        self.assertEqual(self.res_not_selected.json().get('code'), code)

    def pro_max_quantity_more_than_1(self):
        """accroading to the checkAllowCreateTransferOrder() , i can get the wrong outcome for commodity_code"""
        self.res_comm = http.post_for_wms(url=url_create_transfer_order, data={}, session=self.session)

    @inject(data)
    def test_raw_transfer_order(self):
        """raw_data == blank and we can not get create_transfer_order -> isEmpty(createTransferOrder)"""
        self.raw_data()
        self.check_raw_data_status_code(400)
        self.check_raw_data_code(400)

    @inject(data)
    def test_duplicately_create_order(self):
        self.duplicate_create_order()
        self.check_duplicate_status_code(400)
        self.check_duplicate_code(-4003)

    @inject(data)
    def test_expire_token_for_create_order(self):
        self.expire_token_for_create_order()
        self.check_expire_token_for_create_order_status_code(200)
        self.check_expire_token_for_create_order_code(401)

    @inject(data)
    def test_unauthorized_create_order(self):
        self.wrong_x_site_for_create_transfer_order()
        self.check_wrong_x_site_for_create_transfer_order_status_code(401)
        self.check_wrong_x_site_for_create_transfer_order_code(403)

    def wrong_x_site_for_create_transfer_order(self):
        self.res_unauthorized = http.post_for_unauthorized_wms(url=url_create_transfer_order,
                                                               data={
                                                                   "createTransferOrder": [
                                                                       {
                                                                           "demandType": "2",
                                                                           # 20
                                                                           "orderNo": "PD2021001129",
                                                                           # 20
                                                                           "uploadedDate": "20210317",
                                                                           # 40
                                                                           "gsrhTime": None,
                                                                           # 40
                                                                           "remark": None,
                                                                           # 40
                                                                           "transportationRoute": "X800",
                                                                           # 20
                                                                           "customerNo": "10000",
                                                                           # 20
                                                                           "customerName": "二寿堂",
                                                                           # 20
                                                                           "customerNo2": "10000",
                                                                           # 20
                                                                           "customerName2": "二寿堂",
                                                                           # 20
                                                                           "storeStockType": "2",
                                                                           # 20
                                                                           "commodityCode": "02030000123",
                                                                           # 20
                                                                           "commodityName": "(医用外科口罩)医用外科口罩",
                                                                           # 40
                                                                           "specifications": "10片",
                                                                           "unit": "个",
                                                                           "manufacturer": "河南函泰医疗科技有限公司",
                                                                           "demandQuantity": 11,
                                                                           # 20
                                                                           "actualDistributionQuantity": 11,
                                                                           # 30
                                                                           "storeStockQuantity": 0,
                                                                           "isRemoved": None,
                                                                           "storeStockDays": 0,
                                                                           "isSatisfied": None,
                                                                           "warehouseStockQuantity": 0,
                                                                           "warehouseAvailableStockQuantity": 0,
                                                                           "stockStatus": "",
                                                                           "maxQuantityOfContainingHemp": None,
                                                                           "distributionByMediumPackage": "0",
                                                                           "pleaseOrderNo": "PD2021001129",
                                                                           "openOrderUser": None,
                                                                           "openOrderTime": None,
                                                                           "deliveryNo": None,
                                                                           "orderSerialNo": "1",
                                                                           "productBatchNo": "",
                                                                           "onlyKey": "PD20210011291",
                                                                           # 订单号+订单序号
                                                                           "poPrice": 1.8989,
                                                                           # 出库价
                                                                           "gsrhType": "2",
                                                                           "wmCbj": 1.8989,
                                                                           "proLsj": 10,
                                                                           # 零售价
                                                                           "alpAddAmt": None,
                                                                           "alpAddRate": None,
                                                                           "addRate": "",
                                                                           "hwh": "",
                                                                           "proMidPackage": "5",
                                                                           "updatePrice": "0",
                                                                           "alpCataloguePrice": None,
                                                                           "rowId": "0_0_PD20210011291_",
                                                                           "background": "lightgreen"
                                                                       }]},
                                                               session=self.session)

    def check_wrong_x_site_for_create_transfer_order_status_code(self, status_code):
        self.assertEqual(self.res_unauthorized.status_code, status_code)

    def check_wrong_x_site_for_create_transfer_order_code(self, code):
        self.assertEqual(self.res_unauthorized.json().get('code'), code)

    def raw_data(self):
        """raw_data == blank body"""
        self.res_raw_data = http.post_for_wms(url=url_create_transfer_order, data={}, session=self.session)

    def check_raw_data_status_code(self, status_code):
        """for check raw data status_code"""
        self.assertEqual(self.res_raw_data.status_code, status_code)

    def check_raw_data_code(self, code):
        """for check raw data code"""
        self.assertEqual(self.res_raw_data.json().get('code'), code)

    def expire_token_for_create_order(self):
        self.res_expire_token = http.expire_token_for_post(url=url_create_transfer_order, data={
            "createTransferOrder": [
                {
                    "demandType": "2",  # 20
                    "orderNo": "PD2021001129",  # 20
                    "uploadedDate": "20210317",  # 40
                    "gsrhTime": None,  # 40
                    "remark": None,  # 40
                    "transportationRoute": "X800",  # 20
                    "customerNo": "10000",  # 20
                    "customerName": "二寿堂",  # 20
                    "customerNo2": "10000",  # 20
                    "customerName2": "二寿堂",  # 20
                    "storeStockType": "2",  # 20
                    "commodityCode": "02030000123",  # 20
                    "commodityName": "(医用外科口罩)医用外科口罩",  # 40
                    "specifications": "10片",
                    "unit": "个",
                    "manufacturer": "河南函泰医疗科技有限公司",
                    "demandQuantity": 11,  # 20
                    "actualDistributionQuantity": 11,  # 30
                    "storeStockQuantity": 0,
                    "isRemoved": None,
                    "storeStockDays": 0,
                    "isSatisfied": None,
                    "warehouseStockQuantity": 0,
                    "warehouseAvailableStockQuantity": 0,
                    "stockStatus": "",
                    "maxQuantityOfContainingHemp": None,
                    "distributionByMediumPackage": "0",
                    "pleaseOrderNo": "PD2021001129",
                    "openOrderUser": None,
                    "openOrderTime": None,
                    "deliveryNo": None,
                    "orderSerialNo": "1",
                    "productBatchNo": "",
                    "onlyKey": "PD20210011291",  # 订单号+订单序号
                    "poPrice": 1.8989,  # 出库价
                    "gsrhType": "2",
                    "wmCbj": 1.8989,
                    "proLsj": 10,  # 零售价
                    "alpAddAmt": None,
                    "alpAddRate": None,
                    "addRate": "",
                    "hwh": "",
                    "proMidPackage": "5",
                    "updatePrice": "0",
                    "alpCataloguePrice": None,
                    "rowId": "0_0_PD20210011291_",
                    "background": "lightgreen"
                }]}, session=self.session)

    def check_expire_token_for_create_order_status_code(self, status_code):
        self.assertEqual(self.res_expire_token.status_code, status_code)

    def check_expire_token_for_create_order_code(self, code):
        self.assertEqual(self.res_expire_token.json().get('code'), code)

    def create_transfer_order(self):
        """the method for create transfer order"""
        self.res_create_transfer_order = http.post_for_wms(url=url_create_transfer_order, data={
            "createTransferOrder": [
                {
                    "demandType": "2",  # 20
                    "orderNo": "PD2021001129",  # 20
                    "uploadedDate": "20210317",  # 40
                    "gsrhTime": None,  # 40
                    "remark": None,  # 40
                    "transportationRoute": "X800",  # 20
                    "customerNo": "10000",  # 20
                    "customerName": "二寿堂",  # 20
                    "customerNo2": "10000",  # 20
                    "customerName2": "二寿堂",  # 20
                    "storeStockType": "2",  # 20
                    "commodityCode": "02030000123",  # 20
                    "commodityName": "(医用外科口罩)医用外科口罩",  # 40
                    "specifications": "10片",
                    "unit": "个",
                    "manufacturer": "河南函泰医疗科技有限公司",
                    "demandQuantity": 11,  # 20
                    "actualDistributionQuantity": 11,  # 30
                    "storeStockQuantity": 0,
                    "isRemoved": None,
                    "storeStockDays": 0,
                    "isSatisfied": None,
                    "warehouseStockQuantity": 0,
                    "warehouseAvailableStockQuantity": 0,
                    "stockStatus": "",
                    "maxQuantityOfContainingHemp": None,
                    "distributionByMediumPackage": "0",
                    "pleaseOrderNo": "PD2021001129",
                    "openOrderUser": None,
                    "openOrderTime": None,
                    "deliveryNo": None,
                    "orderSerialNo": "1",
                    "productBatchNo": "",
                    "onlyKey": "PD20210011291",  # 订单号+订单序号
                    "poPrice": 1.8989,  # 出库价
                    "gsrhType": "2",
                    "wmCbj": 1.8989,
                    "proLsj": 10,  # 零售价
                    "alpAddAmt": None,
                    "alpAddRate": None,
                    "addRate": "",
                    "hwh": "",
                    "proMidPackage": "5",
                    "updatePrice": "0",
                    "alpCataloguePrice": None,
                    "rowId": "0_0_PD20210011291_",
                    "background": "lightgreen"
                }
            ]
        }, session=self.session)

    def duplicate_create_order(self):
        """the method for create transfer order"""
        self.res_duplicate = http.post_for_wms(url=url_create_transfer_order, data={
            "createTransferOrder": [
                {
                    "demandType": "2",
                    "orderNo": "PD2021001129",
                    "uploadedDate": "20210317",
                    "gsrhTime": None,
                    "remark": None,
                    "transportationRoute": "X800",
                    "customerNo": "10000",
                    "customerName": "二寿堂",
                    "customerNo2": "10000",
                    "customerName2": "二寿堂",
                    "storeStockType": "2",
                    "commodityCode": "02030000123",
                    "commodityName": "(医用外科口罩)医用外科口罩",
                    "specifications": "10片",
                    "unit": "个",
                    "manufacturer": "河南函泰医疗科技有限公司",
                    "demandQuantity": 11,
                    "actualDistributionQuantity": 11,
                    "storeStockQuantity": 0,
                    "isRemoved": None,
                    "storeStockDays": 0,
                    "isSatisfied": None,
                    "warehouseStockQuantity": 0,
                    "warehouseAvailableStockQuantity": 0,
                    "stockStatus": "",
                    "maxQuantityOfContainingHemp": None,
                    "distributionByMediumPackage": "0",
                    "pleaseOrderNo": "PD2021001129",
                    "openOrderUser": None,
                    "openOrderTime": None,
                    "deliveryNo": None,
                    "orderSerialNo": "1",
                    "productBatchNo": "",
                    "onlyKey": "PD20210011291",
                    "poPrice": 1.8989,
                    "gsrhType": "2",
                    "wmCbj": 1.8989,
                    "proLsj": 10,
                    "alpAddAmt": None,
                    "alpAddRate": None,
                    "addRate": "",
                    "hwh": "",
                    "proMidPackage": "5",
                    "updatePrice": "0",
                    "alpCataloguePrice": None,
                    "rowId": "0_0_PD20210011291_",
                    "background": "lightgreen"
                }
            ]
        }, session=self.session)

    def check_duplicate_status_code(self, status_code):
        """for check create_status_code"""
        self.assertEqual(self.res_duplicate.status_code, status_code)

    def check_duplicate_code(self, code):
        """for check create_status_code"""
        self.assertEqual(self.res_duplicate.json().get('code'), code)

    def check_create_status_code(self, status_code):
        """for check create_status_code"""
        self.assertEqual(self.res_create_transfer_order.status_code, status_code)

    def check_create_code(self, code):
        """for check create_status_code"""
        self.assertEqual(self.res_create_transfer_order.json().get('code'), code)

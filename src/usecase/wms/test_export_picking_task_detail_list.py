#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
@date 2021年07月21日1:54 下午
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.wms import url_export_picking_task_detail_list

http = Http()
data = get_data('src/data/csv/wms/export_picking_task_detail_list.csv')


@test
@injectable(title=[], session=True)
class TestExportPickingTaskDetailList(YaoudTestCase):
    def test_export_normally(self):
        self.export_picking_task_detail_list()
        self.check_export_status_code(200)

    def test_None_startRange(self):
        self.None_startRange()
        self.check_None_startRange_status_code(200)

    def test_None_body(self):
        self.None_body()
        self.check_None_body_status_code(500)
        self.check_None_body_code(500)

    def None_body(self):
        self.res_None = http.get_for_wms(url=url_export_picking_task_detail_list, data={},
                                         headers={'content-type': 'application/json'},
                                         session=self.session)

    def check_None_body_status_code(self, status_code):
        self.assertEqual(self.res_None.status_code, status_code)

    def check_None_body_code(self, code):
        self.assertEqual(self.res_None.json().get('code'), code)

    def export_picking_task_detail_list(self):
        self.res_export = http.get_for_wms(url=url_export_picking_task_detail_list, data={
            "conditions": [
                {
                    "releasedDate": [
                        {
                            "startRange": "20210714",
                            "endRange": "20210721"
                        }
                    ],
                    "wmSfJa": [
                        {
                            "startRange": "1",
                            "endRange": ""
                        }
                    ],
                    "isPosted": [
                        {
                            "startRange": "0",
                            "endRange": ""
                        }
                    ]
                }
            ],
            "pageIndex": 1,
            "pageSize": 100,
            "total": 1,
            "sortBy": None,
            "orderBy": None
        }, session=self.session)

    def check_export_status_code(self, status_code):
        self.assertEqual(self.res_export.status_code, status_code)

    def None_startRange(self):
        self.res_absent_startRange = http.get_for_wms(url=url_export_picking_task_detail_list, data={
            "conditions": [
                {
                    "releasedDate": [
                        {
                            "endRange": "20210721"
                        }
                    ],
                    "wmSfJa": [
                        {
                            "startRange": "1",
                            "endRange": ""
                        }
                    ],
                    "isPosted": [
                        {
                            "startRange": "0",
                            "endRange": ""
                        }
                    ]
                }
            ],
            "pageIndex": 1,
            "pageSize": 100,
            "total": 1,
            "sortBy": None,
            "orderBy": None
        }, headers={'content-type': 'application/json'}, session=self.session)

    def check_None_startRange_status_code(self, status_code):
        self.assertEqual(self.res_absent_startRange.status_code, status_code)

    def check_None_startRange_code(self, code):
        self.assertEqual(self.res_absent_startRange.json().get('code'), code)

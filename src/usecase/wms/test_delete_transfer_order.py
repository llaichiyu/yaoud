#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.wms import url_delete_transfer_order

http = Http()
data = get_data('src/data/csv/wms/delete_transfer_order.csv')


@test
@injectable(title=['deliveryNumberList_deliveryNo'], session=True)
class TestdDeleteTransferOrder(YaoudTestCase):
    # @inject(data)
    def test_del_not_exists_delivery_no(self):
        """delivery_no has been deleted"""
        self.delete_transfer_order()
        self.check_delete_transfer_order_status_code(400)
        self.check_delete_transfer_order_code(400)

    # @inject(data)
    def test_delete_raw_body(self):
        """raw body"""
        self.blank_raw_body()
        self.check_blank_raw_body_status_code(200)
        self.check_blank_raw_body_code(200)

    # @inject(data)
    def test_expire_token_for_del(self):
        self.expire_token_for_del_transfer_order()
        self.check_expire_token_for_del_transfer_order_status_code(200)
        self.check_expire_token_for_del_transfer_order_code(401)

    def expire_token_for_del_transfer_order(self):
        self.res_expire_token = http.expire_token_for_post_wms(url=url_delete_transfer_order, data={
            "deliveryNumberList": [
                {
                    "deliveryNo": "PY2021001439"
                }
            ]
        },
                                                               session=self.session)

    def check_expire_token_for_del_transfer_order_status_code(self, status_code):
        self.assertEqual(self.res_expire_token.status_code, status_code)

    def check_expire_token_for_del_transfer_order_code(self, code):
        self.assertEqual(self.res_expire_token.json().get('code'), code)

    def blank_raw_body(self):
        self.res_del_blank = http.post_for_wms(url=url_delete_transfer_order, data={}, session=self.session)

    def check_blank_raw_body_status_code(self, status_code):
        self.assertEqual(self.res_del_blank.status_code, status_code)

    def check_blank_raw_body_code(self, code):
        self.assertEqual(self.res_del_blank.json().get('code'), code)

    def delete_transfer_order(self):
        self.res_delete_transfer_order = http.post_for_wms(url=url_delete_transfer_order, data={
            "deliveryNumberList": [
                {
                    "deliveryNo": "PY2021001439"
                }
            ]
        }, session=self.session)

    def check_delete_transfer_order_status_code(self, status_code):
        self.assertEqual(self.res_delete_transfer_order.status_code, status_code)

    def check_delete_transfer_order_code(self, code):
        self.assertEqual(self.res_delete_transfer_order.json().get('code'), code)

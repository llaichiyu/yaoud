#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject, get_x_site
from src.data.csv import get_data
from src.url.wms import url_get_allocated_stock_by_recommending

data = get_data('src/data/csv/wms/get_allocated_stock_by_recommending.csv')
http = Http()


@test
@injectable(title=['distributionList_orderNo', 'distributionList_orderSerialNo',
                   'distributionList_commodityCode', 'distributionList_demandQuantity', 'distributionList_stockStatus',
                   'distributionList_requestType', 'distributionList_hwh', 'distributionList_productBatchNo'],
            session=True)
class TestGetAllocatedStockByRecommending(YaoudTestCase):
    @inject(data[0])
    def test_get_allocated_by_recommending(self):
        """normally get allocated by recommending"""
        self.get_allocated_stock_by_recommending()
        self.check_get_allocated_stock_by_recommending_status_code(200)
        self.check_get_allocated_stock_by_recommending_code(200)

    @inject(data[0])
    def test_absent_x_site(self):
        """for x_site is absent"""
        self.absent_x_stie()
        self.check_absent_x_site_status_code(401)
        self.check_absent_x_site_code(401)

    @inject(data[0])
    def test_absent_x_token(self):
        """for x_token is absent"""
        self.absent_x_token()
        self.check_absent_x_token_status_code(200)
        self.check_absent_x_token_code(401)

    @inject(data[0])
    def test_x_site_not_exists(self):
        """for x_site is defined as a nonexists person"""
        self.mock_like_a_person()
        self.check_mock_status_code(401)
        self.check_mock_code(403)

    @inject(data[0])
    def test_absent_content_type(self):
        self.absent_headers_of_content_type()
        self.check_absent_headers_of_content_type_status_code(500)
        self.check_absent_headers_of_content_type_code(500)

    def absent_headers_of_content_type(self):
        self.res_absent_headers_of_content_type = requests.session().post(url=url_get_allocated_stock_by_recommending,
                                                                          data=json.dumps(obj={
                                                                              "distributionList": [
                                                                                  {
                                                                                      "orderNo": "PD2021001129",
                                                                                      "orderSerialNo": "1",
                                                                                      "commodityCode": "02030000123",
                                                                                      "demandQuantity": 11,
                                                                                      "stockStatus": "",
                                                                                      "requestType": "2",
                                                                                      "hwh": "",
                                                                                      "productBatchNo": ""
                                                                                  }
                                                                              ]
                                                                          }), headers={'X-Token': get_token(),
                                                                                       'X-Site': get_x_site()})

    def check_absent_headers_of_content_type_status_code(self, status_code):
        self.assertEqual(self.res_absent_headers_of_content_type.status_code, status_code)

    def check_absent_headers_of_content_type_code(self, code):
        self.assertEqual(self.res_absent_headers_of_content_type.json().get('code'), code)

    def mock_like_a_person(self):
        """for x_site is mocked as an exists person"""
        self.mock = requests.session().post(url=url_get_allocated_stock_by_recommending, data={
            "distributionList": [
                {
                    "orderNo": "PD2021001129",
                    "orderSerialNo": "1",
                    "commodityCode": "02030000123",
                    "demandQuantity": 11,
                    "stockStatus": "",
                    "requestType": "2",
                    "hwh": "",
                    "productBatchNo": ""
                }
            ]
        },
                                            headers={'content-type': 'application/json', 'X-Token': get_token(),
                                                     'X-Site': '111111119999999'})

    def check_mock_status_code(self, status_code):
        self.assertEqual(self.mock.status_code, status_code)

    def check_mock_code(self, code):
        self.assertEqual(self.mock.json().get('code'), code)

    def absent_x_token(self):
        self.res_absent_x_token = requests.session().post(url=url_get_allocated_stock_by_recommending,
                                                          data=json.dumps(obj={
                                                              "distributionList": [
                                                                  {
                                                                      "orderNo": "PD2021001129",
                                                                      "orderSerialNo": "1",
                                                                      "commodityCode": "02030000123",
                                                                      "demandQuantity": 11,
                                                                      "stockStatus": "",
                                                                      "requestType": "2",
                                                                      "hwh": "",
                                                                      "productBatchNo": ""
                                                                  }
                                                              ]
                                                          }),
                                                          headers={'content-type': 'application/json',
                                                                   'X-Site': get_x_site()})

    def check_absent_x_token_status_code(self, status_code):
        self.assertEqual(self.res_absent_x_token.status_code, status_code)

    def check_absent_x_token_code(self, code):
        self.assertEqual(self.res_absent_x_token.json().get('code'), code)

    def absent_x_stie(self):
        """x_site == none"""
        self.res_absent_x_site = requests.session().post(url=url_get_allocated_stock_by_recommending,
                                                         data=json.dumps({
                                                             "distributionList": [
                                                                 {
                                                                     "orderNo": "PD2021001129",
                                                                     "orderSerialNo": "1",
                                                                     "commodityCode": "02030000123",
                                                                     "demandQuantity": 11,
                                                                     "stockStatus": "",
                                                                     "requestType": "2",
                                                                     "hwh": "",
                                                                     "productBatchNo": ""
                                                                 }
                                                             ]
                                                         }),
                                                         headers={'content-type': 'application/json',
                                                                  'X-Token': get_token()})

    def check_absent_x_site_status_code(self, status_code):
        """for check absent x_site status code"""
        self.assertEqual(first=self.res_absent_x_site.status_code, second=status_code)

    def check_absent_x_site_code(self, code):
        """for check absent x_site code"""
        self.assertEqual(first=self.res_absent_x_site.json().get('code'), second=code)

    def get_allocated_stock_by_recommending(self):
        """for get allocated stock by recommending"""
        self.res_get_allocated_stock_by_recommending = http.post_for_wms(url=url_get_allocated_stock_by_recommending,
                                                                         data={
                                                                             "distributionList": [
                                                                                 {
                                                                                     "orderNo": "PD2021001129",
                                                                                     "orderSerialNo": "1",
                                                                                     "commodityCode": "02030000123",
                                                                                     "demandQuantity": 11,
                                                                                     "stockStatus": "",
                                                                                     "requestType": "2",
                                                                                     "hwh": "",
                                                                                     "productBatchNo": ""
                                                                                 }
                                                                             ]
                                                                         }, session=self.session)

    def check_get_allocated_stock_by_recommending_status_code(self, status_code):
        """for check get allocated stock by recommending status code"""
        self.assertEqual(first=self.res_get_allocated_stock_by_recommending.status_code, second=status_code)

    def check_get_allocated_stock_by_recommending_code(self, code):
        self.assertEqual(first=self.res_get_allocated_stock_by_recommending.json().get('code'), second=code)

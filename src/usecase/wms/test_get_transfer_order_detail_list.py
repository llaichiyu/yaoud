#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.wms import url_get_transfer_order_detail_list

http = Http()
data = get_data('src/data/csv/wms/get_transfer_order_detail_list.csv')


@test
@injectable(title=[], session=True)
class TestGetTransferOrderDetailList(YaoudTestCase):
    @inject(data)
    def test_get_transfer_order_detail_list(self):
        """commodityName == None"""
        self.get_transfer_order_detail_list()
        self.check_get_transfer_order_detail_list_status_code(200)
        self.check_get_transfer_order_detail_list_code(200)

    @inject(data)
    def test_get_transfer_order_detail_list_by_commodityName_and_other(self):
        """commodityName is exists in body"""
        self.get_transfer_order_detail_list_by_commodityName()
        self.check_get_transfer_order_detail_list_by_commodityName_status_code(200)
        self.check_get_transfer_order_detail_list_by_commodityName_code(200)

    @inject(data)
    def test_expire_token_for_get_transfer_order_detail_list(self):
        """token is out of date"""
        self.expire_token_for_get_transfer_order_detail_list()
        self.check_expire_token_for_get_transfer_order_detail_list_status_code(200)
        self.check_expire_token_for_get_transfer_order_detail_list_code(401)

    @inject(data)
    def test_get_all_transfer_order_detail_list(self):
        self.get_all_transfer_order_detail_list()
        self.check_get_all_transfer_order_detail_list_status_code(200)
        self.check_get_all_transfer_order_detail_list_code(200)

    @inject(data)
    def test_blank_raw_body(self):
        self.blank_raw_body()
        self.check_blank_raw_body_status_code(500)
        self.check_blank_raw_body_code(500)

    def blank_raw_body(self):
        self.res_blank_raw_body = http.post_for_wms(url=url_get_transfer_order_detail_list, data={},
                                                    session=self.session)

    def check_blank_raw_body_status_code(self, status_code):
        self.assertEqual(self.res_blank_raw_body.status_code, status_code)

    def check_blank_raw_body_code(self, code):
        self.assertEqual(self.res_blank_raw_body.json().get('code'), code)

    def get_all_transfer_order_detail_list(self):
        self.res_all_transfer_order_detail_list = http.post_for_wms(url=url_get_transfer_order_detail_list,
                                                                    data={"conditions": [{}], "pageIndex": 1,
                                                                          "pageSize": 100, "total": 32, "sortBy": None,
                                                                          "orderBy": None},
                                                                    session=self.session)

    def check_get_all_transfer_order_detail_list_status_code(self, status_code):
        self.assertEqual(self.res_all_transfer_order_detail_list.status_code, status_code)

    def check_get_all_transfer_order_detail_list_code(self, code):
        self.assertEqual(self.res_all_transfer_order_detail_list.json().get('code'), code)

    def expire_token_for_get_transfer_order_detail_list(self):
        self.res_expire_token = http.expire_token_for_post_wms(url=url_get_transfer_order_detail_list, data={
            "conditions": [{"isReleased": [{"startRange": "0", "endRange": ""}],
                            "openOrderTime": [{"startRange": "20210712", "endRange": "20210712"}],
                            "customerName2": [{"startRange": "10000", "endRange": ""}],
                            "customerName": [{"startRange": "10000", "endRange": ""}],
                            "transportationRoute": [{"startRange": "X800", "endRange": ""}],
                            "demandType": [{"startRange": "1", "endRange": ""}],
                            "deliverySubNo": [{"startRange": "1常规", "endRange": ""}],
                            "commodityName": [{"startRange": "02030000123", "endRange": ""}]}], "pageIndex": 1,
            "pageSize": 100, "total": 1, "sortBy": None, "orderBy": None},
                                                               session=self.session)

    def check_expire_token_for_get_transfer_order_detail_list_status_code(self, status_code):
        self.assertEqual(self.res_expire_token.status_code, status_code)

    def check_expire_token_for_get_transfer_order_detail_list_code(self, code):
        self.assertEqual(self.res_expire_token.json().get('code'), code)

    def get_transfer_order_detail_list_by_commodityName(self):
        self.res_by_commodity = http.post_for_wms(url=url_get_transfer_order_detail_list, data={"conditions": [
            {"isReleased": [{"startRange": "0", "endRange": ""}],
             "openOrderTime": [{"startRange": "20210712", "endRange": "20210712"}],
             "customerName2": [{"startRange": "10000", "endRange": ""}],
             "customerName": [{"startRange": "10000", "endRange": ""}],
             "transportationRoute": [{"startRange": "X800", "endRange": ""}],
             "demandType": [{"startRange": "1", "endRange": ""}],
             "deliverySubNo": [{"startRange": "1常规", "endRange": ""}],
             "commodityName": [{"startRange": "02030000123", "endRange": ""}]}], "pageIndex": 1, "pageSize": 100,
            "total": 1,
            "sortBy": None,
            "orderBy": None},
                                                  session=self.session)

    def check_get_transfer_order_detail_list_by_commodityName_status_code(self, status_code):
        self.assertEqual(self.res_by_commodity.status_code, status_code)

    def check_get_transfer_order_detail_list_by_commodityName_code(self, code):
        self.assertEqual(self.res_by_commodity.json().get('code'), code)

    def get_transfer_order_detail_list(self):
        self.res_get_transfer_detail_list = http.post_for_wms(url=url_get_transfer_order_detail_list, data={
            "conditions": [
                {
                    "isReleased": [
                        {
                            "startRange": "0",
                            "endRange": ""
                        }
                    ],
                    "openOrderTime": [
                        {
                            "startRange": "20210712",
                            "endRange": "20210712"
                        }
                    ],
                    "customerName2": [
                        {
                            "startRange": "10000",
                            "endRange": ""
                        }
                    ],
                    "customerName": [
                        {
                            "startRange": "10000",
                            "endRange": ""
                        }
                    ],
                    "transportationRoute": [
                        {
                            "startRange": "X800",
                            "endRange": ""
                        }
                    ],
                    "demandType": [
                        {
                            "startRange": "1",
                            "endRange": ""
                        }
                    ],
                    "deliverySubNo": [
                        {
                            "startRange": "1常规",
                            "endRange": ""
                        }
                    ]
                }
            ],
            "pageIndex": 1,
            "pageSize": 100,
            "total": 1,
            "sortBy": None,
            "orderBy": None
        }, session=self.session)

    def check_get_transfer_order_detail_list_status_code(self, status_code):
        self.assertEqual(self.res_get_transfer_detail_list.status_code, status_code)

    def check_get_transfer_order_detail_list_code(self, code):
        self.assertEqual(self.res_get_transfer_detail_list.json().get('code'), code)

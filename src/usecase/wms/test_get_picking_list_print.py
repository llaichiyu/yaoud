#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
@date 2021年07月20日10:04 上午
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.wms import url_get_picking_list

http = Http()
data = get_data('src/data/csv/wms/get_picking_list.csv')


@test
@injectable(
    title=['isPrint_startRange', 'isPrint_endRange', 'transportationRoute_startRange', 'transportationRoute_endRange',
           'waveNo_startRange', 'waveNo_endRange', 'pickNo_startRange', 'pickNo_endRange', 'customerName_startRange',
           'customerName_endRange', 'deliverNo_startRange', 'deliverNo_endRange', 'pickArea_startRange',
           'pickArea_endRange', 'pageIndex', 'pageSize', 'total'],
    session=True)
class TestGetPickingListPrint(YaoudTestCase):
    def test_get_pick_list_normally(self):
        self.get_picking_list()
        self.check_get_picking_list_status_code(200)
        self.check_get_picking_list_code(200)
        self.check_ret_true()

    def check_ret_true(self):
        self.assertTrue(expr=True, msg=self.res_get_picking_list.json().get('success'))

    def test_None_start_range(self):
        self.None_start_range()
        self.check_None_start_range_status_code(200)
        self.check_None_start_range_code(200)

    def test_None_end_range(self):
        self.None_end_range()
        self.check_None_end_range_status_code(200)
        self.check_None_end_range_code(200)

    def test_absent_condition(self):
        self.absent_conditions()
        self.check_absent_conditions_status_code(500)
        self.check_absent_conditions_code(500)

    def test_page_absent_page_index(self):
        self.absent_page_index()
        self.check_absent_page_index_status_code(200)
        self.check_absent_page_index_code(200)

    def test_expire_token_for_get_picking_list(self):
        self.expire_token_for_get_picking_list()
        self.check_expire_token_status_code(200)
        self.check_expire_token_code(401)

    def test_unauthorized_get_picking_list(self):
        self.unauthorized_get_picking_list()
        self.check_unauthoritized_status_code(401)
        self.check_unauthoritized_code(403)

    def unauthorized_get_picking_list(self):
        self.res_unauthoritized = http.post_for_unauthorized_wms(url=url_get_picking_list, data={
            "conditions": [
                {
                    "isPrint": [
                        {
                            "startRange": "0"
                        }
                    ],
                    "transportationRoute": [
                        {
                            "startRange": "X800",
                            "endRange": ""
                        }
                    ],
                    "waveNo": [
                        {
                            "startRange": "20201231-002",
                            "endRange": ""
                        }
                    ],
                    "pickNo": [
                        {
                            "startRange": "20201231-002-02",
                            "endRange": ""
                        }
                    ],
                    "customerName": [
                        {
                            "startRange": "10001",
                            "endRange": ""
                        }
                    ],
                    "deliverNo": [
                        {
                            "startRange": "1常规",
                            "endRange": ""
                        }
                    ],
                    "pickArea": [
                        {
                            "startRange": "TH",
                            "endRange": ""
                        }
                    ]
                }
            ],
            "pageIndex": 1,
            "pageSize": 100,
            "total": 1
        }, session=self.session)

    def check_unauthoritized_status_code(self, status_code):
        self.assertEqual(self.res_unauthoritized.status_code, status_code)

    def check_unauthoritized_code(self, code):
        self.assertEqual(self.res_unauthoritized.json().get('code'), code)

    def expire_token_for_get_picking_list(self):
        self.res_expire_token = http.expire_token_for_post_wms(url=url_get_picking_list, data={
            "conditions": [
                {
                    "isPrint": [
                        {
                            "startRange": "0"
                        }
                    ],
                    "transportationRoute": [
                        {
                            "startRange": "X800",
                            "endRange": ""
                        }
                    ],
                    "waveNo": [
                        {
                            "startRange": "20201231-002",
                            "endRange": ""
                        }
                    ],
                    "pickNo": [
                        {
                            "startRange": "20201231-002-02",
                            "endRange": ""
                        }
                    ],
                    "customerName": [
                        {
                            "startRange": "10001",
                            "endRange": ""
                        }
                    ],
                    "deliverNo": [
                        {
                            "startRange": "1常规",
                            "endRange": ""
                        }
                    ],
                    "pickArea": [
                        {
                            "startRange": "TH",
                            "endRange": ""
                        }
                    ]
                }
            ],
            "pageIndex": 1,
            "pageSize": 100,
            "total": 1
        }, session=self.session)

    def check_expire_token_status_code(self, status_code):
        self.assertEqual(self.res_expire_token.status_code, status_code)

    def check_expire_token_code(self, code):
        self.assertEqual(self.res_expire_token.json().get('code'), code)

    def absent_page_index(self):
        self.res_absent_page_index = http.post_for_wms(url=url_get_picking_list, data={
            "conditions": [
                {
                    "isPrint": [
                        {
                            "startRange": "0"
                        }
                    ],
                    "transportationRoute": [
                        {
                            "startRange": "X800",
                            "endRange": ""
                        }
                    ],
                    "waveNo": [
                        {
                            "startRange": "20201231-002",
                            "endRange": ""
                        }
                    ],
                    "pickNo": [
                        {
                            "startRange": "20201231-002-02",
                            "endRange": ""
                        }
                    ],
                    "customerName": [
                        {
                            "startRange": "10001",
                            "endRange": ""
                        }
                    ],
                    "deliverNo": [
                        {
                            "startRange": "1常规",
                            "endRange": ""
                        }
                    ],
                    "pickArea": [
                        {
                            "startRange": "TH",
                            "endRange": ""
                        }
                    ]
                }
            ],
            "pageSize": 100,
            "total": 1
        }, session=self.session)

    def check_absent_page_index_status_code(self, status_code):
        self.assertEqual(self.res_absent_page_index.status_code, status_code)

    def check_absent_page_index_code(self, code):
        self.assertEqual(self.res_absent_page_index.json().get('code'), code)

    def absent_conditions(self):
        self.res_None_absent_conditions = http.post_for_wms(url=url_get_picking_list, data={
            "conditions": [],
            "pageIndex": 1,
            "pageSize": 100,
            "total": 1
        }, session=self.session)

    def check_absent_conditions_status_code(self, status_code):
        self.assertEqual(self.res_None_absent_conditions.status_code, status_code)

    def check_absent_conditions_code(self, code):
        self.assertEqual(self.res_None_absent_conditions.json().get('code'), code)

    def None_start_range(self):
        """absent startrange"""
        self.res_None_start_range = http.post_for_wms(url=url_get_picking_list, data={
            "conditions": [
                {
                    "isPrint": [
                        {
                            "endRange": ""
                        }
                    ],
                    "transportationRoute": [
                        {
                            "startRange": "X800",
                            "endRange": ""
                        }
                    ],
                    "waveNo": [
                        {
                            "startRange": "20201231-002",
                            "endRange": ""
                        }
                    ],
                    "pickNo": [
                        {
                            "startRange": "20201231-002-02",
                            "endRange": ""
                        }
                    ],
                    "customerName": [
                        {
                            "startRange": "10001",
                            "endRange": ""
                        }
                    ],
                    "deliverNo": [
                        {
                            "startRange": "1常规",
                            "endRange": ""
                        }
                    ],
                    "pickArea": [
                        {
                            "startRange": "TH",
                            "endRange": ""
                        }
                    ]
                }
            ],
            "pageIndex": 1,
            "pageSize": 100,
            "total": 1
        }, session=self.session)

    def check_None_start_range_status_code(self, status_code):
        self.assertEqual(self.res_None_start_range.status_code, status_code)

    def check_None_start_range_code(self, code):
        self.assertEqual(self.res_None_start_range.json().get('code'), code)

    def get_picking_list(self):
        """accroading to transportationRoute , waveNo,pickNo,customerName,deliverNo,pickArea and isprint"""
        self.res_get_picking_list = http.post_for_wms(url=url_get_picking_list, data={
            "conditions": [
                {
                    "isPrint": [
                        {
                            "startRange": "0",
                            "endRange": ""
                        }
                    ],
                    "transportationRoute": [
                        {
                            "startRange": "X800",
                            "endRange": ""
                        }
                    ],
                    "waveNo": [
                        {
                            "startRange": "20201231-002",
                            "endRange": ""
                        }
                    ],
                    "pickNo": [
                        {
                            "startRange": "20201231-002-02",
                            "endRange": ""
                        }
                    ],
                    "customerName": [
                        {
                            "startRange": "10001",
                            "endRange": ""
                        }
                    ],
                    "deliverNo": [
                        {
                            "startRange": "1常规",
                            "endRange": ""
                        }
                    ],
                    "pickArea": [
                        {
                            "startRange": "TH",
                            "endRange": ""
                        }
                    ]
                }
            ],
            "pageIndex": 1,
            "pageSize": 100,
            "total": 1
        }, session=self.session)

    def check_get_picking_list_status_code(self, status_code):
        """for check get picking list status code"""
        self.assertEqual(self.res_get_picking_list.status_code, status_code)

    def check_get_picking_list_code(self, code):
        """for check get picking list code"""
        self.assertEqual(self.res_get_picking_list.json().get('code'), code)

    def None_end_range(self):
        """end range == None"""
        self.res_None_end_range = http.post_for_wms(url=url_get_picking_list, data={
            "conditions": [
                {
                    "isPrint": [
                        {
                            "startRange": "0"
                        }
                    ],
                    "transportationRoute": [
                        {
                            "startRange": "X800",
                            "endRange": ""
                        }
                    ],
                    "waveNo": [
                        {
                            "startRange": "20201231-002",
                            "endRange": ""
                        }
                    ],
                    "pickNo": [
                        {
                            "startRange": "20201231-002-02",
                            "endRange": ""
                        }
                    ],
                    "customerName": [
                        {
                            "startRange": "10001",
                            "endRange": ""
                        }
                    ],
                    "deliverNo": [
                        {
                            "startRange": "1常规",
                            "endRange": ""
                        }
                    ],
                    "pickArea": [
                        {
                            "startRange": "TH",
                            "endRange": ""
                        }
                    ]
                }
            ],
            "pageIndex": 1,
            "pageSize": 100,
            "total": 1
        }, session=self.session)

    def check_None_end_range_status_code(self, status_code):
        """for check get picking list status code"""
        self.assertEqual(self.res_None_end_range.status_code, status_code)

    def check_None_end_range_code(self, code):
        """for check get picking list code"""
        self.assertEqual(self.res_None_end_range.json().get('code'), code)

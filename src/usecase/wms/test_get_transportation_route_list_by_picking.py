#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
@date 2021年07月20日2:46 下午
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.wms import url_get_transportation_route_list_by_picking

http = Http()
data = get_data('src/data/csv/wms/get_transportation_route_list_by_picking.csv')


@test
@injectable(title=[], session=True)
class TestGetTransportationRouteListByPickingList(YaoudTestCase):
    def test_get_transportation_route_list_by_picking_list(self):
        self.get_transportation_route_list()
        self.check_get_transportation_route_list_status_code(200)
        self.check_get_transportation_route_list_code(200)

    def test_expire_token_for_get_transportation_route_list(self):
        self.expire_token_for_get_transportation_route_list()
        self.check_expire_token_status_code(200)
        self.check_expire_token_code(401)

    def expire_token_for_get_transportation_route_list(self):
        self.res_token = http.expire_token_for_post_wms(url=url_get_transportation_route_list_by_picking, data={},
                                                        session=self.session)

    def check_expire_token_status_code(self, status_code):
        self.assertEqual(self.res_token.status_code, status_code)

    def check_expire_token_code(self, code):
        self.assertEqual(self.res_token.json().get('code'), code)

    def get_transportation_route_list(self):
        self.res_get_transportation_route_list = http.post_for_wms(url=url_get_transportation_route_list_by_picking,
                                                                   data={}, session=self.session)

    def check_get_transportation_route_list_status_code(self, status_code):
        self.assertEqual(self.res_get_transportation_route_list.status_code, status_code)

    def check_get_transportation_route_list_code(self, code):
        self.assertEqual(self.res_get_transportation_route_list.json().get('code'), code)

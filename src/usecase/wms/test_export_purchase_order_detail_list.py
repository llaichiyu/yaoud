#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.wms import url_export_purchase_order_detail_list

http = Http()
data = get_data('src/data/csv/wms/export_purchase_order_detail_list.csv')


@test
@injectable(title=['conditions', 'pageIndex', 'pageSize', 'total', 'sortBy', 'orderBy'], session=True)
class TestExportPurchaseOrderDetailList(YaoudTestCase):
    # @inject(data)
    def test_export_all_data(self):
        """for test all data
        导出的excel 不需要校验retcode
        但需要盯对表格中的每一列是否少了字段及对应数据"""
        self.export_purchase_order_detail_list()
        self.check_export_purchase_order_detail_list_status_code(200)
        # self.check_export_purchase_order_detail_list_code(0)

    def export_purchase_order_detail_list(self):
        """for export purchase order detail list"""
        self.res_export_purchase_order_detail_list = http.post_for_wms(url=url_export_purchase_order_detail_list,
                                                                       data={
                                                                           "conditions": [
                                                                               {}
                                                                           ],
                                                                           "pageIndex": 1,
                                                                           "pageSize": -1,
                                                                           "total": 245,
                                                                           "sortBy": None,
                                                                           "orderBy": None}, session=self.session)

    def check_export_purchase_order_detail_list_status_code(self, status_code):
        self.assertEqual(self.res_export_purchase_order_detail_list.status_code, status_code)
    #
    # def check_export_purchase_order_detail_list_code(self, code):
    #     self.assertEqual(self.res_export_purchase_order_detail_list.json().get('code'), code)

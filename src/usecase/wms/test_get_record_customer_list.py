#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.wms import url_get_record_customer_list

data = get_data('src/data/csv/wms/get_record_customer_list.csv')
http = Http()


@test
@injectable(title=[], session=True)
class TestGetRecordCustomerList(YaoudTestCase):
    @inject(data)
    def test_get_all_record_list(self):
        self.get_record_customer_list()
        self.check_get_record_customer_status_code(200)
        self.check_get_record_code(200)

    def get_record_customer_list(self):
        """for get record main method"""
        self.res_get_record_main = http.post_for_wms(url=url_get_record_customer_list, data={}, session=self.session)

    def check_get_record_customer_status_code(self, status_code):
        """for get status code"""
        self.assertEqual(self.res_get_record_main.status_code, status_code)

    def check_get_record_code(self, code):
        self.assertEqual(self.res_get_record_main.json().get('code'), code)

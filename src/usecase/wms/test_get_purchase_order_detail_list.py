#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.wms import url_get_record_customer_list

data = get_data('src/data/csv/wms/get_purchase_order_detail_list.csv')
http = Http()


@test
@injectable(title=['conditions', 'pageIndex', 'pageSize', 'total', 'sortBy', 'orderBy'], session=True)
class TestGetPurchaseOrderDetailList(YaoudTestCase):

    @inject(data[0], multi=False)
    def test_get_purchase_order_detail_list(self):
        self.get_purchase_order_detail_list()
        self.check_get_purchase_order_detail_list_status_code(200)
        self.check_get_purchase_order_detail_list_code(200)

    def get_purchase_order_detail_list(self):
        self.res_get_purchase_detail = http.post_for_wms(url=url_get_record_customer_list,
                                                         data={
                                                             "conditions": [
                                                                 {
                                                                     "customerId": [
                                                                         {
                                                                             "startRange": "10000",
                                                                             "endRange": ""
                                                                         }
                                                                     ],
                                                                     "orderNo": [
                                                                         {
                                                                             "startRange": "PD2021001132",
                                                                             "endRange": ""
                                                                         }
                                                                     ],
                                                                     "commodityName": [
                                                                         {
                                                                             "startRange": "02030000207",
                                                                             "endRange": ""
                                                                         }
                                                                     ],
                                                                     "customerNo": [
                                                                         {
                                                                             "startRange": "10000",
                                                                             "endRange": ""
                                                                         }
                                                                     ],
                                                                     "demandType": [
                                                                         {
                                                                             "startRange": "0",
                                                                             "endRange": ""
                                                                         }
                                                                     ]
                                                                 }
                                                             ],
                                                             "pageIndex": 1,
                                                             "pageSize": -1,
                                                             "total": 1,
                                                             "sortBy": None,
                                                             "orderBy": None
                                                         },
                                                         session=self.session)

    def check_get_purchase_order_detail_list_status_code(self, status_code):
        self.assertEqual(self.res_get_purchase_detail.status_code, status_code)

    def check_get_purchase_order_detail_list_code(self, code):
        self.assertEqual(self.res_get_purchase_detail.json().get('code'), code)

#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.wms import url_get_product_position_list

http = Http()
data = get_data('src/data/csv/wms/get_product_position_list.csv')


@test
@injectable(title=['productInfo'], session=True)
class TestGetProductionPositionList(YaoudTestCase):
    @inject(data[0])
    def test_get_productInfo_exists(self):
        """productInfo exists in Fuzzy"""
        self.get_product_position_list()
        self.check_get_product_position_list_status_code(200)
        self.check_get_product_position_list_code(200)

    @inject(data[1])
    def test_productInfo_not_exists(self):
        """produtInfo == 9999999999999999999999"""
        self.get_product_position_list()
        self.check_get_product_position_list_status_code(200)
        self.check_get_product_position_list_code(200)

    @inject(data)
    def test_blank_productInfo(self):
        """productInfo == blank str"""
        self.blank_productInfo()
        self.check_blank_productInfo_status_code(200)
        self.check_blank_productInfo_code(200)
        self.check_msg('暂无承载数据')

    @inject(data)
    def test_None_body(self):
        """productInfo == None"""
        self.None_body()
        self.check_None_body_status_code(200)
        self.check_None_body_code(200)
        self.check_None_msg('暂无承载数据')

    @inject(data)
    def test_expire_token(self):
        """token is out of date"""
        self.expire_token_for_get_product_list()
        self.check_expire_token_for_get_product_list_status_code(200)
        self.check_expire_token_for_get_product_list_code(401)

    def check_None_msg(self, msg):
        self.assertMultiLineEqual(first=self.res_None_body.json().get('msg'), second=msg)

    def None_body(self):
        self.res_None_body = http.post_for_wms(url=url_get_product_position_list, data={}, session=self.session)

    def check_None_body_status_code(self, status_code):
        self.assertEqual(self.res_None_body.status_code, status_code)

    def check_None_body_code(self, code):
        self.assertEqual(self.res_None_body.json().get('code'), code)

    def blank_productInfo(self):
        self.res_blank_productInfo = http.post_for_wms(url=url_get_product_position_list,
                                                       data={'productInfo': ''},
                                                       session=self.session)

    def expire_token_for_get_product_list(self):
        self.res_expire_token = http.expire_token_for_post_wms(url=url_get_product_position_list,
                                                               data={'productInfo': self.productInfo},
                                                               session=self.session)

    def check_expire_token_for_get_product_list_status_code(self, status_code):
        self.assertEqual(self.res_expire_token.status_code, status_code)

    def check_expire_token_for_get_product_list_code(self, code):
        self.assertEqual(self.res_expire_token.json().get('code'), code)

    def check_blank_productInfo_status_code(self, status_code):
        self.assertEqual(self.res_blank_productInfo.status_code, status_code)

    def check_blank_productInfo_code(self, code):
        self.assertEqual(self.res_blank_productInfo.json().get('code'), code)

    def check_msg(self, msg):
        self.assertMultiLineEqual(first=self.res_blank_productInfo.json().get('msg'), second=msg)

    def get_product_position_list(self):
        self.res_get_product_position_list = http.post_for_wms(url=url_get_product_position_list,
                                                               data={'productInfo': self.productInfo},
                                                               session=self.session)

    def check_get_product_position_list_status_code(self, status_code):
        self.assertEqual(self.res_get_product_position_list.status_code, status_code)

    def check_get_product_position_list_code(self, code):
        self.assertEqual(self.res_get_product_position_list.json().get('code'), code)

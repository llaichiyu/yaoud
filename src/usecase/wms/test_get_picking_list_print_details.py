#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
@date 2021年07月20日2:11 下午
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.wms import url_get_picking_list_print_details
from src.base.run_test import get_x_site

http = Http()
data = get_data('src/data/csv/wms/get_picking_list_print_details.csv')


@test
@injectable(title=['deliveryNo'], session=True)
class TestGetPickingListPrintDetails(YaoudTestCase):
    @inject(data[0])
    def test_get_picking_list_print_details(self):
        """deliveryNo is exists in db"""
        self.get_picking_list_print_details()
        self.check_get_picking_list_print_details_status_code(200)
        self.check_get_picking_list_print_details_code(200)

    @inject(data[0])
    def test_None_deliveryNo(self):
        """deliveryNo == None"""
        self.None_deliveryNo()
        self.check_None_deliveryNo_status_code(500)
        self.check_None_deliveryNo_code(500)

    @inject(data[0])
    def test_expire_token_for_get_details(self):
        """token is out of expire"""
        self.expire_token_for_get_details()
        self.check_expire_token_status_code(200)
        self.check_expire_token_code(401)

    def expire_token_for_get_details(self):
        self.res_expire = requests.session().post(url=url_get_picking_list_print_details,
                                                  data=json.dumps(obj={'deliveryNo': self.deliveryNo}),
                                                  headers={'content-type': 'application/json',
                                                           'X-Token': '00909090d9s09d0s9d0s9d0s9d',
                                                           'X-Site': get_x_site()})
        return self.res_expire

    def check_expire_token_status_code(self, status_code):
        self.assertEqual(self.res_expire.status_code, status_code)

    def check_expire_token_code(self, code):
        self.assertEqual(self.res_expire.json().get('code'), code)

    def None_deliveryNo(self):
        self.res_None_deliveryNo = http.post_for_wms(url=url_get_picking_list_print_details, data={},
                                                     session=self.session)

    def check_None_deliveryNo_status_code(self, status_code):
        self.assertEqual(self.res_None_deliveryNo.status_code, status_code)

    def check_None_deliveryNo_code(self, code):
        self.assertEqual(self.res_None_deliveryNo.json().get('code'), code)

    def get_picking_list_print_details(self):
        self.res_get_picking_list_print_details = http.post_for_wms(url=url_get_picking_list_print_details,
                                                                    data={'deliveryNo': self.deliveryNo},
                                                                    session=self.session)

    def check_get_picking_list_print_details_status_code(self, status_code):
        self.assertEqual(self.res_get_picking_list_print_details.status_code, status_code)

    def check_get_picking_list_print_details_code(self, code):
        self.assertEqual(self.res_get_picking_list_print_details.json().get('code'), code)

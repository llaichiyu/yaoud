#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.wms import url_get_auth_data
from src.base.run_test import get_x_site

data = get_data('src/data/csv/wms/get_auth_data.csv')
http = Http


@test
@injectable(title=[], session=True)
class TestGetAuthData(YaoudTestCase):
    @inject(data)
    def test_get_auth_data(self):
        """for get all auth_hub data"""
        self.get_auth_data()
        self.check_get_auth_data_status_code(200)
        self.check_get_auth_data_code(200)

    def get_auth_data(self):
        """for get auth_hub data"""
        self.res_get_auth_data = requests.session().post(url=url_get_auth_data, data=json.dumps(obj={}),
                                                         headers={'content-type': 'application/json',
                                                                  'X-Token': get_token(), 'X-Site': get_x_site()})
        return self.res_get_auth_data

    def check_get_auth_data_status_code(self, status_code):
        """for check get auth_hub status code"""
        self.assertEqual(self.res_get_auth_data.status_code, status_code)

    def check_get_auth_data_code(self, code):
        self.assertEqual(self.res_get_auth_data.json().get('code'), code)

#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
@date 2021年07月21日4:10 下午
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.wms import url_get_product_positioning_list

http = Http()
data = get_data('src/data/csv/wms/get_product_positioning_list.csv')


@test
@injectable(title=['productInfo'], session=True)
class TestGetProductioningList(YaoudTestCase):
    @inject(data[0])
    def test_get_productioning_list_normally(self):
        self.get_productioning_list()
        self.check_get_productioning_list_status_code(200)
        self.check_get_productioning_list_code(200)

    def test_None_productInfo(self):
        self.None_productInfo()
        self.check_None_productInfo_status_code(200)
        self.check_None_productInfo_code(200)

    @inject(data[1])
    def test_productInfo_not_exists_in_db(self):
        self.get_productioning_list()
        self.check_get_productioning_list_status_code(200)
        self.check_get_productioning_list_code(200)

    def get_productioning_list(self):
        self.res_get_productioning_list = http.post_for_wms(url=url_get_product_positioning_list,
                                                            data={'productInfo': self.productInfo},
                                                            session=self.session)

    def check_get_productioning_list_status_code(self, status_code):
        self.assertEqual(self.res_get_productioning_list.status_code, status_code)

    def check_get_productioning_list_code(self, code):
        self.assertEqual(self.res_get_productioning_list.json().get('code'), code)

    def None_productInfo(self):
        self.res_None_productInfo = http.post_for_wms(url=url_get_product_positioning_list,
                                                      data={},
                                                      session=self.session)

    def check_None_productInfo_status_code(self, status_code):
        self.assertEqual(self.res_None_productInfo.status_code, status_code)

    def check_None_productInfo_code(self, code):
        self.assertEqual(self.res_None_productInfo.json().get('code'), code)

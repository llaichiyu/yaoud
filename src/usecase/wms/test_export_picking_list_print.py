#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
@date 2021年07月20日1:29 下午
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.wms import url_export_picking_list_print

http = Http()
data = get_data('src/data/csv/wms/export_picking_list_print.csv')


@test
@injectable(
    title=['isPrint_startRange', 'isPrint_endRange', 'transportationRoute_startRange', 'transportationRoute_endRange',
           'waveNo_startRange', 'waveNo_endRange', 'pickNo_startRange', 'pickNo_endRange', 'customerName_startRange',
           'customerName_endRange', 'deliverNo_startRange', 'deliverNo_endRange', 'pickArea_startRange',
           'pickArea_endRange', 'pageIndex', 'pageSize', 'total'], session=True)
class TestExportPickingListPrint(YaoudTestCase):
    def test_export_picking_list_print(self):
        self.export_picking_list_print()
        self.check_export_normally_status_code(200)

    def test_None_start_range(self):
        self.None_start_range()
        self.check_None_start_range_status_code(200)

    def test_None_end_range(self):
        self.None_end_range()
        self.check_None_end_range_status_code(200)

    def test_expire_token(self):
        self.expire_token_for_export_picking_list()
        self.check_expire_token_status_code(200)
        self.check_expire_token_code(401)

    def expire_token_for_export_picking_list(self):
        self.res_expire_token = http.expire_token_for_post_wms(url=url_export_picking_list_print, data={
            "conditions": [
                {
                    "isPrint": [
                        {
                            "startRange": "0",
                            "endRange": ""
                        }
                    ],
                    "transportationRoute": [
                        {
                            "startRange": "X800",
                            "endRange": ""
                        }
                    ],
                    "waveNo": [
                        {
                            "startRange": "20201231-002",
                            "endRange": ""
                        }
                    ],
                    "pickNo": [
                        {
                            "startRange": "20201231-002-02",
                            "endRange": ""
                        }
                    ],
                    "customerName": [
                        {
                            "startRange": "10001",
                            "endRange": ""
                        }
                    ],
                    "deliverNo": [
                        {
                            "startRange": "1常规",
                            "endRange": ""
                        }
                    ],
                    "pickArea": [
                        {
                            "startRange": "TH",
                            "endRange": ""
                        }
                    ]
                }
            ],
            "pageIndex": 1,
            "pageSize": 100,
            "total": 1
        }, session=self.session)

    def check_expire_token_status_code(self, status_code):
        self.assertEqual(self.res_expire_token.status_code, status_code)

    def check_expire_token_code(self, code):
        self.assertEqual(self.res_expire_token.json().get('code'), code)

    def None_end_range(self):
        """end range == None"""
        self.res_None_end_range = http.post_for_wms(url=url_export_picking_list_print, data={
            "conditions": [
                {
                    "isPrint": [
                        {
                            "startRange": "0"
                        }
                    ],
                    "transportationRoute": [
                        {
                            "startRange": "X800",
                            "endRange": ""
                        }
                    ],
                    "waveNo": [
                        {
                            "startRange": "20201231-002",
                            "endRange": ""
                        }
                    ],
                    "pickNo": [
                        {
                            "startRange": "20201231-002-02",
                            "endRange": ""
                        }
                    ],
                    "customerName": [
                        {
                            "startRange": "10001",
                            "endRange": ""
                        }
                    ],
                    "deliverNo": [
                        {
                            "startRange": "1常规",
                            "endRange": ""
                        }
                    ],
                    "pickArea": [
                        {
                            "startRange": "TH",
                            "endRange": ""
                        }
                    ]
                }
            ],
            "pageIndex": 1,
            "pageSize": 100,
            "total": 1
        }, session=self.session)

    def check_None_end_range_status_code(self, status_code):
        """for check get picking list status code"""
        self.assertEqual(self.res_None_end_range.status_code, status_code)

    def check_None_end_range_code(self, code):
        """for check get picking list code"""
        self.assertEqual(self.res_None_end_range.json().get('code'), code)

    def None_start_range(self):
        """absent startrange"""
        self.res_None_start_range = http.post_for_wms(url=url_export_picking_list_print, data={
            "conditions": [
                {
                    "isPrint": [
                        {
                            "endRange": ""
                        }
                    ],
                    "transportationRoute": [
                        {
                            "startRange": "X800",
                            "endRange": ""
                        }
                    ],
                    "waveNo": [
                        {
                            "startRange": "20201231-002",
                            "endRange": ""
                        }
                    ],
                    "pickNo": [
                        {
                            "startRange": "20201231-002-02",
                            "endRange": ""
                        }
                    ],
                    "customerName": [
                        {
                            "startRange": "10001",
                            "endRange": ""
                        }
                    ],
                    "deliverNo": [
                        {
                            "startRange": "1常规",
                            "endRange": ""
                        }
                    ],
                    "pickArea": [
                        {
                            "startRange": "TH",
                            "endRange": ""
                        }
                    ]
                }
            ],
            "pageIndex": 1,
            "pageSize": 100,
            "total": 1
        }, session=self.session)

    def check_None_start_range_status_code(self, status_code):
        self.assertEqual(self.res_None_start_range.status_code, status_code)

    def check_None_start_range_code(self, code):
        self.assertEqual(self.res_None_start_range.json().get('code'), code)

    def export_picking_list_print(self):
        self.res_export_picking_list_print = http.post_for_wms(url=url_export_picking_list_print, data={
            "conditions": [
                {
                    "isPrint": [
                        {
                            "startRange": "0",
                            "endRange": ""
                        }
                    ],
                    "transportationRoute": [
                        {
                            "startRange": "X800",
                            "endRange": ""
                        }
                    ],
                    "waveNo": [
                        {
                            "startRange": "20201231-002",
                            "endRange": ""
                        }
                    ],
                    "pickNo": [
                        {
                            "startRange": "20201231-002-02",
                            "endRange": ""
                        }
                    ],
                    "customerName": [
                        {
                            "startRange": "10001",
                            "endRange": ""
                        }
                    ],
                    "deliverNo": [
                        {
                            "startRange": "1常规",
                            "endRange": ""
                        }
                    ],
                    "pickArea": [
                        {
                            "startRange": "TH",
                            "endRange": ""
                        }
                    ]
                }
            ],
            "pageIndex": 1,
            "pageSize": 100,
            "total": 1
        }, session=self.session)

    def check_export_normally_status_code(self, status_code):
        self.assertEqual(self.res_export_picking_list_print.status_code, status_code)

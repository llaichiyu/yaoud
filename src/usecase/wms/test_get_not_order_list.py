#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.wms import url_get_not_order_list

http = Http()
data = get_data('src/data/csv/wms/get_not_order_list.csv')


@test
@injectable(title=[], session=True)
class TestGetNotOrderList(YaoudTestCase):
    def test_get_not_order_list(self):
        """raw data is None"""
        self.get_not_order_list()
        self.check_get_not_order_list_status_code(200)
        self.check_get_not_order_list_code(200)

    def get_not_order_list(self):
        """for get not order list"""
        self.res_get_not_order_list = http.post_for_wms(url=url_get_not_order_list, data={}, session=self.session)

    def check_get_not_order_list_status_code(self, status_code):
        self.assertEqual(self.res_get_not_order_list.status_code, status_code)

    def check_get_not_order_list_code(self, code):
        self.assertEqual(self.res_get_not_order_list.json().get('code'), code)

#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.wms import url_get_request_typelist_of_purchase_order

data = get_data('src/data/csv/wms/get_request_typelist_of_purchase_order.csv')
http = Http()


@test
@injectable(title=['orderStatus'], session=True)
class TestGetRequestTypeListOfPurchaseOrder(YaoudTestCase):
    @inject(data[0])
    def test_get_purchase_order(self):
        self.get_request_type_list_of_purchase_order()
        self.check_get_request_type_list_of_purchase_order_status_code(200)
        self.check_get_request_type_list_of_purchase_order_code(200)

    def get_request_type_list_of_purchase_order(self):
        self.res_get_request_type_list_of_purchase_order = http.post_for_wms(
            url=url_get_request_typelist_of_purchase_order, data={'orderStatus': self.orderStatus},
            session=self.session)

    def check_get_request_type_list_of_purchase_order_status_code(self, status_code):
        self.assertIs(self.res_get_request_type_list_of_purchase_order.status_code, status_code)

    def check_get_request_type_list_of_purchase_order_code(self, code):
        self.assertIs(self.res_get_request_type_list_of_purchase_order.json().get('code'), code)

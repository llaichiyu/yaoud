#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
@date 2021年07月21日2:34 下午
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.wms import url_get_deliver_list_print

http = Http()


@test
@injectable(title=[], session=True)
class TestGetDeliverListPrint(YaoudTestCase):

    def test_get_deliver_list_print(self):
        self.get_deliver_list_print()
        self.check_get_deliver_list_print_status_code(200)
        self.check_get_deliver_list_print_code(200)

    def test_None_body(self):
        self.None_body()
        self.check_None_body_status_code(500)
        self.check_None_body_code(500)

    def test_check_expire_token(self):
        self.expire_token_for_get_deliver_list_print()
        self.check_expire_status_code(200)
        self.check_expire_code(401)

    def expire_token_for_get_deliver_list_print(self):
        self.res_expire_token = http.expire_token_for_post_wms(url=url_get_deliver_list_print, data={
            "pageIndex": 1,
            "pageSize": 100,
            "sortBy": "",
            "orderBy": "",
            "conditions": [
                {
                    "isPrint": [
                        {
                            "startRange": "0",
                            "endRange": ""
                        }
                    ],
                    "waveNo": [
                        {
                            "startRange": "20210721-001",
                            "endRange": ""
                        }
                    ],
                    "shopName": [
                        {
                            "startRange": "10000",
                            "endRange": ""
                        }
                    ],
                    "deliverNo": [
                        {
                            "startRange": "PY2021001130",
                            "endRange": ""
                        }
                    ],
                    "transportationRoute": [
                        {
                            "startRange": "X800",
                            "endRange": ""
                        }
                    ],
                    "requirementType": [
                        {
                            "startRange": "2",
                            "endRange": ""
                        }
                    ],
                    "pickNo": [
                        {
                            "startRange": "20210721-001-01",
                            "endRange": "20210721-001-02",
                            "v": None
                        }
                    ],
                    "releasedDate": [
                        {
                            "startRange": "20210721",
                            "endRange": "20210721"
                        }
                    ]
                }
            ]
        }, session=self.session)

    def check_expire_status_code(self, status_code):
        self.assertEqual(self.res_expire_token.status_code, status_code)

    def check_expire_code(self, code):
        self.assertEqual(self.res_expire_token.json().get('code'), code)

    def None_body(self):
        self.res_None = http.post_for_wms(url=url_get_deliver_list_print, data={},
                                          session=self.session)

    def check_None_body_status_code(self, status_code):
        self.assertEqual(self.res_None.status_code, status_code)

    def check_None_body_code(self, code):
        self.assertEqual(self.res_None.json().get('code'), code)

    def get_deliver_list_print(self):
        self.res_get_deliver_list_print = http.post_for_wms(url=url_get_deliver_list_print, data={
            "pageIndex": 1,
            "pageSize": 100,
            "sortBy": "",
            "orderBy": "",
            "conditions": [
                {
                    "isPrint": [
                        {
                            "startRange": "0",
                            "endRange": ""
                        }
                    ],
                    "waveNo": [
                        {
                            "startRange": "20210721-001",
                            "endRange": ""
                        }
                    ],
                    "shopName": [
                        {
                            "startRange": "10000",
                            "endRange": ""
                        }
                    ],
                    "deliverNo": [
                        {
                            "startRange": "PY2021001130",
                            "endRange": ""
                        }
                    ],
                    "transportationRoute": [
                        {
                            "startRange": "X800",
                            "endRange": ""
                        }
                    ],
                    "requirementType": [
                        {
                            "startRange": "2",
                            "endRange": ""
                        }
                    ],
                    "pickNo": [
                        {
                            "startRange": "20210721-001-01",
                            "endRange": "20210721-001-02",
                            "v": None
                        }
                    ],
                    "releasedDate": [
                        {
                            "startRange": "20210721",
                            "endRange": "20210721"
                        }
                    ]
                }
            ]
        }, session=self.session)

    def check_get_deliver_list_print_status_code(self, status_code):
        self.assertEqual(self.res_get_deliver_list_print.status_code, status_code)

    def check_get_deliver_list_print_code(self, code):
        self.assertEqual(self.res_get_deliver_list_print.json().get('code'), code)

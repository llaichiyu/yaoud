#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
@date 2021年07月21日10:18 上午
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.wms import url_finish_deliver
from src.utils.sql_class import ExecuteDb
from src.sql.wms.update_finish_deliver import sql_update_finish_deliver

sql = ExecuteDb()
http = Http()
data = get_data('src/data/csv/wms/finish_deliver.csv')


@test
@injectable(title=[], session=True)
class TestFinishDeliver(YaoudTestCase):
    def test_finish_deliver(self):
        self.finish_deliver()
        self.check_finish_deliver_status_code(400)
        self.check_finish_deliver_code(200)
        self.update_deliver_status()

    def test_has_been_delivered(self):
        self.has_been_delivered()
        self.check_has_been_delivered_status_code(400)
        self.check_has_been_delivered_code(400)

    def test_None_pickNo(self):
        """pickNo == None"""
        self.None_body()
        self.check_None_body_status_code(500)
        self.check_None_body_code(500)

    def expire_token_for_finish_deliver(self):
        self.expire_token()
        self.check_expire_token_status_code(200)
        self.check_expire_token_code(401)

    def test_has_been_delivered_by_app(self):
        self.pickNo_has_been_delivered_by_app()
        self.check_app_status_code(400)
        self.check_app_code(400)

    def pickNo_has_been_delivered_by_app(self):
        """WM_JHDH = '202012270007-01"""
        self.res_by_app = http.post_for_wms(url=url_finish_deliver, data={
            "finishPickingDTOList": [
                {
                    "pickNo": "202012270007-01"
                }

            ]
        }, session=self.session)

    def check_app_status_code(self, status_code):
        self.assertEqual(self.res_by_app.status_code, status_code)

    def check_app_code(self, code):
        self.assertEqual(self.res_by_app.json().get('code'), code)

    def has_been_delivered(self):
        self.res_has = http.post_for_wms(url=url_finish_deliver, data={
            "finishPickingDTOList": [
                {
                    "pickNo": "20210329-001-01"
                }
            ]
        }, session=self.session)

    def check_has_been_delivered_status_code(self, status_code):
        self.assertEqual(self.res_has.status_code, status_code)

    def check_has_been_delivered_code(self, code):
        self.assertEqual(self.res_has.json().get('code'), code)

    def finish_deliver(self):
        self.res_finish_deliver = http.post_for_wms(url=url_finish_deliver, data={
            "finishPickingDTOList": [
                {
                    "pickNo": "20210714-002-01"
                }
            ]
        }, session=self.session)

    def check_finish_deliver_status_code(self, status_code):
        self.assertEqual(self.res_finish_deliver.status_code, status_code)

    def check_finish_deliver_code(self, code):
        self.assertEqual(self.res_finish_deliver.json().get('code'), code)

    def update_deliver_status(self):
        update = sql.execute_sql(command='update', sql=sql_update_finish_deliver)
        return update

    def None_body(self):
        self.res_None = http.post_for_wms(url=url_finish_deliver, data={}, session=self.session)

    def check_None_body_status_code(self, status_code):
        self.assertEqual(self.res_None.status_code, status_code)

    def check_None_body_code(self, code):
        self.assertEqual(self.res_None.json().get('code'), code)

    def expire_token(self):
        self.res_expire_token = http.expire_token_for_post(url=url_finish_deliver, data={
            "finishPickingDTOList": [
                {
                    "pickNo": "20210714-002-01"
                }
            ]
        }, session=self.session)

    def check_expire_token_status_code(self, status_code):
        self.assertEqual(self.res_expire_token.status_code, status_code)

    def check_expire_token_code(self, code):
        self.assertEqual(self.res_expire_token.json().get('code'), code)

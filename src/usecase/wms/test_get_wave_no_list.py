#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
@date 2021年07月21日9:47 上午
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.wms import url_get_wave_no_list
from src.base.run_test import get_x_site

http = Http()
data = get_data('src/data/csv/wms/get_wave_no_list.csv')


@test
@injectable(title=[], session=True)
class TestGetWaveNoList(YaoudTestCase):

    def test_get_wave_no_list_normally(self):
        self.get_wave_no_list()
        self.check_get_wave_no_list_status_code(200)
        self.check_get_wave_no_list_code(200)

    def test_expire_token(self):
        self.expire_token_for_get_wave_no_list()
        self.check_expire_token_for_get_wave_no_list_status_code()
        self.check_expire_token_for_get_wave_no_list_code(401)

    def get_wave_no_list(self):
        self.res_get_wave_no_list = http.post_for_wms(url=url_get_wave_no_list, data={}, session=self.session)

    def check_get_wave_no_list_status_code(self, status_code):
        self.assertEqual(self.res_get_wave_no_list.status_code, status_code)

    def check_get_wave_no_list_code(self, code):
        self.assertEqual(self.res_get_wave_no_list.json().get('code'), code)

    def expire_token_for_get_wave_no_list(self):
        self.res_expire = requests.session().post(url=url_get_wave_no_list, data=json.dumps(obj={}),
                                                  headers={'content-type': 'application/json',
                                                           'X-Token': 'djaklsjdksjlkdjlsjdlkjslkdjslkjdkl',
                                                           'X-Site': get_x_site()})

    def check_expire_token_for_get_wave_no_list_status_code(self, status_code):
        self.assertEqual(self.res_expire.status_code, status_code)

    def check_expire_token_for_get_wave_no_list_code(self, code):
        self.assertEqual(self.res_expire.json().get('code'), code)

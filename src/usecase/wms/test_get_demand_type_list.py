#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.wms import url_get_demand_type_list

http = Http()
data = get_data('src/data/csv/wms/get_demand_type_list.csv')


@test
@injectable(title=[], session=True)
class TestGetDemandTypeList(YaoudTestCase):
    @inject(data)
    def test_get_demand_type_list(self):
        """for normally get demand type list and normal token"""
        self.get_demand_type_list()
        self.check_get_demand_type_list_status_code(200)
        self.check_get_demand_type_list_code(200)

    @inject(data)
    def test_expire_token_for_get_demand_type_list(self):
        """token is out of date"""
        self.expire_token_for_get_demand_type_list()
        self.check_expire_token_for_get_demand_type_list_status_code(200)
        self.check_expire_token_for_get_demand_type_list_code(401)

    def expire_token_for_get_demand_type_list(self):
        self.res_expire_token_for_get_demand_type_list = http.expire_token_for_post(url=url_get_demand_type_list,
                                                                                    data={},
                                                                                    session=self.session)

    def check_expire_token_for_get_demand_type_list_status_code(self, status_code):
        self.assertEqual(self.res_expire_token_for_get_demand_type_list.status_code, status_code)

    def check_expire_token_for_get_demand_type_list_code(self, code):
        self.assertEqual(self.res_expire_token_for_get_demand_type_list.json().get('code'), code)

    def get_demand_type_list(self):
        self.res_get_demand_type_list = http.post_for_wms(url=url_get_demand_type_list, data={}, session=self.session)

    def check_get_demand_type_list_status_code(self, status_code):
        self.assertEqual(self.res_get_demand_type_list.status_code, status_code)

    def check_get_demand_type_list_code(self, code):
        self.assertEqual(self.res_get_demand_type_list.json().get('code'), code)

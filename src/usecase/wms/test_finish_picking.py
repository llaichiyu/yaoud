#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
@date 2021年07月20日3:47 下午
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.wms import url_finish_picking
from src.sql.wms.update_picking_list_status import sql_update_picking_list_status_or_not
from src.utils.sql_class import ExecuteDb

sql = ExecuteDb()
http = Http()
data = get_data('src/data/csv/wms/finishing_picking.csv')


@test
@injectable(title=['finishPickingDTOList_pickNo'], session=True)
class TestFinishPicking(YaoudTestCase):
    def test_diff_client(self):
        self.different_client()
        self.check_diff_status_code(400)
        self.check_diff_code(500)

    @inject(data[0])
    def test_finish_picking_list(self):
        self.finish_picking()
        self.check_finish_picking_status_code(200)
        self.check_finish_picking_code(200)
        self.update()

    @inject(data[1])
    def test_picking_list_has_been_picked(self):
        """cur pickNo has been picked"""
        self.finish_picking()
        self.check_finish_picking_status_code(400)
        self.check_finish_picking_code(400)

    def test_None_body(self):
        self.None_body()
        self.check_None_body_status_code(500)
        self.check_None_body_code(500)

    def different_client(self):
        """pickNo 's owner 's client is not cur_client"""
        self.res_diff = http.post_for_wms(url=url_finish_picking, data={
            "finishPickingDTOList": [
                {
                    "pickNo": "20210601-001-01"
                }
            ]
        }, session=self.session)

    def check_diff_status_code(self, status_code):
        self.assertEqual(self.res_diff.status_code, status_code)

    def check_diff_code(self, code):
        self.assertEqual(self.res_diff.json().get('code'), code)

    def None_body(self):
        self.res_None_body = http.post_for_wms(url=url_finish_picking, data={}, session=self.session)

    def check_None_body_status_code(self, status_code):
        self.assertEqual(self.res_None_body.status_code, status_code)

    def check_None_body_code(self, code):
        self.assertEqual(self.res_None_body.json().get('code'), code)

    def update(self):
        update = sql.execute_sql(command='UPDATE', sql=sql_update_picking_list_status_or_not)
        return update

    def finish_picking(self):
        self.res_finish_picking = http.post_for_wms(url=url_finish_picking, data={
            "finishPickingDTOList": [
                {
                    "pickNo": "202106020004-01"
                }
            ]
        }, session=self.session)

    def check_finish_picking_status_code(self, status_code):
        self.assertEqual(self.res_finish_picking.status_code, status_code)

    def check_finish_picking_code(self, code):
        self.assertEqual(self.res_finish_picking.json().get('code'), code)

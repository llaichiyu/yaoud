#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.wms import url_get_current_dc_list

data = get_data('src/data/csv/wms/get_current_dc_list.csv')
http = Http()


@test
@injectable(session=True)
class TestGetCurrentDcList(YaoudTestCase):
    @inject(data)
    def test_normally_get_dc_list(self):
        """cur_user exists && choose_store"""
        self.get_current_dc_list()
        self.check_get_current_dc_list_status_code(200)
        self.check_get_current_dc_list_code(200)

    def get_current_dc_list(self):
        """for get current_dc_list"""
        self.res_get_current_dc_list = http.post(url=url_get_current_dc_list, data={}, session=self.session)

    def check_get_current_dc_list_status_code(self, status_code):
        """for check status code"""
        self.assertEqual(self.res_get_current_dc_list.status_code, status_code)

    def check_get_current_dc_list_code(self, code):
        """for check code"""
        self.assertEqual(self.res_get_current_dc_list.json().get('code'), code)

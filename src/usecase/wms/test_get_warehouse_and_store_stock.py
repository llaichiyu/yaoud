#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject, get_x_site
from src.data.csv import get_data
from src.url.wms import url_get_ware_house_and_store_stock

data = get_data('src/data/csv/wms/get_warehouse_and_store_stock.csv')
http = Http()


@test
@injectable(title=['storeId', 'commodityCode'], session=True)
class TestGetWareHouseAndStoreStock(YaoudTestCase):
    @inject(data[0])
    def test_absent_x_site(self):
        """for token about absent x_site"""
        self.absent_x_site()
        self.check_absent_x_site_status_code(200)
        self.check_absent_x_site_code(401)

    @inject(data[0])
    def test_normally_get_warehouse_and_store_stock(self):
        """storeId and commodityCode and x-site all exists"""
        self.get_warehouse_and_store_stock()
        self.check_normally_get_status_code(200)
        self.check_normally_get_code(200)

    def get_warehouse_and_store_stock(self):
        """now i have x-site and i can get in get_x_site()"""
        self.res_get_ware_house_and_store_stock = http.post_for_wms(url=url_get_ware_house_and_store_stock,
                                                                    data={'storeId': self.storeId,
                                                                          'commodityCode': self.commodityCode},
                                                                    session=self.session)

    def check_normally_get_status_code(self, status_code):
        self.assertEqual(self.res_get_ware_house_and_store_stock.status_code, status_code)

    def check_normally_get_code(self, code):
        self.assertEqual(self.res_get_ware_house_and_store_stock.json().get('code'), code)

    def absent_x_site(self):
        """error for get warehouse and storestock and our token is absent for X-Site"""
        self.res_absent_x_site = requests.session().post(url=url_get_ware_house_and_store_stock,
                                                         data=json.dumps(obj={'storeId': self.storeId,
                                                                              'commodityCode': self.commodityCode}),
                                                         headers={'content-type': 'application/json'})

    def check_absent_x_site_status_code(self, status_code):
        """for check absent x_site status code"""
        self.assertEqual(self.res_absent_x_site.status_code, status_code)

    def check_absent_x_site_code(self, code):
        """for check absent x_site code"""
        self.assertEqual(self.res_absent_x_site.json().get('code'), code)

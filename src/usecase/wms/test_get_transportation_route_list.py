#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.wms import url_get_transportation_route_list

http = Http()
data = get_data('src/data/csv/wms/get_transportation_route_list.csv')


@test
@injectable(title=['orderStatus'], session=True)
class TestGetTransportationRouteList(YaoudTestCase):
    @inject(data[0])
    def test_get_transportation_route_list(self):
        self.get_transportation_route_list()
        self.check_get_transportation_route_list_status_code(200)
        self.check_get_transportation_route_list_code(200)

    @inject(data[0])
    def test_expire_token_for_get_transportation_route_list(self):
        """for check expire token for get transportation route list"""
        self.expire_token_for_get_transportation_route_list()
        self.check_expire_token_for_get_transportation_route_list_status_code(200)
        self.check_expire_token_for_get_transportation_route_list_code(401)

    def expire_token_for_get_transportation_route_list(self):
        self.res_expire_token_for_get_transportation_route_list = http.expire_token_for_post_wms(
            url=url_get_transportation_route_list, data={'orderStatus': self.orderStatus}, session=self.session)

    def check_expire_token_for_get_transportation_route_list_status_code(self, status_code):
        self.assertEqual(self.res_expire_token_for_get_transportation_route_list.status_code, status_code)

    def check_expire_token_for_get_transportation_route_list_code(self, code):
        self.assertEqual(self.res_expire_token_for_get_transportation_route_list.json().get('code'), code)

    def get_transportation_route_list(self):
        """for get transportation_route_list"""
        self.res_get_transportation_route_list = http.post_for_wms(url=url_get_transportation_route_list,
                                                                   data={'orderStatus': self.orderStatus},
                                                                   session=self.session)

    def check_get_transportation_route_list_status_code(self, status_code):
        self.assertEqual(first=self.res_get_transportation_route_list.status_code, second=status_code)

    def check_get_transportation_route_list_code(self, code):
        self.assertEqual(first=self.res_get_transportation_route_list.json().get('code'), second=code)

#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.auth_hub import url_login
from src.base.md5 import *
from unittest import skip

http = Http()
data = get_data('src/data/csv/auth_hub/login.csv')


@test
@injectable(['username', 'password'], session=True)
class TestLogin(YaoudTestCase):
    @inject(data[0])
    def test_normally_login(self):
        """username and password are normally"""
        self.login()
        self.check_login_status_code(200)
        self.check_login_code(0)

    @inject(data[1])
    def test_blank_username(self):
        self.login()
        self.check_login_status_code(200)
        self.check_login_code(1001)

    @inject(data[2])
    def test_blank_password(self):
        self.login()
        self.check_login_status_code(200)
        self.check_login_code(1001)

    # todo:重点关注有bug
    @inject(data[0])
    def test_None_username(self):
        """username is None and status_code must be 400"""
        self.None_username()
        self.check_None_username_status_code(400)
        self.check_None_username_code(1001)

    @inject(data[0])
    def test_None_password(self):
        """password == None"""
        self.None_password()
        self.check_None_password_status_code(200)
        self.check_None_password_code(1001)

    def None_password(self):
        """password == None"""
        self.session = requests.session()
        self.res_None_password = self.session.post(url=url_login, json={'username': self.username})

    def check_None_password_status_code(self, status_code):
        """for check None password status_code"""
        self.assertEqual(self.res_None_password.status_code, status_code)

    def check_None_password_code(self, code):
        """for check None password code"""
        self.assertEqual(self.res_None_password.json().get('code'), code)

    def None_username(self):
        """username == None"""
        self.session = requests.session()
        self.res_None_username = self.session.post(url=url_login, json={'password': self.password},
                                                   headers={'content-type': 'application/json'})

    def check_None_username_status_code(self, status_code):
        """for check None username status code"""
        self.assertEqual(self.res_None_username.status_code, status_code)

    def check_None_username_code(self, code):
        """for check None username code"""
        self.assertEqual(self.res_None_username.json().get('code'), code)

    def login(self):
        """login method"""
        self.session = requests.session()
        self.result_login = self.session.post(url=url_login,
                                              json={'username': self.username, 'password': self.password},
                                              headers={'content-type': 'application/json'})

    def check_login_status_code(self, status_code):
        """for check login status code"""
        self.assertEqual(self.result_login.status_code, status_code)

    def check_login_code(self, code):
        """for check code"""
        self.assertEqual(self.result_login.json().get('code'), code)

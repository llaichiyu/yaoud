#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.auth_hub import url_get_res_by_user

data = get_data('src/data/csv/auth_hub/get_res_by_user.csv')
http = Http()


@test
@injectable(session=True)
class TestGetResByUser(YaoudTestCase):
    @inject(data)
    def test_get_res_by_user(self):
        """we can also say this is normal"""
        self.get_res_by_user()
        self.check_get_res_by_user_status_code(200)
        self.check_get_res_by_user_code(0)

    def get_res_by_user(self):
        """get res by user"""
        self.result_get_res_by_user = http.post(url=url_get_res_by_user, session=self.session)

    def check_get_res_by_user_status_code(self, status_code):
        """for check get res by _user status code"""
        self.assertEqual(self.result_get_res_by_user.status_code, status_code)

    def check_get_res_by_user_code(self, code):
        """for  check code"""
        self.assertEqual(self.result_get_res_by_user.json().get('code'), code)

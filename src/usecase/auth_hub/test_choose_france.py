#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.auth_hub import url_choose_franc

http = Http()
data = get_data('src/data/csv/auth_hub/choose_france.csv')


@test
@injectable(title=['client', 'userId'], session=True)
class TestChooseFrance(YaoudTestCase):
    @inject(data[0], multi=False)
    def test_normally_choose_france(self):
        """0.login
           1.choose france
        """
        self.choose_france()
        print(data, '这是data')
        self.check_choose_france_status_code(200)
        self.check_choose_france_code(0)

    @inject(data[1], multi=True)
    def test_userId_not_exists(self):
        """userId is not exists in db"""
        self.choose_france()
        self.check_choose_france_status_code(200)
        self.check_choose_france_code(1001)

    def choose_france(self):
        """the method for choose france"""
        self.result_choose_france = http.post(url=url_choose_franc, data={'client': self.client, 'userId': self.userId},
                                              session=self.session)

    def check_choose_france_status_code(self, status_code):
        """for check choose france status code"""
        self.assertEqual(self.result_choose_france.status_code, status_code)

    def check_choose_france_code(self, code):
        """for check code"""
        self.assertEqual(self.result_choose_france.json().get('code'), code)

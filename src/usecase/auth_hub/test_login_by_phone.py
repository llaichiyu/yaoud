#! /usr/bin/env python3
# -*- coding:utf-8 -*-

"""
@author: pauline
@contact: 302869907@qq.com
@software: PyCharm
@file: test_login_by_phone.py
@time: 2022/3/14 11:28 上午
"""
import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.auth_hub import url_login_by_phone

http = Http()
data = get_data('src/data/csv/auth_hub/login_by_phone.csv')


@test
@injectable(title=['phone', 'code'], session=True)
class TestLoginByPhone(YaoudTestCase):
    @inject(data[0])
    def test_expire_code(self):
        self.login_by_phone()
        self.check_login_by_phone_status_code(200)
        self.check_login_by_phone_code(1001)

    def login_by_phone(self):
        self.res_login_by_phone = http.post(url=url_login_by_phone, data={'phone': self.phone, 'code': self.code},
                                            session=self.session)
        print(self.res_login_by_phone.json())

    def check_login_by_phone_status_code(self, status_code):
        self.assertEqual(self.res_login_by_phone.status_code, status_code)

    def check_login_by_phone_code(self, code):
        self.assertEqual(self.res_login_by_phone.json().get('code'), code)

#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url.auth_hub import url_select_franc_list

http = Http()
data = get_data('src/data/csv/auth_hub/get_user_list.csv')


@test
@injectable(session=True)
class TestSelectFrance(YaoudTestCase):
    @inject(data)
    def test_select_france_list(self):
        """for get user list normally"""
        self.select_france_list()
        self.check_get_user_list_status_code(200)
        self.check_get_user_list_code(0)

    def select_france_list(self):
        """for get user list when i change the cur user"""
        self.result_select_france_list = http.post(url=url_select_franc_list, session=self.session)

    def check_get_user_list_status_code(self, status_code):
        """for check get user list status code"""
        self.assertEqual(self.result_select_france_list.status_code, status_code)

    def check_get_user_list_code(self, code):
        """for check get user list code"""
        self.assertEqual(self.result_select_france_list.json().get('code'), code)

#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
@date 2021年08月19日4:08 下午
"""

from src.url import web_base_url_uat, operate_prefix

url_get_member_list = web_base_url_uat + operate_prefix + '/gsm03/gsm/getMemberList'
# url_get_brand_page = web_base_url_uat + operate_prefix + '/gsm03/gsm/getBrandPage'

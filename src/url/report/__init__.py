#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

from src.url import web_base_url_uat, report_prefix

url_choose_user_and_get_list = web_base_url_uat + report_prefix + '/loginMessage/listmessage'

#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

from src.url import web_base_url_uat, wms_prefix

url_get_ware_house_and_store_stock = web_base_url_uat + wms_prefix + '/web/api/v1/shippingManager/transferRequest/getWarehouseAndStoreStock'
url_get_current_dc_list = web_base_url_uat + wms_prefix + '/web/api/v1/thirdPartyManager/internal/getCurrentDcList'
url_get_allocated_stock_by_recommending = web_base_url_uat + wms_prefix + '/web/api/v1/shippingManager/transferRequest/getAllocatedStockByRecommending'
url_get_auth_data = web_base_url_uat + wms_prefix + '/web/api/v1/thirdPartyManager/internal/getAuthData'
url_get_record_customer_list = web_base_url_uat + wms_prefix + '/web/api/v1/shippingManager/querying/getRecordCustomerList'
url_get_purchase_order_detail_list = web_base_url_uat + wms_prefix + '/web/api/v1/shippingManager/transferRequest/getPurchaseOrderDetailList'
url_get_request_typelist_of_purchase_order = web_base_url_uat + wms_prefix + '/web/api/v1/shippingManager/transferRequest/getRequestTypeListOfPurchaseOrder'
url_get_transportation_route_list = web_base_url_uat + wms_prefix + '/web/api/v1/shippingManager/transferRequest/getTransportationRouteList'
url_create_transfer_order = web_base_url_uat + wms_prefix + '/web/api/v1/shippingManager/transferRequest/createTransferOrder'
url_export_purchase_order_detail_list = web_base_url_uat + wms_prefix + '/web/api/v1/shippingManager/transferRequest/exportPurchaseOrderDetailList'
url_get_not_order_list = web_base_url_uat + wms_prefix + '/web/api/v1/shippingManager/transferRequest/getNotOrderList'
url_get_customer_list = web_base_url_uat + wms_prefix + '/web/api/v1/shippingManager/querying/getCustomerList'
url_get_transportation_route_list_release = web_base_url_uat + wms_prefix + '/web/api/v1/shippingManager/releasing/getTransportationRouteList'
url_release_get_delivery_subno_list = web_base_url_uat + wms_prefix + '/web/api/v1/shippingManager/releasing/getDeliverySubNoList'
url_get_demand_type_list = web_base_url_uat + wms_prefix + '/web/api/v1/shippingManager/releasing/getDemandTypeList'
url_create_picking_list = web_base_url_uat + wms_prefix + '/web/api/v1/shippingManager/releasing/createPickingList'
url_get_transfer_order_detail_list = web_base_url_uat + wms_prefix + '/web/api/v1/shippingManager/releasing/getTransferOrderDetailList'
url_get_product_position_list = web_base_url_uat + wms_prefix + '/web/api/v1/thirdPartyManager/internal/getProductPositioningList'
url_export_transfer_order_detail_list = web_base_url_uat + wms_prefix + '/web/api/v1/shippingManager/releasing/exportTransferOrderDetailList'
url_delete_transfer_order = web_base_url_uat + wms_prefix + '/web/api/v1/shippingManager/releasing/deleteTransferOrder'
url_get_picking_list = web_base_url_uat + wms_prefix + '/web/api/v1/shippingManager/transferRequest/getPickingListPrint'
url_export_picking_list_print = web_base_url_uat + wms_prefix + '/web/api/v1/shippingManager/transferRequest/exportPickingListPrint'
url_get_picking_list_print_details = web_base_url_uat + wms_prefix + '/web/api/v1/shippingManager/transferRequest/getPickingPrintDetails'
url_get_transportation_route_list_by_picking = web_base_url_uat + wms_prefix + '/web/api/v1/shippingManager/picking/getTransportationRouteList'
url_finish_picking = web_base_url_uat + wms_prefix + '/web/api/v1/shippingManager/picking/finishPicking'
url_get_wave_no_list = web_base_url_uat + wms_prefix + '/web/api/v1/shippingManager/picking/getWaveNoList'
url_finish_deliver = web_base_url_uat + wms_prefix + '/web/api/v1/shippingManager/picking/finishDeliver'
url_export_picking_task_detail_list = web_base_url_uat + wms_prefix + 'web/api/v1/shippingManager/picking/exportPickingTaskDetailList'
url_get_deliver_list_print = web_base_url_uat + wms_prefix + '/web/api/v1/shippingManager/transferRequest/getDeliverListPrint'
url_export_deliver_list_print = web_base_url_uat + wms_prefix + '/web/api/v1/shippingManager/transferRequest/exportDeliverListPrint'
url_get_product_positioning_list = web_base_url_uat + wms_prefix + '/web/api/v1/thirdPartyManager/internal/getProductPositioningList'

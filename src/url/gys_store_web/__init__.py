#! /usr/bin/env python
# -*- coding:utf-8 -*-

from src.url import web_base_url_uat, gys_store_web_prefix

url_select_export_shop_list = web_base_url_uat + gys_store_web_prefix + '/adjust/selectExportShopList'
url_get_commodity_info = web_base_url_uat + gys_store_web_prefix + '/adjust/getCommodityInfo'
url_save_adjust_apply = web_base_url_uat + gys_store_web_prefix + '/adjust/saveAdjustApply'

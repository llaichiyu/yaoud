#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

from src.url import web_base_url_uat, operation_prefix, auth_hub

url_add_member = web_base_url_uat + operation_prefix + '/memberAddOrEdit/addMember'
url_recharge = web_base_url_uat + operation_prefix + '/pay/queryRecharge'
url_choose_store = web_base_url_uat + auth_hub + operation_prefix + '/chooseStore'
url_get_member = web_base_url_uat + operation_prefix + '/app/member/queryMember'
url_query_user = web_base_url_uat + operation_prefix + '/saleScheduleSetting/queryUser'
url_allocation = web_base_url_uat + operation_prefix + '/accept/diaoboList'
url_judgment = web_base_url_uat + operation_prefix + '/replenish/web/judgment'
url_replenish = web_base_url_uat + operation_prefix + '/replenish/web/hasReplenished'
url_showWareHouseStock = web_base_url_uat + operation_prefix + '/replenish/web/showWareHouseStock'
url_approve_vourchId = web_base_url_uat + operation_prefix + '/replenish/web/approve'
url_query_pro_thirdly = web_base_url_uat + operation_prefix + '/productSortSetting/queryProThirdly'
url_replenish_detail = web_base_url_uat + operation_prefix + '/replenish/web/replenishDetail'
url_replenish_insert = web_base_url_uat + operation_prefix + '/replenish/web/insert'
url_get_replenish_list_by_date = web_base_url_uat + operation_prefix + '/replenish/web/list'
url_get_detail_by_gsrhVoucherId = web_base_url_uat + operation_prefix + '/replenish/web/getDetail'
url_get_accept = web_base_url_uat + operation_prefix + '/accept/list'
url_get_accept_order_list = web_base_url_uat + operation_prefix + '/accept/acceptOrderList'
url_accept_warehouse_list = web_base_url_uat + operation_prefix + '/acceptWareHouse/list'
url_get_store_list = web_base_url_uat + operation_prefix + '/app/store/getStoreList'
url_insert_cashpayment = web_base_url_uat + operation_prefix + '/cashPayment/insert'
url_get_accept_warehouse_list = web_base_url_uat + operation_prefix + '/acceptWareHouse/list'
url_select_pick_productList = web_base_url_uat + operation_prefix + '/acceptWareHouse/selectPickProductList'
url_get_sup = web_base_url_uat + operation_prefix + '/accept/getSup'
url_get_tax = web_base_url_uat + operation_prefix + '/accept/getTax'
url_select_total_detail = web_base_url_uat + operation_prefix + '/acceptWareHouse/selectTotalDetail'
url_show_wtps_label = web_base_url_uat + operation_prefix + '/acceptWareHouse/showWtpsLabel'
url_pick_order_list = web_base_url_uat + operation_prefix + '/acceptWareHouse/pickOrderList'
url_query_user = web_base_url_uat + operation_prefix + '/saleScheduleSetting/queryUser'
url_get_franchisee_by_name = web_base_url_uat + operation_prefix + '/franchisee/getFranchiseeByName'
url_sale_plan_list = web_base_url_uat + operation_prefix + '/saleTask/system/salePlanList'
url_get_list_ent_product_bill = web_base_url_uat + operation_prefix + '/gaiaEntNewProductEvaluationH/listEntProductBill'
url_get_ent_product_bill_detail = web_base_url_uat + operation_prefix + '/gaiaEntNewProductEvaluationH/listEntProductBillDetail'
url_get_ent_info_list = web_base_url_uat + operation_prefix + '/gaiaEntNewProductEvaluationH/listEntInfo'
url_update_detail_list = web_base_url_uat + operation_prefix + '/gaiaEntNewProductEvaluationH/updateDetailList'
url_get_cal_list = web_base_url_uat + operation_prefix + '/newStoreDistribution/getCalculationList'
url_get_base_info = web_base_url_uat + operation_prefix + '/newStoreDistribution/getBaseInfo'
url_export = web_base_url_uat + operation_prefix + '/newStoreDistribution/export'

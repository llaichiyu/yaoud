#! /usr/bin/env python
# -*- coding:utf-8 -*- 
# @author:liyalan
# @created at 2020-09-19

from src.base.enviroment import *

# todo:base_web_url

web_base_url_uat = get_environment()
web_base_url_dev = get_environment()

# todo:all prefix

auth_hub = '/authHub'
operation_prefix = '/operation'
report_prefix = '/report'
accept_prefix = '/accept'
wms_prefix = '/wms'
operate_prefix = '/operate'
gys_store_web_prefix = '/gys-store-web/'

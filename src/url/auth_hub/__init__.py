#! /usr/bin/env python
# -*- coding:utf-8 -*- 
# @author:liyalan
# @created at 2020-10-10

from src.url import web_base_url_uat, auth_hub

url_login = web_base_url_uat + auth_hub + '/loginV2'
url_login_by_phone = web_base_url_uat + auth_hub + '/authHub/loginByPhone'
url_get_res_by_user = web_base_url_uat + auth_hub + '/getResByUser'
url_choose_franc = web_base_url_uat + auth_hub + '/chooseFranc'
url_select_franc_list = web_base_url_uat + auth_hub + '/selectFrancList'

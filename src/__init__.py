#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

import os
import sys

project_dir = os.path.abspath(
    os.path.join(os.path.abspath(__file__), os.path.pardir, os.path.pardir))
# print(project_dir)
# print('\t')

# sys.path.insert(0, project_dir)
print(sys.path)
# print('\t')

# sys.path.append(os.path.dirname(__file__) + os.sep + '../')
# print('\t')
# print(sys.path)

# print('+++++++++++++++++++++++++++++++++++++++')
# print('+++++++++++++++++++++++++++++++++++++++')
# print('+++++++++++++++++++++++++++++++++++++++')
# print('+++++++++++++++++++++++++++++++++++++++')
#
# print(os.pardir)
# # print(os.getcwd())

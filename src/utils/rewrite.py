#! /usr/bin/env python3
# -*- coding:utf-8 -*-

class Rewriteclass(object):
    def __init__(self, k, v):
        self.dic = {}
        self.dic[k] = v

    def __get__(self, k):
        return self.dic[k]

    def __set__(self, k, v):
        self.dic[k] = v


a = Rewriteclass(k='q', v='pp')
if __name__ == '__main__':
    print(a.__get__(k='q'))

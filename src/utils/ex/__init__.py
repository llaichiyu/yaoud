#! /usr/bin/env python3
# -*- coding:utf-8 -*-

"""
@author: pauline
@contact: 302869907@qq.com
@software: PyCharm
@file: __init__.py.py
@time: 2022/3/15 3:57 下午
"""
import requests, json
from src.base.http_class import Http
from src.base.run_test import injectable, test, YaoudTestCase, inject
from src.data.csv import get_data
from src.url

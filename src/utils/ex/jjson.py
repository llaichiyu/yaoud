#! /usr/bin/env python3
# -*- coding:utf-8 -*-

"""
@author: pauline
@contact: 302869907@qq.com
@software: PyCharm
@file: jjson.py
@time: 2022/3/15 9:25 下午
"""

import json


class Jjson(object):
    @classmethod
    def ob_to_json(cls):
        dictt = {'name': 'lee', 'age': 12}
        j = json.dumps(dictt)
        print(j)
        print(type(j))
        return j

    @classmethod
    def json_to_ob(cls):
        jj = json.dumps({'grade': 100})
        print(jj)
        print(type(jj))
        print('\t')
        aa = json.loads(jj)
        print(aa)
        print(type(aa))
        return aa


class Testabc(object):
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def std2(aa):
        return {
            'name': aa.name,
            'age': aa.age
        }


s = Testabc(name='lee', age=10)

if __name__ == '__main__':
    # Jjson.ob_to_json()
    # Jjson.json_to_ob()
    print(json.dumps(s,default=std2))

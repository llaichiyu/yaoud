#! /usr/bin/env python3
# -*- coding:utf-8 -*-

"""
@author: pauline
@contact: 302869907@qq.com
@software: PyCharm
@file: leime.py
@time: 2022/3/15 2:50 下午
"""


class A(object):
    def get_timea(self):
        import time
        print(time.time())
        return time.time()

    @get_timea
    def a(self):
        return "a"

    @classmethod
    def b(cls, q):
        b = 'b'
        print(b, q)
        return "b"

    @staticmethod
    def get_time():
        import time
        return time.time()


if __name__ == '__main__':
    A.b(q='123')

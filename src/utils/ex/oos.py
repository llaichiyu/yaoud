#! /usr/bin/env python3
# -*- coding:utf-8 -*-

"""
@author: pauline
@contact: 302869907@qq.com
@software: PyCharm
@file: oos.py
@time: 2022/3/15 4:39 下午
"""
import os


class Testos(object):
    @classmethod
    def get_oss(cls):
        print(os.environ.get('PATH'))
        return os.environ

    @classmethod
    def get_abpath(cls):
        print(os.path.abspath(''))
        return os.path.abspath('')

    @classmethod
    def get_new_dir(cls):
        a = os.path.join('yaoud/src/utils', '/qqq')
        print(a)
        return a

    @classmethod
    def mk_dir(cls):
        a = os.mkdir('yaoud/src/utils/bbb')
        print(a)
        return a


if __name__ == '__main__':
    # Testos.get_oss()
    Testos.get_abpath()
    Testos.get_new_dir()

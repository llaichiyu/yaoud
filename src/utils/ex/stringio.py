#! /usr/bin/env python3
# -*- coding:utf-8 -*-

"""
@author: pauline
@contact: 302869907@qq.com
@software: PyCharm
@file: stringio.py
@time: 2022/3/15 4:21 下午
"""
from io import StringIO


class Iooo(object):
    @classmethod
    def getioo(cls):
        f = StringIO()
        f.write('123')
        print(f.getvalue())
        f.close()
        return f

    @classmethod
    def getio1(cls):
        f = StringIO('qqq')
        f = f.read()
        # f = f.readline()
        print(f)
        return f


if __name__ == '__main__':
    Iooo.getioo()
    Iooo.getio1()

#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

"""统计网站访问量
统计出每个IP的访问量有多少？（从日志文件中查找）
"""
# !/usr/bin/env python3
# !coding=utf-8
list = []
f = file('/tmp/1.log')
str1 = f.readlines()
f.close()
for i in str1:
    ip = i.split()[0]
"""通过指定分隔符对字符串进行切片，默认为所有的空字符；split分隔后是一个列表，[0]
表示取其第一个元素；"""
list.append(ip)
list_num = set(list)
for j in list_num:
    num = list.count(j)
print('%s : %s' % (j, num))

# 生成报表
# _*_coding:utf-8_*_
import MySQLdb
import xlwt
from datetime import datetime


def get_data(sql):
    conn = MySQLdb.connect(host='127.0.0.1', user='root' \
                           , passwd='123456', db='test', port=3306, charset='utf8')
    cur = conn.cursor()
    cur.execute(sql)
    result = cur.fetchall()
    cur.close()
    conn.close
    return result


def write_data_to_excel(name, sql):
    result = get_data(sql)
    wbk = xlwt.Workbook()
    sheet = wbk.add_sheet('Sheet1', cell_overwrite_ok=True)
    today = datetime.today()
    today_date = datetime.date(today)
    for i in range(len(result)):
        for j in range(len(result[i])):
            sheet.write(i, j, result[i][j])
            wbk.save(name + str(today_date) + '.xls')


if __name__ == '__main__':
    db_dict = {'test': 'select * from student'}
    for k, v in db_dict.items():
        write_data_to_excel(k, v)

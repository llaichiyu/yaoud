#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""
from src.utils.get_some_data import get_db_host, get_db_port, get_db_user, get_db_pass, get_db_charset, get_db_name
import pymysql
import time


class ExecuteDb(object):
    def __init__(self, conn_name=''):
        while True:
            try:
                self.conn = pymysql.connect(host=get_db_host(), port=get_db_port(), user=get_db_user(),
                                            password=get_db_pass(),
                                            db=get_db_name(), charset=get_db_charset())
                self.cursor = self.conn.cursor()
                print("Connect success !!!")
                break
            except Exception as e:
                print('mysql error %d:%s' % (e.args[0], e.args[1]))
                print("连接错误，重试连接中。。。")
                time.sleep(10)
                continue

    def execute_sql(self, command, sql):
        """for types of sql in this method
        0. command
        1. sql
        2. return
        """
        if command in ('SELECT', 'select'):
            sql = sql.encode('utf-8')
            try:
                self.cursor.execute(sql)
                result = self.cursor.fetchall()
                return result
            except Exception as e:
                print(e)
            finally:
                self.conn.close()

        elif command in ('DELETE', 'delete', 'INSERT', 'insert', 'UPDATE', 'update'):
            sql = sql.encode('utf-8')
            try:
                self.cursor.execute(sql)
                result = self.cursor.execute(sql)
                self.conn.commit()
            except Exception as e:
                self.conn.rollback()
                print(e)
            finally:
                self.conn.close()
        else:
            print('command error!!!!!')

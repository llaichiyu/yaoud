#! /usr/bin/env python3
# -*- coding:utf-8 -*-


import json


class CheckDifference(object):

    def check_differnce_between_new_and_old(self, oldobj, newobj):
        oldobj = json.dumps(oldobj)
        newobj = json.dumps(newobj)
        print(oldobj + '\t')
        print("=======================================================================")
        print("=======================================================================")
        print("=======================================================================")
        print("======================================================================")
        print("======================================================================")
        print(newobj)
        oldkeys = list(oldobj.keys())  # get oldobj.keys()
        newkeys = list(newobj.keys())
        diff = {}
        for _key in set(oldkeys + newkeys):
            if newobj.get(_key, 0) != oldobj.get(_key, 0):
                diff[_key] = "new:%s old:%s" % (newobj.get(_key, 0), oldobj.get(_key, 0))
            return diff
    # def unicode_to_utf8(self):



old = {"code": 0,
       "data": {"adminFlag": 1, "client": "10000003", "clientName": "苏州公约树信息技术科技有限公司-UAT测试区", "depId": "10000003",
                "depName": "苏州公约树信息技术科技有限公司-UAT测试区", "franchiseeList": None, "loginAccount": "18816290151",
                "loginName": "吴鸣", "password": "", "showName": "苏州公约树信息技术科技有限公司-UAT测试区", "stoAttribute": "",
                "stoDeliveryMode": "", "storeList": None, "storeSelectLimit": 1,
                "token": "623d57100e6e4ef184af546ff55ee87c", "tokenUUID": "", "userAddr": "", "userId": "1045",
                "userIdc": "", "userLoginSta": "1", "userSex": "1", "userSoleSaleNo": "", "userSta": "0",
                "userTel": "18816290151", "userYsId": "", "userYsZyfw": "", "userYsZylb": ""}, "message": "提示：登录成功！"}

new = {"code": 0, "data": {"adminFlag": 1, "client": "10000005", "clientName": "苏州济世保盛医药连锁有限公司", "depId": "10000005",
                           "depName": "苏州济世保盛医药连锁有限公司", "franchiseeList": None, "loginAccount": "17793829974",
                           "loginName": "李雅兰", "password": "", "showName": "苏州济世保盛医药连锁有限公司", "stoAttribute": "",
                           "stoDeliveryMode": "", "storeList": None, "storeSelectLimit": 1,
                           "token": "6128ceb095ea45ccb40be81ab686004c", "tokenUUID": "", "userAddr": "",
                           "userId": "1083", "userIdc": "", "userLoginSta": "1", "userSex": "1", "userSoleSaleNo": "",
                           "userSta": "0", "userTel": "17793829974", "userYsId": "", "userYsZyfw": "",
                           "userYsZylb": ""}, "message": "提示：登录成功！"}


a = CheckDifference()

if __name__ == '__main__':
    a.check_differnce_between_new_and_old(newobj=new, oldobj=old)

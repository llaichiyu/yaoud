# #! /usr/bin/env python3
# # -*- coding:utf-8 -*-
# """
# @author: pauline
# Created on 27/05/2021 13:17 PM
# """
#
import platform
import hashlib

url_login = 'http://221.224.90.19:10102/auth/login'
url_choose_store = 'http://221.224.90.19:10102/auth/operation/chooseStore'


#

def get_password_md5():
    pv = platform.python_version()
    # print(pv)
    demo_val = 'Aa123456'
    md5_val = hashlib.md5(string=demo_val.encode('utf-8')).hexdigest()
    return md5_val


def get_wrong_x_site():
    return '99999991911191911919919'


def get_content():
    return 'application/json'


def get_fake_token():
    return 'oK3EbzG1vZKHzQbxObvr+MQr58FeRWfhkyMTxD82q/VUqEJjZIpwv45KQGymZWx8d6h0uIiSmI1KkZ9Nv4EB5A=='


def get_username():
    return '18816290151'


def get_userId():
    return '1071'


def get_stoCode():
    return '10000'


def get_client():
    return '10000005'


import json
import requests

url_login = 'http://221.224.90.19:10102/auth/login'


def get_password_md5():
    pv = platform.python_version()
    # print(pv)
    demo_val = 'Aa123456'
    md5_val = hashlib.md5(string=demo_val.encode('utf-8')).hexdigest()
    return md5_val


def get_token():
    def func():
        session = requests.session()
        login_token_ready = session.post(url=url_login,
                                         data=json.dumps({'username': get_username(), 'password': get_password_md5()}),
                                         headers={'content-type': get_content()}).json()
        login_token = login_token_ready['data']['token']
        print(login_token, type(login_token))
        choose_store_json = session.post(url=url_choose_store,
                                         data=json.dumps(
                                             {"userId": get_userId(), "stoCode": get_stoCode(),
                                              "client": get_client()}),
                                         headers={'content-type': get_content(),
                                                  'X-Token': login_token}).json()
        choose_store_token = choose_store_json['data']['token']
        print(choose_store_token, type(choose_store_token))
        return choose_store_token

    return func()


url_get_current_dc_list = 'http://221.224.90.19:10102/wms/web/api/v1/thirdPartyManager/internal/getCurrentDcList'


def get_x_site():
    """ dcCode == X-Site"""
    res_data = requests.session().post(url=url_get_current_dc_list, data=json.dumps({}),
                                       headers={'content-type': get_content(), 'X-Token': get_token()}).json().get(
        'data')
    res_data_0 = res_data[
        0]
    dc_code = res_data_0.get('dcCode')
    print(dc_code, type(dc_code))
    return dc_code


url_choose_store = 'http://221.224.90.19:10102/auth/operation/chooseStore'


def choose_store_for_token():
    """for choose store"""
    res_session = requests.session()
    res_choose_store = res_session.post(url=url_choose_store, data=json.dumps(
        obj={"userId": get_userId(), "stoCode": get_stoCode(), "client": get_client()}),
                                        headers={'X-Token': get_token(), 'content-type': get_content()}).json()
    print(res_choose_store, type(res_choose_store))
    return res_choose_store['data']['token']


"""[{},{}]"""
#
# if __name__ == '__main__':
#     # get_x_site()
#     # get_token()

#! /usr/bin/env python3
# -*- coding:utf-8 -*-

from datetime import date
import time


class GetTime(object):
    def __init__(self):
        pass

    def _get_outer_now(self):
        """:return float"""
        return time.time()

    def get_now(self):
        """outer method"""
        print(type(time.time()))
        return time.time()

    def get_tuple_time(self):
        """:return tuple"""
        now = self._get_outer_now()
        return time.localtime(now)

    def get_timestamp(self):
        now = self._get_outer_now()
        localtime = time.localtime(now)
        return time.strftime("%Y-%m-%d %H:%M:%S", localtime)  # 注意大小写,否则会变成2022-54-02/15/22 13-54-22
        # >> > time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time()))

    def change_str_to_timestamp(self):
        from datetime import datetime
        st = self._get_outer_now()
        print(type(st))
        exit(0)
        str_to_timestamp = datetime.strptime(st, "%Y-%m-%d %H:%M:%S")
        print(str_to_timestamp)
        return str_to_timestamp


date = GetTime()

if __name__ == '__main__':
    # print(date.change_str_to_timestamp()) #有点问题
    # print(date.get_now())
    print(date.get_timestamp())
    pass

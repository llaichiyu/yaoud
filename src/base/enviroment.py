#! /usr/bin/env python3
# -*- coding:utf-8 -*-


import os


class UatConfig(object):
    """uat地址"""
    host = ""


class DevConfig(object):
    """dev地址"""
    host = ""


mapping = {
    "uat": UatConfig,
    "dev": DevConfig
}

import sys


def get_environment():
    # print(sys.argv)  # return a list
    num = len(sys.argv) - 1
    if num < 1 or num > 1:
        exit("ERROR COMMAND")
        # print(num)
    env = sys.argv[1]  # 返回下标为1的传入参数
    # print(env)
    appenv = os.environ.get("appenv", env).lower()
    # print("appenv" + "\t" + appenv)
    config = mapping[appenv]()
    # if config == "uat" or config == "dev":
    #     exit("key error")
    # print(config.host)
    return config.host


if __name__ == '__main__':
    get_environment()

#! /usr/bin/env python3
# -*- coding:utf-8 -*-

import requests
import json
from src.utils.get_some_data import *
from src.base.md5 import *

md5 = MD5()

from src.url.operation import url_choose_store


class GetToken(object):
    """
    for get token
    """

    def __init__(self):
        # self.url_login = url_login
        # self.url_choose_store = url_choose_store
        # self.username = username
        # self.password = password
        # self.userId = userId
        # self.stoCode = stoCode
        # self.client = client
        # self.content = content
        # self.login_token = login_token
        pass

    def get_token(self):
        def func():
            import requests, json
            r0 = requests.session().post(url='',
                                         data=json.dumps(
                                             {'username': '',
                                              'password': ''}),
                                         headers={'content-type': 'application/json'}).json()
            client = r0['data']['clients'][5]['client']
            userId = r0['data']['clients'][5]['userId']
            r1 = requests.session().post(url='',
                                         data=json.dumps({'client': client, 'userId': userId}),
                                         headers={'content-type': 'application/json'}).json()
            token = r1['data']['loginInfo']['token']
            return token

        return func()

    def get_fake_token(self):
        return 'oK3EbzG1vZKHzQbxObvr+MQr58FeRWfhkyMTxD82q/VUqEJjZIpwv45KQGymZWx8d6h0uIiSmI1KkZ9Nv4EB5A=='

#
# a = GetToken()
#
# if __name__ == '__main__':
#     a.get_token()

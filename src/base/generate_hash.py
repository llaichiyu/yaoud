#! /usr/bin/env python
# -*- coding:utf-8 -*- 
# @author:liyalan
# @created at 2020-10-15

import hashlib

# SHA-256

hash = hashlib.sha256()
hash.update(''.encode('utf-8'))
print('SHA-256', '长度:', len(hash.hexdigest()) * 4, hash.hexdigest())



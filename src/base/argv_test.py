#! /usr/bin/env python3
# -*- coding:utf-8 -*-

import sys


def test_sys_argv():
    """
    #todo:注意看下面三个不同的例子
    sys.argv ->  <class 'list'>   sys.argv -> %message ['base/argv_test.py']
    sys.argv[0] -> <class 'str'>    sys.argv -> %message base/argv_test.py
    sys.argv[1] -> IndexError: list index out of range
    """
    message = sys.argv[0]
    print(type(message), '\t', 'sys.argv -> %message', message)


if __name__ == '__main__':
    test_sys_argv()

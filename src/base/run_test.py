#! /usr/bin/env python
# -*- coding:utf-8 -*-
# @author:liyalan
# @created at 2020-09-22

"""you choose"""


class InjectError(Exception):
    pass


class TestError(Exception):
    pass


class TokenError(Exception):
    def token_overtime(self):
        """return couldn't get token owning to time out"""
        raise TimeoutError


from src.url.wms import url_get_current_dc_list
from src.base.get_token import *

a = GetToken()


def get_x_site():
    """ dcCode == X-Site"""
    res_data = requests.session().post(url=url_get_current_dc_list, data=json.dumps({}),
                                       headers={'content-type': get_content(),
                                                'X-Token': a.get_token()}).json().get(
        'data')
    res_data_0 = res_data[
        0]
    dc_code = res_data_0.get('dcCode')
    return dc_code


import unittest
import logging.config
import warnings


class YaoudTestCase(unittest.TestCase):
    def setUp(self):
        warnings.simplefilter('ignore', ResourceWarning)
        print('++++++++用例{}测试开始++++++++++'.format(self))

    def __init__(self, methodName):
        super().__init__(methodName)
        self._multi = False
        self._dataset = []

    def tearDown(self):
        print('--------用例{}测试结束----------'.format(self))


def _inject_to(data, self):
    """_inject_to"""
    if not self or not hasattr(self, '_title'):
        raise InjectError('function not injectable')
    for i, name in enumerate(self._title):
        setattr(self, name, data[i])


def _inject_multi_to(dataset, self):
    """_inject_multi_to"""
    if not self or not hasattr(self, '_title'):
        raise InjectError('function not injectable')
    if len(dataset) > 1:
        data = dataset[0]
        self._dataset = dataset[1:]
        self._multi = True
    else:
        data = dataset[0]
        self._dataset = None
        self._multi = False
    for i, name in enumerate(self._title):
        setattr(self, name, data[i])


def _remove(self):
    """_remove"""
    _title = self._title
    for name in self._title:
        delattr(self, name)


def inject(data, multi=False):
    """inject()"""

    def decorator(func):
        def wrapper(self):
            if multi:
                _inject_multi_to(data, self)
            else:
                _inject_to(data, self)
            func(self)
            _remove(self)

        return wrapper

    return decorator


def injectable(title=[], session=False):
    import requests

    def setUpSession(func, klass):
        def decorator(*args, **kw):
            klass.session = requests.Session()
            func(*args, **kw)

        return decorator

    def tearDownSession(func, klass):
        def decorator(*args, **kw):
            klass.session.close()
            func(*args, **kw)

        return decorator

    def decorator(klass):
        if session:
            klass.setUp = setUpSession(klass.setUp, klass)
            klass.tearDown = tearDownSession(klass.tearDown, klass)
        return type(klass.__name__, (klass,), {"_title": title})

    return decorator


tests = []


def test(klass):
    """test"""
    tests.append(klass)
    return klass


def test_runner(func):
    """test -> runner"""

    def decorator(*args, **kw):
        kw['tests'] = tests
        func(*args, **kw)

    return decorator

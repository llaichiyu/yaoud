#! /usr/bin/env python
# -*- coding:utf-8 -*-

import requests
import json
import functools
from src.base.run_test import get_x_site, get_content, get_wrong_x_site
from src.base.get_token import GetToken

a = GetToken()
with_session = None


class Http():
    """rewrite Http class"""

    def __init__(self):
        # self.url = url
        # self.data = data
        # self.params = params
        # self.session = session
        # self.headers = headers
        pass

    def post(self, url, data={}, params={}, session=with_session, **headers):
        if params is None:
            params = {}
        if not headers.get('Content-type', None):
            headers['Content-type'] = get_content()
        if not headers.get('X-Token', None):
            headers['X-Token'] = a.get_token()
        if data is not None:
            data = json.dumps(data)
        if session:
            r = session
        else:
            r = requests
        result = r.post(url=url, data=data, params=params, headers=headers)
        return result

    def post_for_wms(self, url, data={}, params={}, session=with_session, **headers):
        if params is None:
            params = {}
        if not headers.get('Content-type', None):
            headers['Content-type'] = get_content()
        if not headers.get('X-Token', None):
            headers['X-Token'] = a.get_token()
            if not headers.get('X-Site', None):
                headers['X-Site'] = get_x_site()
        if data is not None:
            data = json.dumps(data)
        if session:
            r = session
        else:
            r = requests
        result = r.post(url=url, data=data, params=params, headers=headers)
        return result

    def expire_token_for_post(self, url, data={}, params={}, session=with_session, **headers):
        if params is None:
            params = {}
        if not headers.get('Content-type', None):
            headers['Content-type'] = get_content()
        if not headers.get('X-Token', None):
            headers[
                'X-Token'] = a.get_fake_token()
        if data is not None:
            data = json.dumps(data)
        if session:
            r = session
        else:
            r = requests
        result = r.post(url=url, data=data, params=params, headers=headers)
        return result

    def expire_token_for_post_wms(self, url, data={}, params={}, session=with_session, **headers):
        if params is None:
            params = {}
        if not headers.get('Content-type', None):
            headers['Content-type'] = get_content()
        if not headers.get('X-Token', None):
            headers[
                'X-Token'] = a.get_fake_token()
        if not headers.get('X-Site', None):
            headers['X-Site'] = get_x_site()
        if data is not None:
            data = json.dumps(data)
        if session:
            r = session
        else:
            r = requests
        result = r.post(url=url, data=data, params=params, headers=headers)
        return result

    def post_for_unauthorized_wms(self, url, data={}, params={}, session=with_session, **headers):
        if params is None:
            params = {}
        if not headers.get('Content-type', None):
            headers['Content-type'] = get_content()
        if not headers.get('X-Token', None):
            headers[
                'X-Token'] = a.get_token()
        if not headers.get('X-Site', None):
            headers['X-Site'] = get_wrong_x_site()
        if data is not None:
            data = json.dumps(data)
        if session:
            r = session
        else:
            r = requests
        result = r.post(url=url, data=data, params=params, headers=headers)
        return result

    def get(self, url, params={}, session=with_session, **headers):
        if params is None:
            params = {}
        if not headers.get('Content-type', None):
            headers['Content-type'] = get_content()
        if not headers.get('X-Token', None):
            headers['X-Token'] = a.get_token()
            r = None
        if session:
            r = session
        else:
            r = requests
        result = r.get(url=url, params=params, headers=headers)
        return result

    def expire_token_for_get(self, url, params={}, session=with_session, **headers):
        if params is None:
            params = {}
        if not headers.get('Content-type', None):
            headers['Content-type'] = get_content()
        if not headers.get('X-Token', None):
            headers[
                'X-Token'] = a.get_fake_token()
        r = None
        if session:
            r = session
        else:
            r = requests
        result = r.get(url=url, params=params, headers=headers)
        return result

    def get_for_wms(self, url, params={}, session=with_session, **headers):
        if params is None:
            params = {}
        if not headers.get('Content-type', None):
            headers['Content-type'] = get_content()
        if not headers.get('X-Token', None):
            headers['X-Token'] = a.get_token()
            headers['X-Site'] = get_x_site()
        r = None
        if session:
            r = session
        else:
            r = requests
        result = r.get(url=url, params=params, headers=headers)
        return result

    def put(self, url, data={}, params={}, session=with_session, **headers):
        if params is None:
            params = {}
        if not headers.get('Content-type', None):
            headers['Content-type'] = get_content()
        if not headers.get('X-Token', None):
            headers['X-Token'] = a.get_token()
        if data is not None:
            data = json.dumps(data)
        if session:
            r = session
        else:
            r = requests
        result = r.put(url=url, data=data, params=params, headers=headers)
        return result

    def expire_token_for_put(self, url, data={}, params={}, session=with_session, **headers):
        if params is None:
            params = {}
        if not headers.get('Content-type', None):
            headers['Content-type'] = get_content()
        if not headers.get('X-Token', None):
            headers[
                'X-Token'] = a.get_fake_token()
        if data is not None:
            data = json.dumps(data)
        if session:
            r = session
        else:
            r = requests
        result = r.put(url=url, data=data, params=params, headers=headers)
        return result

    def patch(self, url, data={}, params={}, session=with_session, **headers):
        if params is None:
            params = {}
        if not headers.get('Content-type', None):
            headers['Content-type'] = get_content()
        if not headers.get('X-Token', None):
            headers['X-Token'] = a.get_token()
        if data is not None:
            data = json.dumps(data)
        if session:
            r = session
        else:
            r = requests
        result = r.patch(url=url, data=data, params=params, headers=headers)
        return result

    def expire_token_for_patch(self, url, data={}, params={}, session=with_session, **headers):
        if params is None:
            params = {}
        if not headers.get('Content-type', None):
            headers['Content-type'] = get_content()
        if not headers.get('X-Token', None):
            headers[
                'X-Token'] = a.get_fake_token()
        if data is not None:
            data = json.dumps(data)
        if session:
            r = session
        else:
            r = requests
        result = r.patch(url=url, data=data, params=params, headers=headers)
        return result

    def delete(self, url, data={}, params={}, session=with_session, **headers):
        if params is None:
            params = {}
        if not headers.get('Content-type', None):
            headers['Content-type'] = get_content()
        if not headers.get('X-Token', None):
            headers['X-Token'] = a.get_token()
        if data is not None:
            data = json.dumps(data)
        if session:
            r = session
        else:
            r = requests
        result = r.delete(url=url, data=data, params=params, headers=headers)
        return result

    def expire_token_for_delete(self, url, data={}, params={}, session=with_session, **headers):
        if params is None:
            params = {}
        if not headers.get('Content-type', None):
            headers['Content-type'] = get_content()
        if not headers.get('X-Token', None):
            headers[
                'X-Token'] = a.get_token()
        if data is not None:
            data = json.dumps(data)
        if session:
            r = session
        else:
            r = requests
        result = r.delete(url=url, data=data, params=params, headers=headers)
        return result

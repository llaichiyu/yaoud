#! /usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@author: pauline
Created on 27/05/2021 13:17 PM
"""

import pika

host = ''
port = ''
username = ''
password = ''
call_back = ''
body_json = {
    "saleH": {
        "gsshNormalAmt": 18,  # 零售价格
        "returnStatus": "N",  # ？？？
        "gsshPaymentNo1": "1000",  # 支付方式为 "现金支付"
        "gsshTime": "020945",  #
        "gsshPaymentNo4": "3002",  # 支付方式为"支付宝"
        "gsshPaymentNo5": "5000",  # 支付方式为"储值卡"
        "gsshPaymentAmt2": 0,  # 支付方式2
        "gsshPaymentNo2": "2000",  # 支付方式为 "银联"
        "gsshPaymentAmt1": 18,  # 支付金额1
        "gsshPaymentNo3": "3001",  # 支付方式为微信
        "gsshPaymentAmt4": 0,  # 支付金额4
        "gsshDzqdyActno5": "",  # 销售备注
        "gsshPaymentAmt3": 0,  # 支付金额3
        "gsshDate": "20210513",  # 销售日期
        "gsshPaymentAmt5": 0,  # 支付金额5
        "gsshIntegralAdd": "18",  # 增加积分
        "gsshDzqdyAmt1": 0,  # 电子券抵用金额1
        "gsshYsAmt": 18,  # 应收金额
        "gsshEmp": "1008",  # 收银工号
        "gsshIntegralCashAmt": 0,  # 抵现金额
        "gsshZkAmt": 0,  # 折扣金额
        "gsshDyqAmt": 0,  # 抵用券金额
        "gsshHideFlag": "0",  # 不挂起
        "gsshRechargeCardAmt": 0,  # 储值卡消费金额
        "gsshCallAllow": "1",  # 允许调出销售
        "client": "10000005",  # 加盟商
        "gsshHykNo": "VM10005548",  # 会员卡号
        "gsshRmbZlAmt": 0,  # 现金找零
        "gsshBillNo": "SD2105131P4PDA0EEQ80",  # 销售单号
        "gsshIntegralExchangeAmt": 0,  # 加价兑换积分金额
        "gsshIntegralCash": "0",  # 抵现积分
        "gsshIntegralExchange": "0",  # 兑换积分
        "gsshBrId": "10000",  # 店名
        "whetherToSubmit": "N",  # ???
        "gsshRmbAmt": 18  # 现金收银金额
    },
    "brId": "10000",  # 店名
    "batchChangeList": [{
        "gsbcProId": "01090000003",
        "gsbcBatchNo": "6191760",
        "gsbcVoucherId": "SD2105131P4PDA0EEQ80",
        "client": "10000003",
        "gsbcBrId": "10000",
        "gsbcQty": 1,
        "gsbcSerial": "1",
        "gsbcBatch": "JHAZDA00047408_1",
        "gsbcDate": "20210513"
    }],
    "saleDList": [{
        "gssdSalerId": "1004",  # 营业员编号
        "gssdBrId": "10000",  # 店名
        "gssdMovTax": 0.13,  # 税率
        "gssdAmt": 18, #
        "gssdOriQty": 1,
        "gssdProId": "01090000003",
        "gssdBatchNo": "6191760",
        "gssdValidDate": "6191760\n20220731",
        "gssdBillNo": "SD2105131P4PDA0EEQ80",
        "gssdSerial": "1",
        "client": "10000003",
        "gssdQty": 1,
        "gssdPrc2": 18,
        "gssdPrc1": 18,
        "gssdDate": "20210513",
        "gssdZkAmt": 0
    }],
    "selectElectronList": [],
    "saleRecipelList": [],
    "client": "10000003",
    "payMsgList": [{
        "gsspmRmbAmt": 18,
        "gsspmZlAmt": 0,
        "gsspmBrId": "10000",
        "gsspmId": "1000",
        "gsspmBillNo": "SD2105131P4PDA0EEQ80",
        "client": "10000003",
        "gsspmDate": "20210513",
        "gsspmAmt": 18,
        "gsspmType": "1",
        "gsspmName": "现金"
    }]
}


class TestRabbitmq():

    def producer(self):
        """mock this is a producer"""
        self.credentials = pika.PlainCredentials(username=username, password=password, erase_on_connect=False)
        self.connection = pika.BlockingConnection(
            pika.ConnectionParameters(host=host, port=port, credentials=self.credentials))
        self.channel = self.connection.channel()
        self.channel.queue_declare(queue='hi,this is producer')
        self.channel.basic_publish(exchange='', routing_key='', body='rabbitmq ready')
        print('队列开始！！！！！')
        self.connection.close(reply_code=200, reply_text='normally shutdown!!!')

    def consumer(self):
        """mock this is a consumer"""
        self.credentials = pika.PlainCredentials(username=username, password=password)
        self.connection = pika.BlockingConnection(
            pika.ConnectionParameters(host=host, port=port, credentials=self.credentials))
        self.channel = self.connection.channel()  # create queue
        self.channel.queue_declare(queue='hi,this is consumer')
        print('准备消费啦')

    def callback(self, ch, method, propertities, body):
        """ready for callback"""
        print('[x] Received %r' % body)
        self.channel.basic_consume(queue='this is a queue', on_message_callback=call_back, auto_ack=False)
        print('[*] waiting for messages')
        self.channel.start_consuming()

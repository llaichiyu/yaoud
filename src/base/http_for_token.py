#! /usr/bin/env python3
# -*- coding:utf-8 -*-
import requests, json

with_session = None
from src.utils.get_some_data import *
from src.base.get_token import GetToken


class Http(object):
    """for token 's http"""

    def __init__(self):
        pass

    def post_for_token(self, url_login, data, params, session=None, **headers):
        if data is None:
            data = {}
        if params is None:
            params = {}
        if not headers.get('Content-type', None):
            headers['Content-type'] = get_content()
        if not headers.get('X-Token', None):
            headers['X-Token'] = GetToken.get_token()
            data = json.dumps(obj=data)
        if session:
            res = session
        else:
            res = requests
        result = res.post(url=url_login, data=data, params=params, headers=headers)
        return result

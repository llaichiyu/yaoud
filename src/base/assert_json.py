#! /usr/bin/env python
# -*- coding:utf-8 -*- 
# @author:liyalan
# @created at 2020-09-23

import json


# response = {
#     'k1': 'v1',
#     'retData': {
#         'wallet':
#             {
#                 'total':
#                     {
#                         'balance': '520',
#                         'amount_btc': '123'
#                     }
#             },
#         'list':
#             [{
#                 'currency_id': '2',
#                 'symbol': 'BTC',
#                 'logo': 'https://n-bkt.deepcoin.pro/BTC.png',
#                 'desc': '比特币',
#                 'amount': '0.00000000',
#                 'balance': '0.00'
#             }]
#     }
# }
#
# print(response.get('retData').items())


# print(type(response))


class AssertJson(object):
    """find aimed keys in response json
    """

    def get_keys(self, data):
        """要拿到response的所有key
        :rtype: object
        """
        all_key_list = []

        def get_keys(data):
            if type(data) == type({}):
                keys = data.keys()
                for key in keys:
                    value = data.get(key)
                    if type(value) != type({}) and type(value) != type([]):
                        all_key_list.append(key)
                    elif type(value) == type({}):
                        all_key_list.append(key)
                    elif type(value) == type([]):
                        all_key_list.append(key)
                        for para in value:
                            if type(para) == type({}) and type([]):
                                get_keys(para)
                            else:
                                all_key_list.append(para)

        get_keys(data)
        return all_key_list

    def is_extend(self, data, tag_key):
        """tag_key是否在data里"""
        if type(data) != type([]):
            print('11111')
        else:
            key_list = self.get_keys(data)
            if key_list is not None:
                for key in key_list:
                    if key == tag_key:
                        return True
                    else:
                        print('tag_key没在key_list中')
            else:
                key_list = []
                for k in key_list:
                    key_list.append(k)
                    if k in key_list:
                        return True
                    else:
                        raise BaseException

#
# if __name__ == '__main__':
#     check_json = AssertJson()

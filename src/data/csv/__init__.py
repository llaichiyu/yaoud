#! /usr/bin/env python
# -*- coding:utf-8 -*- 
# @author:liyalan
# @created at 2020-09-19

import csv


def get_data(file_name):
    with open(file_name, mode='r', encoding='utf-8-sig') as csvfile:
        data = list(csv.reader(csvfile, delimiter=','))
        return data
